package edu.kit.informatik.student.knowledgehunt.protocol.messages.meta;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

public class MessageOutputStream {
	private ByteArrayOutputStream stream = new ByteArrayOutputStream();

	public void write(int i) {
		try {
			stream.write(int2Byte(i));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void write(String s) {
		if (s == null) {
			write(0);
			return;
		}
		try {
			byte[] buf = s.getBytes(Charset.forName("UTF-8"));
			write(buf.length);
			stream.write(buf);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public byte[] toByteArray() {
		return stream.toByteArray();
	}

	public static byte[] int2Byte(int i) {
		return ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(i).array();
	}
}
