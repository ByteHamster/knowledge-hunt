package edu.kit.informatik.student.knowledgehunt.protocol.messages.meta;

import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageFactory;

import java.util.logging.Logger;

public abstract class Message {
	public static final int HEADER_LENGTH = 8;
	private static final Logger LOG = Logger.getLogger(Message.class.getName());
	private static final int IXD_LENGTH = 4;
	private int socketId;

	public byte[] getBytes() {
		MessageOutputStream messageStream = new MessageOutputStream();
		messageStream.write(getMessageId());
		messageStream.write(0); // Length is set later
		writeMessagePayload(messageStream);

		byte[] bytes = messageStream.toByteArray();
		int length = bytes.length - HEADER_LENGTH;
		System.arraycopy(MessageOutputStream.int2Byte(length), 0, bytes, IXD_LENGTH, 4);
		return bytes;
	}

	protected abstract void writeMessagePayload(MessageOutputStream messageStream);

	public static Message parse(byte[] bytes) {
		return parse(new MessageInputStream(bytes));
	}

	public static Message parse(MessageInputStream messageStream) {
		int messageType = messageStream.readInt();
		messageStream.readInt(); // Length

		Message message = MessageFactory.getMessage(messageType);

		if (message == null) {
			String text = "Unknown message type: " + messageType;
			LOG.severe(text);
			throw new IllegalArgumentException(text);
		}

		try {
			message.parsePayload(messageStream);
		} catch (RuntimeException e) {
			String text = "Parsing the message failed: " + message.getClass().getName();
			LOG.severe(text);
			LOG.info(messageStream.toString());
			throw new IllegalArgumentException(text, e);
		}

		return message;
	}

	protected abstract void parsePayload(MessageInputStream messageStream);

	private int getMessageId() {
		return getClass().getAnnotation(MessageDef.class).id();
	}

	public int getSocketId() {
		return socketId;
	}

	public void setSocketId(int socketId) {
		this.socketId = socketId;
	}
}
