package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

import java.util.ArrayList;
import java.util.List;


@MessageDef(id = 400)
public class ScoreboardMessage extends Message {
	private int winner;
	private String winningReason;
	private List<Player> players = new ArrayList<>();

	public ScoreboardMessage(int winner, String winningReason, List<Player> players) {
		this.winner = winner;
		this.winningReason = winningReason;
		this.players = players;
	}
	
	public ScoreboardMessage() {
	}
	
	
	@Override
	public String toString() {
		return "Scoreboard: winner=" + winner + ", winningReason=" + winningReason + ", players=" + players;
	}
	

	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(winner);
		messageStream.write(winningReason);
		messageStream.write(players.size());
		for (Player player : players) {
			messageStream.write(player.getId());
			messageStream.write(player.getPoints());
		}
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		winner = messageStream.readInt();
		winningReason = messageStream.readString();
		ArrayList<Player> list = new ArrayList<>();
		int numPlayers = messageStream.readInt();
		for (int i = 0; i < numPlayers; i++) {
			Player player = new Player();
			player.setId(messageStream.readInt());
			player.setPoints(messageStream.readInt());
			list.add(player);
		}
		players = list;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public int getWinner() {
		return winner;
	}

	public void setWinner(int winner) {
		this.winner = winner;
	}

	public String getWinningReason() {
		return winningReason;
	}

	public void setWinningReason(String winningReason) {
		this.winningReason = winningReason;
	}
}
