package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

@MessageDef(id = 301)
public class TokenUpdateMessage extends Message {
	private int tokenId;
	private int locationId;

	public TokenUpdateMessage(int tokenId, int locationId) {
		this.tokenId = tokenId;
		this.locationId = locationId;
	}
	
	public TokenUpdateMessage() {
	}
	
	
	@Override
	public String toString() {
		return "TokenUpdate: tokenId=" + tokenId + ", locationId=" + locationId;
	}
	

	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(tokenId);
		messageStream.write(locationId);
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		tokenId = messageStream.readInt();
		locationId = messageStream.readInt();
	}

	public int getTokenId() {
		return tokenId;
	}

	public void setTokenId(int tokenId) {
		this.tokenId = tokenId;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
}
