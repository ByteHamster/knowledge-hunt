package edu.kit.informatik.student.knowledgehunt.protocol;

import java.time.Duration;

public final class ProtocolConstants {  // TODO adjust values
	
	public static final Duration MAX_DURATION_PREROUND = Duration.ofHours(1);
	public static final Duration MAX_DURATION_ROUND = Duration.ofHours(1);
	public static final Duration MAX_DURATION_POSTROUND = Duration.ofHours(1);
	
	public static final int MAX_NUMBER_LOCATIONS = 100;
	public static final int MAX_NUMBER_TOKENS = 1000;
	
	public static final int INVALID_TOKEN_ID = 0;
	public static final int INVALID_LOCATION_ID = 0;
	public static final int INVALID_PLAYER_ID = Integer.MIN_VALUE;
	public static final int INVALID_REQUEST_ID = 0;
	public static final int INVALID_TIMER = 0;
	
	public static final int HIDDEN_TYPE = Integer.MIN_VALUE;
	public static final int HIDDEN_VALUE = Integer.MIN_VALUE;
	public static final String HIDDEN_DISPLAY_NAME = "";
	
	
	private ProtocolConstants() {
	}
	
}
