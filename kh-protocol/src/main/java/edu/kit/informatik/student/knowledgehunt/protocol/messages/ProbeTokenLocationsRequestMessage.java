package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

@MessageDef(id = 302)
public class ProbeTokenLocationsRequestMessage extends Message {
	private int requestId;
	private int tokenId;
	
	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(requestId);
		messageStream.write(tokenId);
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		requestId = messageStream.readInt();
		tokenId = messageStream.readInt();
	}

	public int getTokenId() {
		return tokenId;
	}

	public void setTokenId(int tokenId) {
		this.tokenId = tokenId;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	@Override
	public String toString() {
		return getClass().getName() + ": token id=" + tokenId;
	}
}
