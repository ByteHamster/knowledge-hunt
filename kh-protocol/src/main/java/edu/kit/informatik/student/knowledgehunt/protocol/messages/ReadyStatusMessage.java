package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

@MessageDef(id = 153)
public class ReadyStatusMessage extends Message {
	private boolean ready;
	
	
	@Override
	public String toString() {
		return "ReadyStatus: ready=" + ready;
	}
	
	
	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(ready ? "READY" : "UNREADY");
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		String readyEnum = messageStream.readString();
		switch (readyEnum) {
			case "READY":
				ready = true;
				break;
			case "UNREADY":
				ready = false;
				break;
			default:
				throw new IllegalArgumentException("Undefined enum constant");
		}
	}
	
	
	public boolean isReady() {
		return ready;
	}

	public void setReady(boolean ready) {
		this.ready = ready;
	}
}
