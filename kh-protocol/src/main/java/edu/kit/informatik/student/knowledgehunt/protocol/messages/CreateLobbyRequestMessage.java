package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

@MessageDef(id = 106)
public class CreateLobbyRequestMessage extends Message {
	private int requestId;
	private String lobbyName;
	private int maxPlayers;
	private String gameModeId;
	
	@Override
	public String toString() {
		return "CreateLobbyRequest: lobbyName=" + lobbyName + ", maxPlayers=" + maxPlayers
				+ ", gameModeId=" + gameModeId;
	}
	
	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(requestId);
		messageStream.write(lobbyName);
		messageStream.write(maxPlayers);
		messageStream.write(gameModeId);
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		requestId = messageStream.readInt();
		lobbyName = messageStream.readString();
		maxPlayers = messageStream.readInt();
		gameModeId = messageStream.readString();
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public String getLobbyName() {
		return lobbyName;
	}

	public void setLobbyName(String lobbyName) {
		this.lobbyName = lobbyName;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	public String getGameModeId() {
		return gameModeId;
	}

	public void setGameModeId(String gameModeId) {
		this.gameModeId = gameModeId;
	}
}
