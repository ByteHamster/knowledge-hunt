package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

@MessageDef(id = 100)
public class DiscoveryMessage extends Message {
	public static final int PORT = 10555;
	public static final int MAX_PACKET_LENGTH = 500;
	private String name;
	private String ipAdress;
	private int port;
	private int numPlayers;
	private int numGameLobbies;
	
	public DiscoveryMessage(String name, String ipAdress, int port, int numPlayers, int numGameLobbies) {
		this.name = name;
		this.ipAdress = ipAdress;
		this.port = port;
		this.numPlayers = numPlayers;
		this.numGameLobbies = numGameLobbies;
	}
	
	public DiscoveryMessage() {
	}

	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(name);
		messageStream.write(ipAdress);
		messageStream.write(port);
		messageStream.write(numPlayers);
		messageStream.write(numGameLobbies);
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		name = messageStream.readString();
		ipAdress = messageStream.readString();
		port = messageStream.readInt();
		numPlayers = messageStream.readInt();
		numGameLobbies = messageStream.readInt();
	}

	public String getIpAdress() {
		return ipAdress;
	}

	public void setIpAdress(String ipAdress) {
		this.ipAdress = ipAdress;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getNumPlayers() {
		return numPlayers;
	}

	public void setNumPlayers(int numPlayers) {
		this.numPlayers = numPlayers;
	}

	public int getNumGameLobbies() {
		return numGameLobbies;
	}

	public void setNumGameLobbies(int numGameLobbies) {
		this.numGameLobbies = numGameLobbies;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
