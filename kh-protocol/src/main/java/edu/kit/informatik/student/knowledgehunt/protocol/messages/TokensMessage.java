package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.model.Token;
import edu.kit.informatik.student.knowledgehunt.protocol.ProtocolConstants;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

@MessageDef(id = 502)
public class TokensMessage extends Message {
	private List<Token> tokens = new ArrayList<>();
	private Player hideAllExceptThis = null;

	public TokensMessage(List<Token> tokens, Player hideAllExceptThis) {
		this.tokens = tokens;
		this.hideAllExceptThis = hideAllExceptThis;
	}

	public TokensMessage(List<Token> tokens) {
		this.tokens = tokens;
	}
	
	public TokensMessage() {
	}
	

	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(tokens.size());
		for (Token token : tokens) {
			messageStream.write(token.getId());
			if (hideAllExceptThis != null && token.getPlayerId() != hideAllExceptThis.getId()) {
				messageStream.write(ProtocolConstants.HIDDEN_TYPE);
				messageStream.write(ProtocolConstants.HIDDEN_VALUE);
				messageStream.write(ProtocolConstants.HIDDEN_DISPLAY_NAME);
			} else {
				messageStream.write(token.getType());
				messageStream.write(token.getValue());
				messageStream.write(token.getDisplayName());
			}
			messageStream.write(token.getPlayerId());
		}
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		ArrayList<Token> list = new ArrayList<>();
		int arrayLength = messageStream.readInt();
		for (int i = 0; i < arrayLength; i++) {
			Token token = new Token();
			token.setId(messageStream.readInt());
			token.setType(messageStream.readInt());
			token.setValue(messageStream.readInt());
			token.setDisplayName(messageStream.readString());
			token.setPlayerId(messageStream.readInt());
			list.add(token);
		}
		tokens = list;
	}

	public List<Token> getTokens() {
		return tokens;
	}

	public void setTokens(List<Token> tokens) {
		this.tokens = tokens;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + tokens.stream().map(Token::toString)
				.collect(Collectors.joining(","));
	}
}
