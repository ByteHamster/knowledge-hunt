package edu.kit.informatik.student.knowledgehunt.protocol.messages.meta;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

public class MessageInputStream {
	private ByteBuffer array;

	public MessageInputStream(byte[] buffer) {
		array = ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN);
	}

	public int readInt() {
		return array.getInt();
	}

	public String readString(int length) {
		byte[] buff = new byte[length];
		array.get(buff, 0, length);
		return new String(buff, Charset.forName("UTF-8"));
	}

	public String readString() {
		int length = readInt();
		return readString(length);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < array.limit(); i++) {
			builder.append(String.format("%02X ", array.get(i)));
		}
		return "MessageInputStream: " + builder.toString();
	}
}
