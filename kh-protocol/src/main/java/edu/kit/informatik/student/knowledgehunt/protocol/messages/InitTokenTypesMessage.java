package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.model.TokenType;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

import java.util.ArrayList;

@MessageDef(id = 210)
public class InitTokenTypesMessage extends Message {
	private ArrayList<TokenType> types = new ArrayList<>();
	
	
	public InitTokenTypesMessage(ArrayList<TokenType> types) {
		this.types = types;
	}
	
	public InitTokenTypesMessage() {
	}
	
	
	@Override
	public String toString() {
		return "InitTokenTypes: types=" + types;
	}
	

	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(types.size());
		for (TokenType type : types) {
			messageStream.write(type.getId());
			messageStream.write(type.getDisplayName());
			messageStream.write(type.getDescription());
		}
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		ArrayList<TokenType> list = new ArrayList<>();
		int arrayLength = messageStream.readInt();
		for (int i = 0; i < arrayLength; i++) {
			TokenType type = new TokenType();
			type.setId(messageStream.readInt());
			type.setDisplayName(messageStream.readString());
			type.setDescription(messageStream.readString());
			list.add(type);
		}
		types = list;
	}

	public ArrayList<TokenType> getTypes() {
		return types;
	}

	public void setTypes(ArrayList<TokenType> types) {
		this.types = types;
	}
}
