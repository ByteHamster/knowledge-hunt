package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

@MessageDef(id = 102)
public class ConnectionResponseMessage extends Message {
	private int requestId;
	private int playerId;
	
	public ConnectionResponseMessage(int requestId, int playerId) {
		this.requestId = requestId;
		this.playerId = playerId;
	}
	
	public ConnectionResponseMessage() {
	}
	
	@Override
	public String toString() {
		return "ConnectionResponse: playerId=" + playerId;
	}
	
	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(requestId);
		messageStream.write(playerId);
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		requestId = messageStream.readInt();
		playerId = messageStream.readInt();
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public int getPlayerId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
}
