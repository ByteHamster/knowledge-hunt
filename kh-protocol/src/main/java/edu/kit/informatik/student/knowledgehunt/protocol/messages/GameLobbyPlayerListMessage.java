package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

import java.util.ArrayList;
import java.util.List;

@MessageDef(id = 150)
public class GameLobbyPlayerListMessage extends Message {
	private List<Player> players = new ArrayList<>();

	public GameLobbyPlayerListMessage(List<Player> players) {
		this.players = players;
	}
	
	public GameLobbyPlayerListMessage() {
	}
	
	@Override
	public String toString() {
		return "GameLobbyPlayerList: players=" + players;
	}

	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(players.size());
		for (Player player : players) {
			messageStream.write(player.getId());
			messageStream.write(player.getName());
			messageStream.write(player.isReady() ? "READY" : "UNREADY");
		}
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		ArrayList<Player> list = new ArrayList<>();
		int numPlayers = messageStream.readInt();
		for (int i = 0; i < numPlayers; i++) {
			Player player = new Player();
			player.setId(messageStream.readInt());
			player.setName(messageStream.readString());
			String readyEnum = messageStream.readString();
			switch (readyEnum) {
				case "READY":
					player.setReady(true);
					break;
				case "UNREADY":
					player.setReady(false);
					break;
				default:
					throw new IllegalArgumentException("Undefined enum constant");
			}
			list.add(player);
		}
		players = list;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}
}
