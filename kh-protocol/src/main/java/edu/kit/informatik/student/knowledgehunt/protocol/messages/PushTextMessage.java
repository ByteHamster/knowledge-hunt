package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

@MessageDef(id = 503)
public class PushTextMessage extends Message {
	private String text;


	public PushTextMessage(String text) {
		this.text = text;
	}

	public PushTextMessage() {
	}
	
	
	@Override
	public String toString() {
		return "PushText: text=" + text;
	}
	

	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(text);
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		text = messageStream.readString();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
