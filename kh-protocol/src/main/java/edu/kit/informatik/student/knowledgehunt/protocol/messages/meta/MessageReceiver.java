package edu.kit.informatik.student.knowledgehunt.protocol.messages.meta;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface MessageReceiver {
	Class<? extends Message> type();
}
