package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.protocol.ProtocolConstants;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

@MessageDef(id = 550)
public class ErrorMessage extends Message {
	private int requestId;
	private int errorCode;
	private String errorMessage;

	
	public ErrorMessage(int requestId, ErrorCode code, String description) {
		this.requestId = requestId;
		this.errorCode = code.code;
		this.errorMessage = description;
	}
	
	public ErrorMessage(ErrorCode code, String description) {
		this(ProtocolConstants.INVALID_REQUEST_ID, code, description);
	}
	
	public ErrorMessage() {
	}
	
	
	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(requestId);
		messageStream.write(errorCode);
		messageStream.write(errorMessage);
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		requestId = messageStream.readInt();
		errorCode = messageStream.readInt();
		errorMessage = messageStream.readString();
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode.code;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString() {
		return getClass().getName() + ": errorCode=" + errorCode + ", errorMessage=" + errorMessage;
	}
	
	public enum ErrorCode {
		/**
		 * Sent in response to a message that is structurally invalid. This includes that
		 * the MessageId is unknown. Additionally, this includes that the specified
		 * length of a contained list does not fit into the message length.
		 */
		INVALID_MESSAGE(10),
		/**
		 * Sent in response to a message that contains a number outside of the specified
		 * range. This includes that the server sends too many locations.
		 * Additionally, it includes that the requested player name was too long.
		 */
		OUT_OF_RANGE(11),
		/**
		 * Sent in response to a message that refers to an object in the game if the given id does not exist.
		 * This includes trying to place tokens on locations that do not exist.
		 * */
		INVALID_ID(12),
		/**
		 * Sent in response to a message requesting an action that is not allowed.
		 * This includes trying to place a token that does not belong to the
		 * current user.
		 */
		NOT_ALLOWED(13),
		/**
		 * Sent in response to a message requesting an action that is currently
		 * not allowed. This includes trying to place a token that can not be placed on the location
		 * because of a game mode decision. If this message is sent, there is usually a user error.
		 */
		CURRENTLY_NOT_ALLOWED(20),
		/**
		 * Sent in response to a message that can not be sent in the current state. This can happen if a request
		 * was sent just before a state transition because all messages are processed sequentially. Therefore, if
		 * the server receives a request in an invalid state, it sends an error message with code 21 and ignores
		 * the request. All state transitions are managed by the server. If the client receives a message in an
		 * invalid state, it sends the error message and closes the socket.
		 */
		INVALID_STATE(21),
		/**
		 * Sent if the server needs to close the connection inexpectedly.
		 * This error code can be used to inform the user that the server detected an internal error
		 * or is currently shutting down. After sending this error, the server closes the socket.
		 */
		SERVER_CLOSING(30);
		
		private final int code;
		
		ErrorCode(int code) {
			this.code = code;
		}
		
		public int getCode() {
			return code;
		}
	}
	
}
