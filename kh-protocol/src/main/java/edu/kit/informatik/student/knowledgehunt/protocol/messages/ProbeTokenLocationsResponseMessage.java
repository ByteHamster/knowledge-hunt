package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

import java.util.ArrayList;
import java.util.List;

@MessageDef(id = 303)
public class ProbeTokenLocationsResponseMessage extends Message {
	private int requestId;
	private List<Integer> allowedLocations = new ArrayList<>();

	public ProbeTokenLocationsResponseMessage(int requestId, List<Integer> allowedLocations) {
		this.requestId = requestId;
		this.allowedLocations = allowedLocations;
	}

	public ProbeTokenLocationsResponseMessage() {
	}
	
	
	@Override
	public String toString() {
		return "ProbeTokenLocationsResponse: allowedLocations=" + allowedLocations;
	}
	
	
	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(requestId);
		messageStream.write(allowedLocations.size());
		for (int location : allowedLocations) {
			messageStream.write(location);
		}
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		requestId = messageStream.readInt();
		ArrayList<Integer> list = new ArrayList<>();
		int numLocations = messageStream.readInt();
		for (int i = 0; i < numLocations; i++) {
			list.add(messageStream.readInt());
		}
		allowedLocations = list;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public List<Integer> getAllowedLocations() {
		return allowedLocations;
	}

	public void setAllowedLocations(List<Integer> allowedLocations) {
		this.allowedLocations = allowedLocations;
	}
}
