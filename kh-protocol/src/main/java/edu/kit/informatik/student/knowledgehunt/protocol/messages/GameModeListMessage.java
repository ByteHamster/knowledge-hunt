package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import java.util.ArrayList;
import java.util.List;

import edu.kit.informatik.student.knowledgehunt.model.GameMode;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

@MessageDef(id = 104)
public class GameModeListMessage extends Message {
	private List<GameMode> gameModes = new ArrayList<>();

	public GameModeListMessage(List<GameMode> gameModes) {
		this.gameModes = gameModes;
	}
	
	public GameModeListMessage() {
	}
	
	
	@Override
	public String toString() {
		return "GameModeList: gameModes=" + gameModes;
	}
	
	
	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(gameModes.size());
		for (GameMode mode : gameModes) {
			messageStream.write(mode.getId());
			messageStream.write(mode.getName());
			messageStream.write(mode.getDescription());
		}
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		ArrayList<GameMode> list = new ArrayList<>();
		int numGameModes = messageStream.readInt();
		for (int i = 0; i < numGameModes; i++) {
			GameMode mode = new GameMode();
			mode.setId(messageStream.readString());
			mode.setName(messageStream.readString());
			mode.setDescription(messageStream.readString());
			list.add(mode);
		}
		gameModes = list;
	}

	public List<GameMode> getGameModes() {
		return gameModes;
	}

	public void setGameModes(List<GameMode> gameModes) {
		this.gameModes = gameModes;
	}
}
