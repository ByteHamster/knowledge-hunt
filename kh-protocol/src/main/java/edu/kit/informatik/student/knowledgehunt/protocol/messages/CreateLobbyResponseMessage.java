package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

@MessageDef(id = 107)
public class CreateLobbyResponseMessage extends Message {
	private int requestId;
	private int lobbyId;

	public CreateLobbyResponseMessage(int requestId, int lobbyId) {
		this.requestId = requestId;
		this.lobbyId = lobbyId;
	}

	public CreateLobbyResponseMessage() {
	}
	
	@Override
	public String toString() {
		return "CreateLobbyResponse: lobbyId=" + lobbyId;
	}
	
	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(requestId);
		messageStream.write(lobbyId);
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		requestId = messageStream.readInt();
		lobbyId = messageStream.readInt();
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public int getLobbyId() {
		return lobbyId;
	}

	public void setLobbyId(int lobbyId) {
		this.lobbyId = lobbyId;
	}
}
