package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.model.GameLobby;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

import java.util.ArrayList;
import java.util.List;

@MessageDef(id = 105)
public class LobbyListMessage extends Message {
	private List<GameLobby> lobbies = new ArrayList<>();

	public LobbyListMessage(List<GameLobby> lobbies) {
		this.lobbies = lobbies;
	}
	
	public LobbyListMessage() {
	}
	
	
	@Override
	public String toString() {
		return "LobbyList: lobbies=" + lobbies;
	}
	

	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(lobbies.size());
		for (GameLobby lobby : lobbies) {
			messageStream.write(lobby.getId());
			messageStream.write(lobby.getName());
			messageStream.write(lobby.getModeId());
			messageStream.write(lobby.getNumPlayers());
			messageStream.write(lobby.getMaxPlayers());
		}
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		ArrayList<GameLobby> list = new ArrayList<>();
		int numLobbies = messageStream.readInt();
		for (int i = 0; i < numLobbies; i++) {
			GameLobby lobby = new GameLobby();
			lobby.setId(messageStream.readInt());
			lobby.setName(messageStream.readString());
			lobby.setModeId(messageStream.readString());
			lobby.setNumPlayers(messageStream.readInt());
			lobby.setMaxPlayers(messageStream.readInt());
			list.add(lobby);
		}
		lobbies = list;
	}

	public List<GameLobby> getLobbies() {
		return lobbies;
	}

	public void setLobbies(List<GameLobby> lobbies) {
		this.lobbies = lobbies;
	}
}
