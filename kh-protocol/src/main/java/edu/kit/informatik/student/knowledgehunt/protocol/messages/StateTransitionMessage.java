package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

@MessageDef(id = 501)
public class StateTransitionMessage extends Message {
	private int milliseconds;
	private State nextState = State.LOBBY;

	public StateTransitionMessage(int milliseconds, State nextState) {
		this.milliseconds = milliseconds;
		this.nextState = nextState;
	}
	
	public StateTransitionMessage() {
	}
	
	
	@Override
	public String toString() {
		return "StateTransition: milliseconds=" + milliseconds + ", nextState=" + nextState;
	}
	

	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(nextState.name());
		messageStream.write(milliseconds);
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		String state = messageStream.readString();
		switch (state) {
			case "LOBBY":
				nextState = State.LOBBY;
				break;
			case "PREROUND":
				nextState = State.PREROUND;
				break;
			case "ROUND":
				nextState = State.ROUND;
				break;
			case "POSTROUND":
				nextState = State.POSTROUND;
				break;
			default:
				throw new IllegalArgumentException("Invalid enum constant: " + state);
		}
		milliseconds = messageStream.readInt();
	}

	public int getMilliseconds() {
		return milliseconds;
	}

	public void setMilliseconds(int milliseconds) {
		this.milliseconds = milliseconds;
	}

	public State getNextState() {
		return nextState;
	}

	public void setNextState(State nextState) {
		this.nextState = nextState;
	}

	public enum State {
		LOBBY, PREROUND, ROUND, POSTROUND
	}
}
