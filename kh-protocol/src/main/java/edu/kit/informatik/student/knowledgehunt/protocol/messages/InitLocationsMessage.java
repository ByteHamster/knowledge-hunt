package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.model.Location;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

import java.util.ArrayList;

@MessageDef(id = 200)
public class InitLocationsMessage extends Message {
	private ArrayList<Location> locations = new ArrayList<>();

	public InitLocationsMessage(ArrayList<Location> locations) {
		this.locations = locations;
	}
	
	public InitLocationsMessage() {
	}
	
	
	@Override
	public String toString() {
		return "InitLocations: locations=" + locations;
	}
	
	
	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(locations.size());
		for (Location location : locations) {
			messageStream.write(location.getId());
			messageStream.write(location.getDisplayName());
			messageStream.write(location.getPoints());
			messageStream.write(location.getDescription());
		}
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		ArrayList<Location> list = new ArrayList<>();
		int arrayLength = messageStream.readInt();
		for (int i = 0; i < arrayLength; i++) {
			Location location = new Location();
			location.setId(messageStream.readInt());
			location.setDisplayName(messageStream.readString());
			location.setPoints(messageStream.readInt());
			location.setDescription(messageStream.readString());
			list.add(location);
		}
		locations = list;
	}

	public ArrayList<Location> getLocations() {
		return locations;
	}

	public void setLocations(ArrayList<Location> locations) {
		this.locations = locations;
	}
}
