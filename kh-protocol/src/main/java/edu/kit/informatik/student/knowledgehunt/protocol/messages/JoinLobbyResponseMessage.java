package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

@MessageDef(id = 109)
public class JoinLobbyResponseMessage extends Message {
	private int requestId;

	public JoinLobbyResponseMessage(int requestId) {
		this.requestId = requestId;
	}
	
	public JoinLobbyResponseMessage() {
	}
	
	
	@Override
	public String toString() {
		return "JoinLobbyResponse";
	}
	

	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(requestId);
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		requestId = messageStream.readInt();
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}
}
