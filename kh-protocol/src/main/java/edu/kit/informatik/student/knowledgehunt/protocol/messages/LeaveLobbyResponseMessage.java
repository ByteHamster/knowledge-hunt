package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageOutputStream;

@MessageDef(id = 152)
public class LeaveLobbyResponseMessage extends Message {
	private int requestId;

	public LeaveLobbyResponseMessage(int requestId) {
		this.requestId = requestId;
	}

	public LeaveLobbyResponseMessage() {
	}
	
	
	@Override
	public String toString() {
		return "LeaveLobbyResponse";
	}
	
	
	@Override
	protected void writeMessagePayload(MessageOutputStream messageStream) {
		messageStream.write(requestId);
	}

	@Override
	protected void parsePayload(MessageInputStream messageStream) {
		requestId = messageStream.readInt();
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}
}
