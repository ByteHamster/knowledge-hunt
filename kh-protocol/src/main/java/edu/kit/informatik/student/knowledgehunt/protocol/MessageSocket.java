package edu.kit.informatik.student.knowledgehunt.protocol;

import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageInputStream;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageReceiver;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

public class MessageSocket {
	private static final Logger LOG = Logger.getLogger(MessageSocket.class.getName());
	private static final AtomicInteger ID_GENERATOR = new AtomicInteger();
	private static final Object MESSAGE_LISTENER_LOCK = new Object();
	private Socket socket;
	private OutputStream outputStream;
	private InputStream inputStream;
	private final int socketId;
	private Object messageListener;
	private Thread messageListenerThread;
	private boolean isClosed = false;
	private SocketErrorListener socketErrorListener;

	public MessageSocket(Socket socket) {
		this.socketId = ID_GENERATOR.incrementAndGet();
		this.socket = socket;
		try {
			outputStream = socket.getOutputStream();
			inputStream = socket.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public String toString() {
		return "Message" + socket.toString();  // socket.toString() starts with "Socket"
	}

	public void writeMessage(Message message) {
		LOG.info("Sending message to " + socketId + ": " + message.toString());
		try {
			outputStream.write(message.getBytes());
		} catch (IOException e) {
			if (socketErrorListener != null) {
				socketErrorListener.socketError(e);
			} else {
				e.printStackTrace();
			}
		}
	}

	public void close() {
		try {
			isClosed = true;
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (messageListenerThread != null) {
			try {
				messageListenerThread.interrupt();
				messageListenerThread.join();
			} catch (InterruptedException e) {
				// will always be thrown if messageListenerThread joins itself
//				e.printStackTrace();
			}
		}
	}

	public int getSocketId() {
		return socketId;
	}

	public void listen() {
		if (messageListenerThread != null) {
			throw new IllegalArgumentException("Listen was already called");
		}
		messageListenerThread = new Thread(() -> {
			while (true) {
				try {
					receiveOneMessage();
				} catch (IOException e) {
					if (!isClosed) {
						if (socketErrorListener != null) {
							socketErrorListener.socketError(e);
						} else {
							e.printStackTrace();
						}
					}
					return;
				}
			}
		});
		messageListenerThread.start();
	}

	public void checkSingleNewMessage() {
		try {
			if (inputStream.available() >= Message.HEADER_LENGTH) {
				receiveOneMessage();
			}
		} catch (IOException e) {
			if (socketErrorListener != null) {
				socketErrorListener.socketError(e);
			} else {
				e.printStackTrace();
			}
		}
	}

	public void receiveOneMessage() throws IOException {
		int bufferSize = Message.HEADER_LENGTH;
		byte[] message = new byte[bufferSize];
		boolean headerRead = false;
		int totalRead = 0;

		while (totalRead != bufferSize) {
			int read = inputStream.read(message, totalRead, bufferSize - totalRead);
			if (read == -1) {
				throw new SocketException("Socket was closed");
			}
			totalRead += read;

			if (totalRead >= Message.HEADER_LENGTH && !headerRead) {
				headerRead = true;
				byte[] header = message;
				MessageInputStream headerReader = new MessageInputStream(header);
				headerReader.readInt(); // Message Id is not parsed here
				int bodyLength = headerReader.readInt();
				bufferSize = bodyLength + Message.HEADER_LENGTH;
				message = new byte[bufferSize];
				System.arraycopy(header, 0, message, 0, Message.HEADER_LENGTH);
			}
		}
		handleNewMessage(message);
	}

	public void setMessageListener(Object messageListener) {
		this.messageListener = messageListener;
	}

	private void handleNewMessage(byte[] encodedMessage) {
		Message message = Message.parse(encodedMessage);
		message.setSocketId(socketId);
		LOG.info("Handling message from " + socketId + ": " + message.toString());

		if (messageListener != null) {
			Method method = findReceiverWithType(message.getClass());
			if (method == null) {
				method = findReceiverWithType(Message.class);
			}
			if (method != null) {
				try {
					synchronized (MESSAGE_LISTENER_LOCK) {
						method.invoke(messageListener, message);
					}
				} catch (IllegalAccessException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private Method findReceiverWithType(Class type) {
		Class whereToSearch = messageListener.getClass();
		while (whereToSearch.getPackage().getName().startsWith("edu.kit.informatik.student.knowledgehunt")) {
			for (Method method : whereToSearch.getMethods()) {
				MessageReceiver messageReceiver = method.getAnnotation(MessageReceiver.class);
				if (messageReceiver != null && messageReceiver.type() == type) {
					return method;
				}
			}
			whereToSearch = whereToSearch.getSuperclass();
		}
		return null;
	}

	public void setSocketErrorListener(SocketErrorListener socketErrorListener) {
		this.socketErrorListener = socketErrorListener;
	}

	@FunctionalInterface
	public interface SocketErrorListener {
		void socketError(Exception e);
	}
}
