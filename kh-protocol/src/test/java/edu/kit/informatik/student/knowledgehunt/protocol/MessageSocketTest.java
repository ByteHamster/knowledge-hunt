package edu.kit.informatik.student.knowledgehunt.protocol;

import edu.kit.informatik.student.knowledgehunt.protocol.messages.ErrorMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.SetTokenRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageReceiver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class MessageSocketTest {
	private static final int PORT = 4321;
	private ServerSocket server;
	private MessageSocket socket1;
	private MessageSocket socket2;
	private CountDownLatch messageReceived;

	@Before
	public void setup() throws IOException {
		messageReceived = new CountDownLatch(1);
		server = new ServerSocket(PORT);

		socket2 = new MessageSocket(new Socket("127.0.0.1", PORT));
		socket1 = new MessageSocket(server.accept());

		socket2.setMessageListener(this);
	}

	@After
	public void tearDown() throws IOException {
		socket2.setMessageListener(null);
		socket1.close();
		socket2.close();
		server.close();
	}

	@Test
	public void testMessageTransfer() throws InterruptedException {
		ErrorMessage message = new ErrorMessage();
		message.setErrorCode(42);

		socket1.writeMessage(message);
		socket2.checkSingleNewMessage();

		assertTrue(messageReceived.await(5, TimeUnit.SECONDS));
	}

	@Test
	public void testReceiveUnknownMessage() throws InterruptedException {
		SetTokenRequestMessage message = new SetTokenRequestMessage();

		socket1.writeMessage(message);
		socket2.checkSingleNewMessage();

		assertTrue(messageReceived.await(5, TimeUnit.SECONDS));
	}

	@Test
	public void testMultipleMessages() throws InterruptedException {
		messageReceived = new CountDownLatch(2);
		ErrorMessage message = new ErrorMessage();
		message.setErrorCode(42);
		ErrorMessage message2 = new ErrorMessage();
		message2.setErrorCode(43);

		socket1.writeMessage(message);
		socket1.writeMessage(message2);
		socket2.checkSingleNewMessage();
		socket2.checkSingleNewMessage();

		assertTrue(messageReceived.await(5, TimeUnit.SECONDS));
	}

	@Test
	public void testMessageListener() throws InterruptedException {
		socket2.listen();

		ErrorMessage message = new ErrorMessage();
		message.setErrorCode(42);
		socket1.writeMessage(message);

		assertTrue(messageReceived.await(5, TimeUnit.SECONDS));
	}

	@MessageReceiver(type = ErrorMessage.class)
	public void receiveErrorMessage(ErrorMessage message) {
		System.out.println("Received errorMessage");
		messageReceived.countDown();
	}

	@MessageReceiver(type = Message.class)
	public void receiveUnknownMessage(Message message) {
		System.out.println("Received message");
		messageReceived.countDown();

		if (message instanceof ErrorMessage) {
			fail("ErrorMessage ended up in default message receiver even though it has its own receiver");
		}
	}
}
