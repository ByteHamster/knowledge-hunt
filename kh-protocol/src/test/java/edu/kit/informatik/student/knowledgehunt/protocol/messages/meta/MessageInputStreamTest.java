package edu.kit.informatik.student.knowledgehunt.protocol.messages.meta;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MessageInputStreamTest {
	@Test
	public void testReadInteger() {
		MessageInputStream stream = new MessageInputStream(new byte[]{ 42, 0, 0, 0 });
		assertEquals(42, stream.readInt());
	}

	@Test
	public void testReadLargeInteger() {
		MessageInputStream stream = new MessageInputStream(new byte[]{ 42, 1, 0, 0 });
		assertEquals(42 + 256, stream.readInt());
	}

	@Test
	public void testReadString() {
		MessageInputStream stream = new MessageInputStream(new byte[]{
				2, 0, 0, 0, // Length
				0x78, 0x79  // UTF-8 encoded characters "xy"
		});
		assertEquals("xy", stream.readString());
	}

	@Test
	public void testReadEmptyString() {
		MessageInputStream stream = new MessageInputStream(new byte[]{
				0, 0, 0, 0, // Length o
		});
		assertEquals("", stream.readString());
	}

	@Test
	public void testMultiByteCharacter() {
		final String emoji = "😂";
		MessageOutputStream outputStream = new MessageOutputStream();
		outputStream.write(emoji);
		byte[] bytes = outputStream.toByteArray();

		assertEquals(8, bytes.length);

		MessageInputStream inputStream = new MessageInputStream(bytes);
		String string = inputStream.readString();

		assertEquals(emoji, string);
	}
}
