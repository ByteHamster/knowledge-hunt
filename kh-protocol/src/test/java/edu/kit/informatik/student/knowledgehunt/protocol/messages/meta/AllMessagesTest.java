package edu.kit.informatik.student.knowledgehunt.protocol.messages.meta;

import edu.kit.informatik.student.knowledgehunt.protocol.annotations.MessageDef;
import org.junit.Test;
import org.reflections.Reflections;

import java.util.Set;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;

public class AllMessagesTest {
	private static final Logger LOG = Logger.getLogger(AllMessagesTest.class.getName());

	@Test
	public void encodeDecode() {
		Reflections reflections = new Reflections("edu.kit.informatik.student.knowledgehunt.protocol.messages");
		Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(MessageDef.class);
		for (Class<?> clazz : annotated) {
			LOG.info("Testing encode/decode of " + clazz.getSimpleName());
			try {
				Message message = (Message) clazz.newInstance();
				byte[] encodedMessage = message.getBytes();
				Message newMessage = Message.parse(encodedMessage);

				assertEquals(message.getClass(), newMessage.getClass());
			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
}
