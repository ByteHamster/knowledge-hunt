package edu.kit.informatik.student.knowledgehunt.protocol.messages.meta;

import edu.kit.informatik.student.knowledgehunt.protocol.messages.ErrorMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.SetTokenRequestMessage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class MessageTest {
	@Test
	public void testMessageTypeSetToken() {
		byte[] messageBytes = {
				(byte) 44, 1, 0, 0, // Type of SetTokenRequestMessage
				0, 0, 0, 3, // Length
				0, 0, 0, 0,  // request
				0, 0, 0, 0,  // token
				0, 0, 0, 0  // location
		};
		MessageInputStream messageInputStream = new MessageInputStream(messageBytes);
		Message message = Message.parse(messageInputStream);

		assertTrue(message instanceof SetTokenRequestMessage);
	}

	@Test
	public void testMessageTypeError() {
		byte[] messageBytes = {
				(byte) 38, 2, 0, 0, // Type of ErrorMessage
				3, 0, 0, 0, // Length
				0, 0, 0, 0, // requestId
				0, 0, 0, 0, // errorCode
				0, 0, 0, 0  // error message length
		};
		MessageInputStream messageInputStream = new MessageInputStream(messageBytes);
		Message message = Message.parse(messageInputStream);

		assertTrue(message instanceof ErrorMessage);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testMessageTooShort() {
		byte[] messageBytes = {
				(byte) 38, 2, 0, 0, // Type of ErrorMessage
				10, 0, 0, 0, // Length
		};
		MessageInputStream messageInputStream = new MessageInputStream(messageBytes);
		Message.parse(messageInputStream);
	}
}
