package edu.kit.informatik.student.knowledgehunt.protocol.messages.meta;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class MessageOutputStreamTest {
	@Test
	public void testEmptyStream() {
		MessageOutputStream stream = new MessageOutputStream();
		assertEquals(0, stream.toByteArray().length);
	}

	@Test
	public void testWriteInteger() {
		MessageOutputStream stream = new MessageOutputStream();
		stream.write(42);
		assertArrayEquals(
				new byte[]{ 42, 0, 0, 0 },
				stream.toByteArray());
	}

	@Test
	public void testWriteLargeInteger() {
		MessageOutputStream stream = new MessageOutputStream();
		stream.write(42 + 256);
		assertArrayEquals(
				new byte[]{ 42, 1, 0, 0 },
				stream.toByteArray());
	}

	@Test
	public void testWriteString() {
		MessageOutputStream stream = new MessageOutputStream();
		stream.write("xy");
		assertArrayEquals(
				new byte[]{
						2, 0, 0, 0, // Length
						0x78, 0x79  // UTF-8 encoded characters
				},
				stream.toByteArray());
	}

	@Test
	public void testWriteEmptyString() {
		MessageOutputStream stream = new MessageOutputStream();
		stream.write("");
		assertArrayEquals(
				new byte[]{
						0, 0, 0, 0, // Length
				},
				stream.toByteArray());
	}

	@Test
	public void testWriteNullString() {
		MessageOutputStream stream = new MessageOutputStream();
		stream.write(null);
		assertArrayEquals(
				new byte[]{
						0, 0, 0, 0, // Length
				},
				stream.toByteArray());
	}
}
