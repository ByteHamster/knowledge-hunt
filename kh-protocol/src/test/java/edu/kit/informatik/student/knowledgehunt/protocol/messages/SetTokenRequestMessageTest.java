package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SetTokenRequestMessageTest {
	@Test
	public void encodeDecode() {
		SetTokenRequestMessage message = new SetTokenRequestMessage();
		message.setTokenId(42);

		byte[] encodedMessage = message.getBytes();
		SetTokenRequestMessage decodedMessage = (SetTokenRequestMessage) Message.parse(encodedMessage);

		assertEquals(42, decodedMessage.getTokenId());
	}
}
