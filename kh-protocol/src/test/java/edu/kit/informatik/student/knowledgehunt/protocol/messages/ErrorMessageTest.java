package edu.kit.informatik.student.knowledgehunt.protocol.messages;

import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ErrorMessageTest {
	@Test
	public void encodeDecode() {
		ErrorMessage message = new ErrorMessage();
		message.setErrorCode(404);

		byte[] encodedMessage = message.getBytes();
		ErrorMessage decodedMessage = (ErrorMessage) Message.parse(encodedMessage);

		assertEquals(404, decodedMessage.getErrorCode());
	}
}
