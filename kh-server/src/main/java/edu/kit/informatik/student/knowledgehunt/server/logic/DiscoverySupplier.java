package edu.kit.informatik.student.knowledgehunt.server.logic;

/**
 * A DiscoverySupplier allows to retrieve up-to-date information about the
 * server which will be used for the discovery messages.
 */
public interface DiscoverySupplier {
	
	int getNumPlayers();
	
	int getNumLobbies();
	
}
