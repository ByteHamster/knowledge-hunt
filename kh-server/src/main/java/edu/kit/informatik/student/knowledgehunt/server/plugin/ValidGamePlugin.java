package edu.kit.informatik.student.knowledgehunt.server.plugin;

import edu.kit.informatik.student.knowledgehunt.util.plugin.GameInitializationException;
import edu.kit.informatik.student.knowledgehunt.util.plugin.GamePlugin;

/**
 * A plugin whose methods return only valid objects.
 * <p>
 * Exceptions might still be thrown.
 */
public interface ValidGamePlugin extends GamePlugin {
	
	ValidGamePlugin copy(Object...args) throws GameInitializationException;
	
}
