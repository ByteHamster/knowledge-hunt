package edu.kit.informatik.student.knowledgehunt.server.plugin;


public class IllegalPluginBehaviourException extends RuntimeException {
	
	private static final long serialVersionUID = 6140770520596222469L;
	
	public IllegalPluginBehaviourException() {
	}
	
	public IllegalPluginBehaviourException(String message) {
		super(message);
	}
	
	public IllegalPluginBehaviourException(Throwable cause) {
		super(cause);
	}
	
	public IllegalPluginBehaviourException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public IllegalPluginBehaviourException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
	
}
