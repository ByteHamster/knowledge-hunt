package edu.kit.informatik.student.knowledgehunt.server.plugin;

import java.util.jar.JarEntry;

/**
 * A filter for the entries of a jar file.
 * @author Jonas
 * @version 1.0
 */
interface JarEntryFilter {
	
	/**
	 * Returns wether or not this entry is searched.
	 * @param entry the entry to be tested
	 * @return <code>true</code> if and only if this entry is searched
	 */
	boolean accept(JarEntry entry);
	
}
