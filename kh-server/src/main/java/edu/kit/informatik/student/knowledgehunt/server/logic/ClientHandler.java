package edu.kit.informatik.student.knowledgehunt.server.logic;

import java.net.SocketException;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.protocol.MessageSocket;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;

/**
 * A class which represents a client.
 * It acts as wrapper for a {@link MessageSocket} and holds the dedicated
 * {@link Player} object.
 * It also contains some server specific information.
 */
public class ClientHandler {
	
	private static final Logger LOG = Logger.getLogger(ClientHandler.class.getName());
	private final Player player;
	private final MessageSocket socket;
	private Consumer<ClientHandler> disconnectHandler;
	private LobbyState lobbyState;
	private volatile boolean closed = false;
	
	public ClientHandler(Player player, MessageSocket socket) {
		this.player = player;
		this.socket = socket;

		socket.setSocketErrorListener(e -> {
			if (!(e instanceof SocketException)) {  // don't output if socket was closed
				LOG.log(Level.SEVERE, e.getMessage(), e);
			}
			disconnect();
		});
	}
	
	@Override
	public String toString() {
		return "client " + player.getId() + ": " + (player.getName() == null ? "None" : player.getName())
				+ " " + socket;
	}
	
	public void start() {
		socket.listen();
	}
	
	
	public boolean sendMessage(Message message) {
		LOG.finest("send message: " + message);
		socket.writeMessage(message);
		return !closed;
	}
	
	
	public void setMessageListener(MessageListener messageListener) {
		socket.setMessageListener(messageListener);
	}
	
	/**
	 * Sets the disconnect handler which is called when the connection to this client
	 * is being closed or already closed.
	 * @param disconnectHandler consumer called on disconnect
	 */
	public void setDisconnectHandler(Consumer<ClientHandler> disconnectHandler) {
		this.disconnectHandler = disconnectHandler;
	}
	
	public void disconnect() {
		if (closed) {
			return;
		}
		LOG.info(toString() + " disconnected");
		// set before everything else, otherwise the following instructions might try to use this handler and fail
		// => this method would be called twice and may lead to a deadlock while acquiring ServerLobbyHandler#clients
		closed = true;
		disconnectHandler.accept(this);
		socket.close();
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public LobbyState getLobbyState() {
		return lobbyState;
	}
	
	public void setLobbyState(LobbyState lobbyState) {
		this.lobbyState = lobbyState;
	}
	
	
	@Override
	public int hashCode() {
		return Objects.hash(player);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ClientHandler other = (ClientHandler) obj;
		return Objects.equals(player, other.player);
	}
	
}
