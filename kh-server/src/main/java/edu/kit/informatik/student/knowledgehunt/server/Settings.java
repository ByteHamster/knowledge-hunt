package edu.kit.informatik.student.knowledgehunt.server;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Base class for other settings classes.
 * <p>
 * This class allows us to load settings from a string file which is
 * formatted according to the {@link Properties#load(InputStream)} method.
 * 
 */
public class Settings {
	
	private static final Logger LOG = Logger.getLogger(Settings.class.getName());
	
	private static final Map<Class<?>, Class<?>> WRAPPER_MAP = new HashMap<>();
	static {
		WRAPPER_MAP.put(boolean.class, Boolean.class);
		WRAPPER_MAP.put(byte.class, Byte.class);
		WRAPPER_MAP.put(short.class, Short.class);
		WRAPPER_MAP.put(char.class, Character.class);
		WRAPPER_MAP.put(int.class, Integer.class);
		WRAPPER_MAP.put(long.class, Long.class);
		WRAPPER_MAP.put(float.class, Float.class);
		WRAPPER_MAP.put(double.class, Double.class);
	}
	
	private final String path;
	
	
	public Settings(String path) {
		this.path = path;
	}
	
	
	public void load() {
		Properties properties = new Properties();
		
		try (InputStream in = Settings.class.getClassLoader().getResourceAsStream(path)) {
			if (in == null) {
				LOG.info("settings file not found");
				return;
			}
			properties.load(in);
		} catch (IOException e) {
			LOG.log(Level.WARNING, e.getMessage(), e);
			return;
		}
		
		
		for (Class<?> cls = this.getClass(); Settings.class.isAssignableFrom(cls); cls = cls.getSuperclass()) {
			for (String key : properties.stringPropertyNames()) {
				try {
					Field field = cls.getDeclaredField(key);
					
					if ((field.getModifiers() & (Modifier.FINAL + Modifier.STATIC)) != 0) {
						continue;
					}
					
					field.setAccessible(true);
					
					Class<?> type = field.getType();
					String value = properties.getProperty(key);
					
					// set value of field according to the type of the field
					if (type == String.class) {
						field.set(this, value);
					} else if (type == char.class) {
						if (value.length() != 1) {
							throw new IllegalArgumentException("value must be only one character to be cast to char");
						}
						field.setChar(this, value.charAt(0));
					} else if (type.isPrimitive()) {
						// the constructor of the primitive wrapper class will parse the string
						field.set(this, WRAPPER_MAP.get(type).getConstructor(String.class).newInstance(value));
					} else {
						// no match => try to use a constructor which takes the property value as single argument
						// this may fail, in this case, the value of the field will not be changed
						field.set(this, type.getConstructor(String.class).newInstance(value));
					}
				} catch (NoSuchMethodException e) {
					LOG.log(Level.SEVERE, e.getMessage(), e);
				} catch (ReflectiveOperationException | SecurityException | IllegalArgumentException e) {
				}
			}
		}
	}
	
}
