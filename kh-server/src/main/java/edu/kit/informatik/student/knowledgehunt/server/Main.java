package edu.kit.informatik.student.knowledgehunt.server;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.kit.informatik.student.knowledgehunt.server.logic.ServerHandler;
import edu.kit.informatik.student.knowledgehunt.server.logic.ServerLobbyHandler;
import edu.kit.informatik.student.knowledgehunt.server.plugin.PluginLoader;
import edu.kit.informatik.student.knowledgehunt.util.LogConfiguration;
import edu.kit.informatik.student.knowledgehunt.util.plugin.GamePlugin;

public final class Main {
	
	private static final Logger LOG = Logger.getLogger(Main.class.getName());
	
	private static ServerLobbyHandler serverLobby;
	private static ServerHandler server;
	private static boolean initializedLogger = false;
	
	private Main() {
	}
	
	
	public static void main(String[] args) {
		Runtime.getRuntime().addShutdownHook(new Thread(Main::stop));
		
		initializeLogger();
		

		if (args.length >= 1) {
			ServerSettings.setPluginDirectory(new File(args[0]));
		}
		
		try {
			start();
		} catch (IOException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
			System.exit(1);
		}
	}
	
	
	private static void initializeLogger() {
		initializedLogger = true;
		LogConfiguration.initialize(Level.ALL, true);
		LogConfiguration.addFile(new File(ServerSettings.getLoggingDir(), "log_all.txt"), Level.ALL);
		LogConfiguration.addFile(new File(ServerSettings.getLoggingDir(), "log_warning.txt"), Level.WARNING);
	}
	
	
	public static void start() throws IOException {
		Set<GamePlugin> plugins;
		plugins = PluginLoader.loadPlugins(ServerSettings.getPluginDirectory(), GamePlugin.class);

		if (plugins.size() == 0) {
			LOG.severe("No plugins found. Starting server without game modes.");
		}
		
		serverLobby = new ServerLobbyHandler(plugins);
		
		server = new ServerHandler(ServerSettings.getPort(), serverLobby.getDiscoverySupplier(),
				newClient -> serverLobby.connected(newClient));
		server.start();
	}
	
	
	public static void stop() {
		LOG.info("stop...");
		
		if (serverLobby != null) {
			serverLobby.close();
		}
		
		if (server != null) {
			server.interrupt();
		}
		
		if (initializedLogger) {
			LogConfiguration.close();
			initializedLogger = false;
		}
	}
	
}
