package edu.kit.informatik.student.knowledgehunt.server.plugin;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.kit.informatik.student.knowledgehunt.util.plugin.GameInitializationException;
import edu.kit.informatik.student.knowledgehunt.util.plugin.GamePlugin;

public final class PluginLoader {
	
	private static final Logger LOG = Logger.getLogger(PluginLoader.class.getName());
	private static final int ALLOWED_MODIFIERS = Modifier.PUBLIC | Modifier.FINAL;
	
	private PluginLoader() {
	}
	
	/**
	 * Loads all plugins in a specific directory by searching for a specific class.
	 * <p>
	 * An IOException will be thrown if this abstract pathname does not denote a directory,
	 * or if an I/O error occurs when reading the directory.
	 * <p>
	 * If an IOException occurs while reading one of the jar files, it will be ignored
	 * and this method will continue with the next file.
	 * 
	 * @param <T> searched superclass
	 * @param dir directory of plugins
	 * @param cls class object of the searched class
	 * @return list of all subclasses of the given class
	 * @throws IOException if this abstract pathname does not denote a directory,
	 * 				or if an I/O error occurs when reading the directory
	 */
	public static <T> Set<T> loadPlugins(File dir, Class<T> cls) throws IOException {
		File[] files = dir.listFiles((d, name) -> name.endsWith(".jar"));
		
		if (files == null) {
			throw new IOException("I/O error occurred while reading plugin directory " + dir.getAbsolutePath());
		}
		
		Set<T> plugins = new HashSet<>(files.length);
		
		for (File file : files) {
			try (JarLoader jarLoader = new JarLoader(file)) {
				jarLoader.loadClasses(cls).stream().sequential()
					.filter(c -> (c.getModifiers() | ALLOWED_MODIFIERS) == ALLOWED_MODIFIERS)
					.filter(c -> !(c.isEnum() | c.isAnnotation() | c.isArray() | c.isPrimitive()))
					.map(PluginLoader::instantiate)
					.forEach(plugins::add);
			} catch (IOException e) {
				LOG.log(Level.WARNING, e.getMessage(), e);
			}
		}
		
		return plugins;
	}
	
	
	private static <T> T instantiate(Class<? extends T> cls, Object...args) {
		Class<?>[] classes = new Class[args.length];
		
		for (int i = 0; i < classes.length; i++) {
			classes[i] = args[i].getClass();
		}
		
		try {
			return cls.getConstructor(classes).newInstance(args);
		} catch (ReflectiveOperationException | IllegalArgumentException e) {
			return null;
		}
	}
	
	
	public static GamePlugin reinstantiatePlugin(GamePlugin plugin, Object...args) throws GameInitializationException {
		Class<?>[] classes = new Class[args.length];
		
		for (int i = 0; i < classes.length; i++) {
			classes[i] = args[i].getClass();
		}
		
		try {
			return plugin.getClass().getConstructor(classes).newInstance(args);
		} catch (ReflectiveOperationException | IllegalArgumentException e) {
			throw new GameInitializationException(e);
		}
	}
	
}
