package edu.kit.informatik.student.knowledgehunt.server.logic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import edu.kit.informatik.student.knowledgehunt.model.GameLobby;
import edu.kit.informatik.student.knowledgehunt.model.GameMode;
import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.protocol.ProtocolConstants;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ConnectionRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ConnectionResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.CreateLobbyRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.CreateLobbyResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ErrorMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ErrorMessage.ErrorCode;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.GameModeListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.JoinLobbyRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.LobbyListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.PlayerListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.server.plugin.PluginValidator;
import edu.kit.informatik.student.knowledgehunt.server.plugin.ValidGamePlugin;
import edu.kit.informatik.student.knowledgehunt.util.plugin.GamePlugin;

/**
 * This class handles all new clients and all clients in the server lobby.
 * It is thus also responsible for creating new {@link GameLobby lobbies}
 * and managing the names of the lobbies and players.
 */
public class ServerLobbyHandler {
	
	private static final Logger LOG = Logger.getLogger(ServerLobbyHandler.class.getName());
	private final Set<GameMode> modes = new HashSet<>();
	private final Map<String, ValidGamePlugin> plugins = new HashMap<>();
	private final Map<Integer, GameLobbyHandler> lobbies = Collections.synchronizedMap(new HashMap<>());
	private final NameManager nameManager = new NameManager();
	/** This set contains ALL currently connected clients. */
	private final Set<ClientHandler> clients = Collections.synchronizedSet(new HashSet<>());
	private final AtomicInteger lobbyIdCounter = new AtomicInteger();
	private final ServerLobbyCallback callback = new ServerLobbyCallback();
	
	
	public ServerLobbyHandler(Collection<GamePlugin> plugins) {
		AtomicInteger id = new AtomicInteger(0);
		
		for (GamePlugin p : plugins) {
			PluginValidator plugin = new PluginValidator(p);
			GameMode mode = plugin.getGameMode();
			if (mode == null) {
				continue;
			}
			mode.setId("" + id.getAndIncrement());
			this.modes.add(mode);
			this.plugins.put(mode.getId(), plugin);
		}
	}
	
	/**
	 * Try to start a new lobby with the given game mode id.
	 * @param gameModeId id of the game mode of the new lobby
	 * @param name name of the new lobby
	 * @param maxPlayers maximum number of players
	 * @return id of the new lobby
	 * @throws IllegalArgumentException if there is no game mode with the given id
	 */
	public int startNewLobby(String gameModeId, String name, int maxPlayers) {
		LOG.info("create new lobby with game mode id: " + gameModeId);
		ValidGamePlugin gamePlugin = plugins.get(gameModeId);
		
		if (gamePlugin == null) {
			throw new IllegalArgumentException("no plugin found for the given game mode id");
		}
		
		if (!nameManager.registerLobbyName(name)) {
			throw new IllegalArgumentException("lobby name already in use or invalid");  //_TODO bad style?
		}
		
		int id = lobbyIdCounter.getAndIncrement();
		GameMode gameMode = gamePlugin.getGameMode();
		
		if (gameMode == null) {
			throw new IllegalArgumentException("Game plugin is invalid");
		}
		
		GameLobby gameLobby = new GameLobby(id, gameMode,
				Math.min(gamePlugin.getMaxNumPlayers(), maxPlayers));
		gameLobby.setName(name);
		GameLobbyHandler handler = new GameLobbyHandler(gameLobby, gamePlugin, callback);
		lobbies.put(id, handler);
		
		sendToClientsInLobby(getLobbyListMessage(), false);
		
		LOG.info("lobby created with lobbyId: " + id);
		
		return id;
	}
	
	
	private void removeLobby(GameLobbyHandler gameLobbyHandler) {
		lobbies.remove(gameLobbyHandler.getId());
		sendToClientsInLobby(getLobbyListMessage(), false);
	}
	
	
	public void connected(ClientHandler clientHandler) {
		clients.add(clientHandler);
		clientHandler.setLobbyState(LobbyState.CONNECTION_NOT_COMPLETED);
		clientHandler.setDisconnectHandler(this::disconnected);
		clientHandler.setMessageListener(new ConnectMessageListener(clientHandler));
		clientHandler.start();
	}
	
	private void disconnected(ClientHandler clientHandler) {
		nameManager.deregisterPlayerName(clientHandler.getPlayer().getName());
		clients.remove(clientHandler);
		playersUpdated();
	}
	
	
	public void close() {
		LOG.info("closing ServerLobbyHandler...");
		synchronized (clients) {
			for (ClientHandler clientHandler : clients) {
				clientHandler.setDisconnectHandler(ch -> { });
				clientHandler.disconnect();
			}
		}
	}
	
	
	public DiscoverySupplier getDiscoverySupplier() {
		return new DiscoverySupplier() {
			@Override
			public int getNumPlayers() {
				return clients.size();
			}
			
			@Override
			public int getNumLobbies() {
				return lobbies.size();
			}
		};
	}
	
	
	private void sendToClientsInLobby(Message message, boolean failIfAnyClientIsClosed) {
		synchronized (clients) {
			// copy clients because a failure in ClientHandler#sendMessage will disconnect the client
			// and thus remove the client from the Set, which will result in a ConcurrentModificationException
			Set<ClientHandler> clientsCopy = new HashSet<>(clients);
			for (ClientHandler client : clientsCopy) {
				if (client.getLobbyState() == LobbyState.SERVER_LOBBY) {
					boolean successfull = client.sendMessage(message);
					if (!successfull && failIfAnyClientIsClosed) {
						return;
					}
				}
			}
		}
	}
	
	
	private void playersUpdated() {
		List<Player> playerList;
		synchronized (clients) {
			playerList = clients.stream()
					.map(ClientHandler::getPlayer)
					.filter(player -> player.getName() != null) // only players who did sent a ConnectionRequest
					.collect(Collectors.toList());
		}
		
		sendToClientsInLobby(new PlayerListMessage(playerList), true);
	}
	
	
	private LobbyListMessage getLobbyListMessage() {
		synchronized (lobbies) {
			return new LobbyListMessage(lobbies.values().stream()
					.map(GameLobbyHandler::getGameLobby).collect(Collectors.toList()));
		}
	}
	
	
	final class ServerLobbyCallback {
		
		void lobbyClosed(GameLobbyHandler gameLobbyHandler) {
			removeLobby(gameLobbyHandler);
			nameManager.deregisterLobbyName(gameLobbyHandler.getGameLobby().getName());
		}
		
		void returnToLobby(ClientHandler clientHandler) {
			clientHandler.setDisconnectHandler(this::disconnected);
			clientHandler.setMessageListener(new ServerLobbyMessageListener(clientHandler));
			clientHandler.setLobbyState(LobbyState.SERVER_LOBBY);
			clientHandler.getPlayer().setLobbyId(ProtocolConstants.HIDDEN_VALUE);
			sendToClientsInLobby(getLobbyListMessage(), false);
			playersUpdated();
		}
		
		void disconnected(ClientHandler clientHandler) {
			ServerLobbyHandler.this.disconnected(clientHandler);
		}
		
	}
	
	
	private final class ConnectMessageListener extends MessageListener {
		
		private ConnectMessageListener(ClientHandler clientHandler) {
			super(clientHandler);
		}
		
		
		@Override
		public void receiveConnectionRequestMessage(ConnectionRequestMessage message) {
			LOG.finest("received ConnectionRequestMessage from " + clientHandler);
			Player player = clientHandler.getPlayer();
			String playerName = message.getPlayerName();
			
			if (nameManager.registerPlayerName(playerName)) {
				player.setName(playerName);
				player.setLobbyId(ProtocolConstants.HIDDEN_VALUE);
			} else {
				clientHandler.sendMessage(new ErrorMessage(ErrorCode.CURRENTLY_NOT_ALLOWED,
						"name already in use or invalid"));
				disconnected(clientHandler);
				return;
			}
			
			clientHandler.setLobbyState(LobbyState.SERVER_LOBBY);
			clientHandler.sendMessage(new ConnectionResponseMessage(message.getRequestId(), player.getId()));
			clientHandler.sendMessage(getLobbyListMessage());
			clientHandler.sendMessage(new GameModeListMessage(new ArrayList<>(modes)));
			clientHandler.setMessageListener(new ServerLobbyMessageListener(clientHandler));
			
			playersUpdated();
		}
		
	}
	
	
	private final class ServerLobbyMessageListener extends MessageListener {
		
		private ServerLobbyMessageListener(ClientHandler clientHandler) {
			super(clientHandler);
		}
		
		@Override
		public void receiveCreateLobbyRequestMessage(CreateLobbyRequestMessage message) {
			LOG.finest("received CreateLobbyRequestMessage from " + clientHandler);
			int id = -1;
			
			try {
				id = startNewLobby(message.getGameModeId(), message.getLobbyName(), message.getMaxPlayers());
				clientHandler.sendMessage(new CreateLobbyResponseMessage(message.getRequestId(), id));
			} catch (IllegalArgumentException e) {
				clientHandler.sendMessage(new ErrorMessage(ErrorCode.INVALID_ID, e.getMessage()));
			}
		}
		
		@Override
		public void receiveJoinLobbyRequestMessage(JoinLobbyRequestMessage message) {
			LOG.finest("received JoinLobbyRequestMessage from " + clientHandler);
			GameLobbyHandler gameLobbyHandler = lobbies.get(message.getLobbyId());
			if (gameLobbyHandler != null) {
				gameLobbyHandler.join(clientHandler, message.getRequestId());
			} else {
				clientHandler.sendMessage(new ErrorMessage(message.getRequestId(), ErrorCode.INVALID_ID,
						"unknown lobby id"));
			}
		}
		
	}
	
}
