package edu.kit.informatik.student.knowledgehunt.server.logic;

import java.util.logging.Logger;

import edu.kit.informatik.student.knowledgehunt.protocol.messages.ConnectionRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.CreateLobbyRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ErrorMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ErrorMessage.ErrorCode;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.JoinLobbyRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.LeaveLobbyRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ProbeTokenLocationsRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ReadyStatusMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.SetTokenRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageReceiver;

/**
 * Base class for all classes that listen to messages received from a client.
 * <p>
 * By default, all methods will sent an {@link ErrorMessage} with the error code
 * {@link ErrorCode#INVALID_STATE invalid state}.
 * <p>
 * A subclass must override all methods that receive messages which are
 * allowed in the current state.
 */
public class MessageListener {
	
	private static final Logger LOG = Logger.getLogger(MessageListener.class.getName());
	protected final ClientHandler clientHandler;
	
	public MessageListener(ClientHandler clientHandler) {
		this.clientHandler = clientHandler;
	}
	
	
	@MessageReceiver(type = Message.class)
	public void receiveUnkownMessage(Message message) {
		clientHandler.sendMessage(new ErrorMessage(ErrorCode.INVALID_MESSAGE, "unknown message"));
	}
	
	protected void handleIllegalMessage(Message message) {
		clientHandler.sendMessage(new ErrorMessage(ErrorCode.INVALID_STATE, "invalid game state"));
	}
	
	@MessageReceiver(type = ErrorMessage.class)
	public void receiveErrorMessage(ErrorMessage message) {
		LOG.warning("received ErrorMessage from " + clientHandler);
		if (message.getErrorCode() < 20) {
			clientHandler.disconnect();
			// deny all new messages
			clientHandler.setMessageListener(new MessageListener(clientHandler));
		}
	}
	
	@MessageReceiver(type = ConnectionRequestMessage.class)
	public void receiveConnectionRequestMessage(ConnectionRequestMessage message) {
		handleIllegalMessage(message);
	}
	
	@MessageReceiver(type = CreateLobbyRequestMessage.class)
	public void receiveCreateLobbyRequestMessage(CreateLobbyRequestMessage message) {
		handleIllegalMessage(message);
	}
	
	@MessageReceiver(type = JoinLobbyRequestMessage.class)
	public void receiveJoinLobbyRequestMessage(JoinLobbyRequestMessage message) {
		handleIllegalMessage(message);
	}
	
	@MessageReceiver(type = LeaveLobbyRequestMessage.class)
	public void receiveLeaveLobbyRequestMessage(LeaveLobbyRequestMessage message) {
		handleIllegalMessage(message);
	}
	
	@MessageReceiver(type = ReadyStatusMessage.class)
	public void receiveReadyStatusMessage(ReadyStatusMessage message) {
		handleIllegalMessage(message);
	}
	
	@MessageReceiver(type = ProbeTokenLocationsRequestMessage.class)
	public void receiveProbeTokenLocationsRequestMessage(ProbeTokenLocationsRequestMessage message) {
		handleIllegalMessage(message);
	}
	
	@MessageReceiver(type = SetTokenRequestMessage.class)
	public void receiveSetTokenRequestMessage(SetTokenRequestMessage message) {
		handleIllegalMessage(message);
	}
	
}
