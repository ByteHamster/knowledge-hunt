package edu.kit.informatik.student.knowledgehunt.server.logic;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.SocketException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.protocol.MessageSocket;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.DiscoveryMessage;
import edu.kit.informatik.student.knowledgehunt.server.ServerSettings;

/**
 * Handles the {@link ServerSocket} which accepts new client connections.
 * It also sends the discovery messages.
 */
public class ServerHandler extends Thread {
	
	private static final Logger LOG = Logger.getLogger(ServerHandler.class.getName());
	private final ServerSocket server;
	private final DiscoverySupplier supplier;
	private Consumer<ClientHandler> newConnectionConsumer;
	private final Timer discoveryTimer = new Timer("discovery-timer", true);
	
	
	public ServerHandler(int port, DiscoverySupplier supplier, Consumer<ClientHandler> newConnectionConsumer)
			throws IOException {
		this.supplier = supplier;
		this.newConnectionConsumer = newConnectionConsumer;
		server = new ServerSocket(port);
	}
	
	
	@Override
	public void run() {
		LOG.info("ServerHandler started");
		
		discoveryTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				try {
					sendDiscoveryBroadcast();
				} catch (IOException e) {
					LOG.log(Level.SEVERE, e.getMessage(), e);
					discoveryTimer.cancel();
				}
			}
		}, 0, ServerSettings.getDiscoveryIntervall());
		
		
		try {
			while (!isInterrupted()) {
				MessageSocket socket = new MessageSocket(server.accept());
				LOG.info("new connection established: " + socket);
				ClientHandler handler = new ClientHandler(new Player(socket.getSocketId(), null), socket);
				newConnectionConsumer.accept(handler);
			}
		} catch (SocketException e) {
			// should only be thrown if server.close() in #interrupt() was called => intended
		} catch (IOException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
		} finally {
			try {
				server.close();
			} catch (IOException e) {
				LOG.log(Level.SEVERE, e.getMessage(), e);
			}
		}
	}
	
	
	@Override
	public void interrupt() {
		super.interrupt();
		
		LOG.info("closing server socket...");
		discoveryTimer.cancel();
		
		try {
			server.close();
		} catch (IOException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	
	private void sendDiscoveryBroadcast() throws IOException {
		try (DatagramSocket serverSocket = new DatagramSocket()) {
			DiscoveryMessage message = new DiscoveryMessage();
			message.setName("Gruppe 7");
			message.setIpAdress(InetAddress.getLocalHost().getHostAddress());
			message.setPort(server.getLocalPort());
			message.setNumGameLobbies(supplier.getNumLobbies());
			message.setNumPlayers(supplier.getNumPlayers());
			byte[] sendData = message.getBytes();
			
			for (InetAddress address : getBroadcastAddresses()) {
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length,
						address, DiscoveryMessage.PORT);
				serverSocket.send(sendPacket);
			}
		}
	}
	
	
	private Set<InetAddress> getBroadcastAddresses() throws IOException {
		return getInterfaceAdddresses()
				.map(InterfaceAddress::getBroadcast)
				.filter(Objects::nonNull)
				.collect(Collectors.toSet());
	}
	
	private Stream<InterfaceAddress> getInterfaceAdddresses() throws SocketException {
		return Collections.list(NetworkInterface.getNetworkInterfaces()).stream()
				.filter(this::networkInterfaceFilter)
				.map(NetworkInterface::getInterfaceAddresses)	// get interface addresses
				.flatMap(List::stream);
	}
	
	private boolean networkInterfaceFilter(NetworkInterface ni) {
		try {
			return ni.isUp() && !ni.isLoopback();
		} catch (SocketException e) {
			return false;
		}
	}
	
}
