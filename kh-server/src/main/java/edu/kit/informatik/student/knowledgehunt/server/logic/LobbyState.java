package edu.kit.informatik.student.knowledgehunt.server.logic;


public enum LobbyState {
	CONNECTION_NOT_COMPLETED,
	SERVER_LOBBY,
	GAME_LOBBY,
	IN_GAME
}
