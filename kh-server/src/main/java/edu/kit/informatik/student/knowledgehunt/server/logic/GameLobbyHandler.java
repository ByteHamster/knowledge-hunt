package edu.kit.informatik.student.knowledgehunt.server.logic;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import edu.kit.informatik.student.knowledgehunt.model.GameLobby;
import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.protocol.ProtocolConstants;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ErrorMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ErrorMessage.ErrorCode;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.GameLobbyPlayerListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.JoinLobbyResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.LeaveLobbyRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.LeaveLobbyResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ReadyStatusMessage;
import edu.kit.informatik.student.knowledgehunt.server.ServerSettings;
import edu.kit.informatik.student.knowledgehunt.server.logic.ServerLobbyHandler.ServerLobbyCallback;
import edu.kit.informatik.student.knowledgehunt.server.plugin.ValidGamePlugin;
import edu.kit.informatik.student.knowledgehunt.util.plugin.GameInitializationException;

/**
 * A GameLobbyHandler handles a {@link GameLobby} and all messages
 * received by players in this lobby.
 * <p>
 * It will automatically start the game if the minimum number of players
 * is reached and all players are ready.
 * The GameLobbyHandler will also close itself if the lobby is empty
 * for a while (see {@link ServerSettings#getEmptyLobbyTimeout()}).
 */
public class GameLobbyHandler {
	
	private static final Logger LOG = Logger.getLogger(GameLobbyHandler.class.getName());
	/** All clients in this lobby. */
	private final Set<ClientHandler> clients = new HashSet<>();
	private final GameLobby lobby;
	private final ValidGamePlugin plugin;
	private final ServerLobbyCallback serverLobbyCallback;
	private Timer emptinessTimer;
	private boolean closed = false;
	
	
	public GameLobbyHandler(GameLobby lobby, ValidGamePlugin plugin, ServerLobbyCallback serverLobbyCallback) {
		this.lobby = lobby;
		this.plugin = plugin;
		this.serverLobbyCallback = serverLobbyCallback;
		startEmptinessTimer();
	}
	
	/**
	 * Try to let a client join this lobby.
	 * <p>
	 * If this lobby already contains the maximum number of players or
	 * this lobby was closed, the client will not join this lobby and an
	 * {@link ErrorMessage} is sent.
	 * Otherwise, a {@link JoinLobbyResponseMessage} is sent and all players
	 * in this lobby will be notified.
	 * @param clientHandler client who wants to join
	 * @param requestId request id of the
	 * 		{@link edu.kit.informatik.student.knowledgehunt.protocol.messages.JoinLobbyRequestMessage}
	 */
	void join(ClientHandler clientHandler, int requestId) {
		synchronized (this) {
			if (closed || lobby.getNumPlayers() >= lobby.getMaxPlayers()) {
				clientHandler.sendMessage(
						new ErrorMessage(requestId, ErrorCode.CURRENTLY_NOT_ALLOWED, "cannot join lobby"));
				return;
			}
			
			clientHandler.setDisconnectHandler(ch -> {
				leave(ch);
				serverLobbyCallback.disconnected(ch);
			});
			clientHandler.getPlayer().setLobbyId(getId());
			lobby.addPlayer(clientHandler.getPlayer());
			clients.add(clientHandler);
			emptinessTimer.cancel();
			
			clientHandler.setMessageListener(new GameLobbyMessageListener(clientHandler));
			clientHandler.setLobbyState(LobbyState.GAME_LOBBY);
			clientHandler.sendMessage(new JoinLobbyResponseMessage(requestId));
		}
		
		playersUpdated();
	}
	
	private void leave(ClientHandler clientHandler) {
		LOG.finest("leave GameLobby: " + clientHandler);
		synchronized (this) {
			clientHandler.getPlayer().setReady(false);
			clientHandler.getPlayer().setLobbyId(ProtocolConstants.HIDDEN_VALUE);
			lobby.removePlayer(clientHandler.getPlayer());
			clients.remove(clientHandler);
			if (lobby.isEmpty()) {
				startEmptinessTimer();
			}
		}
		
		playersUpdated();
	}
	
	
	private void markReady(Player player, boolean ready) {
		LOG.finest("mark " + player + " as " + (ready ? "ready" : "unready"));
		synchronized (this) {
			if (player.isReady() == ready) {
				return;
			}
			
			player.setReady(ready);
			if (ready) {
				lobby.markReady(player.getId());
				playersUpdated();
				
				if (lobby.allReady() && lobby.getPlayers().size() >= plugin.getMinNumPlayers()) {
					for (ClientHandler ch : clients) {
						ch.getPlayer().setReady(false);
						ch.getPlayer().setLobbyId(ProtocolConstants.HIDDEN_VALUE);
						ch.getPlayer().setPoints(0);
						ch.setLobbyState(LobbyState.IN_GAME);
					}
					
					serverLobbyCallback.lobbyClosed(this);
					closed = true;
					
					try {
						ValidGamePlugin pluginCopy = plugin.copy();
						pluginCopy.initNewGame(lobby.getPlayers());
						
						GameHandler gameHandler = new GameHandler(pluginCopy, clients,
								serverLobbyCallback::returnToLobby);
						gameHandler.start();
					} catch (GameInitializationException e) {
						LOG.log(Level.SEVERE, e.getMessage(), e);
						for (ClientHandler clientHandler : clients) {
							clientHandler.sendMessage(new ErrorMessage(ErrorCode.SERVER_CLOSING, "error in gamemode"));
							clientHandler.disconnect();
						}
					}
				}
			} else {
				lobby.unmarkReady(player.getId());
				playersUpdated();
			}
		}
	}
	
	
	private void playersUpdated() {
		List<Player> playerList;
		synchronized (this) {
			playerList = clients.stream().map(ClientHandler::getPlayer).collect(Collectors.toList());
		}
		
		for (ClientHandler clientHandler : clients) {
			clientHandler.sendMessage(new GameLobbyPlayerListMessage(playerList));
		}
	}
	
	
	public int getId() {
		return lobby.getId();
	}
	
	
	private void startEmptinessTimer() {
		if (this.emptinessTimer != null) {
			this.emptinessTimer.cancel();
		}
		this.emptinessTimer = new Timer("empty-lobby-timer" + lobby.getId(), true);
		this.emptinessTimer.schedule(new CloseEmptyLobbyTask(), ServerSettings.getEmptyLobbyTimeout());
	}
	
	
	public GameLobby getGameLobby() {
		return lobby;
	}
	
	
	private class CloseEmptyLobbyTask extends TimerTask {
		@Override
		public void run() {
			synchronized (GameLobbyHandler.this) {	// make sure lobby is not closed if someone just joined
				if (lobby.isEmpty()) {
					LOG.info("close GameLobby " + lobby.getId());
					serverLobbyCallback.lobbyClosed(GameLobbyHandler.this);
					closed = true;
				}
			}
		}
	}
	
	
	private final class GameLobbyMessageListener extends MessageListener {

		private GameLobbyMessageListener(ClientHandler clientHandler) {
			super(clientHandler);
		}
		
		@Override
		public void receiveErrorMessage(ErrorMessage message) {
			super.receiveErrorMessage(message);
			leave(clientHandler);
		}
		
		@Override
		public void receiveLeaveLobbyRequestMessage(LeaveLobbyRequestMessage message) {
			LOG.finest("received LeaveLobbyRequestMessage from " + clientHandler);
			serverLobbyCallback.returnToLobby(clientHandler);
			clientHandler.sendMessage(new LeaveLobbyResponseMessage(message.getRequestId()));
			leave(clientHandler);
		}
		
		@Override
		public void receiveReadyStatusMessage(ReadyStatusMessage message) {
			LOG.finest("received ReadyStatusMessage from " + clientHandler);
			synchronized (GameLobbyHandler.this) {
				if (closed) {
					clientHandler.sendMessage(new ErrorMessage(ErrorCode.INVALID_STATE, "lobby was closed"));
				} else {
					markReady(clientHandler.getPlayer(), message.isReady());
				}
			}
		}
	}
	
}
