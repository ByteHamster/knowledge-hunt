package edu.kit.informatik.student.knowledgehunt.server.logic;

import java.util.Timer;
import java.util.TimerTask;

/**
 * A GameTimer is used to schedule transitions from one {@link GameState}
 * to another.
 */
class GameTimer {
	
	private final Callback nextStateCallback;
	private Timer timer;
	
	
	GameTimer(Callback processStateCallback) {
		this.nextStateCallback = processStateCallback;
	}
	
	
	public void stop() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}
	
	
	void scheduleStateTransition(GameState state, int durationMillis) {
		if (timer != null) {
			timer.cancel();
		}
		timer = new Timer("GameTimer", true);
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				nextStateCallback.call(state);
			}
		}, durationMillis);
	}
	
	
	interface Callback {
		void call(GameState state);
	}
	
}
