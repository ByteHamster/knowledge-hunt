package edu.kit.informatik.student.knowledgehunt.server.plugin;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

/**
 * A class that provides different utility methods for a given jar file.
 * 
 * @author Jonas
 * @version 1.1
 */
class JarLoader implements Closeable {
	
	private File file;
	private JarFile jar;
	
	
	/**
	 * Creates a new {@link JarLoader}.
	 * @param file jar file to be loaded
	 * @throws IOException if an I/O error has occurred
	 */
	JarLoader(File file) throws IOException {
		this.file = file;
		jar = new JarFile(file);
	}
	
	/**
	 * Returns all jar entries of the jar file.
	 * @return all entries
	 * @throws IOException if an I/O error has occurred
	 */
	public List<JarEntry> listEntries() throws IOException {
		return listEntries(entry -> true);
	}
	
	/**
	 * Returns all jar entries which are accepted by the current {@link JarEntryFilter}.<br/>
	 * @param filter filter that filters all found {@link JarEntry JarEntries}
	 * @return all accepted entries
	 */
	public List<JarEntry> listEntries(JarEntryFilter filter) {
		List<JarEntry> acceptedEntries = new ArrayList<>();
		
		Enumeration<? extends JarEntry> entries = jar.entries();
		
		while (entries.hasMoreElements()) {
			JarEntry entry = entries.nextElement();
			if (filter.accept(entry)) {
				acceptedEntries.add(entry);
			}
		}
		
		return acceptedEntries;
	}
	
	/**
	 * Checks if the jar file contains at least one class that extends the given class
	 * (or implements it, if the parameter is an interface), so that
	 * {@link Class#isAssignableFrom(Class) cls.isAssignable(foundClass)} will return <code>true</code>.
	 * @param cls superclass or interface
	 * @return <code>true</code> if and only if the jar contains a class that is assignable to the argument
	 * @throws IOException if an I/O error has occurred
	 */
	public boolean containsClass(Class<?> cls) throws IOException {
		return !loadClasses(cls).isEmpty();
	}
	
	/**
	 * Returns all {@link Class classes} that are assignable to the searched class so that
	 * {@link Class#isAssignableFrom(Class) searchedClass.isAssignable(foundClass)} will return <code>true</code>.
	 * @param <T> type of the loaded classes
	 * @param searchedClass superclass or interface that is searched
	 * @return all classes that are assignable to the given class
	 * @throws IOException if an I/O error has occurred
	 */
	@SuppressWarnings("unchecked")
	public <T> List<Class<? extends T>> loadClasses(Class<T> searchedClass) throws IOException {
		List<Class<? extends T>> classes = new ArrayList<>();
		
		try (URLClassLoader cl = new URLClassLoader(new URL[] {file.toURI().toURL()})) {
			List<JarEntry> entries = listEntries();
			if (entries == null) {
				return null;
			}
			
			for (JarEntry entry : entries) {
				String name = entry.getName();
				try {
					if (!name.endsWith(".class")) {
						continue;
					}
					Class<?> cls = cl.loadClass(name.substring(0, name.lastIndexOf('.')).replace('/', '.'));
					if (searchedClass.isAssignableFrom(cls)) {
						classes.add((Class<? extends T>) cls);
					}
				} catch (ClassNotFoundException e) {
					System.err.println("Can't load Class " + name);
					e.printStackTrace();
				}
			}
		}
		
		return classes;
	}
	
	/**
	 * Loads all classes as specified in {@link #loadClasses(Class)} and tries to
	 * instaniate them using the given arguments.<br/>
	 * All classes that have no constructor that fits the arguments (see {@link Class#getConstructor(Class...)})
	 * or have a constructor that is not accessible are ignored.<br/>
	 * Any exception that may occur during instantiation is ignored and the class is ignored.
	 * @param <T> type of the instantiated classes
	 * @param searchedClass searched superclass or interface
	 * @param args arguments for the constructor
	 * @return a list of instances, one for each successfully instantiated class
	 * @throws IOException if an I/O error has occurred
	 */
	public <T> List<T> loadInstances(Class<T> searchedClass, Object...args) throws IOException {
		Class<?>[] classes = new Class[args.length];
		
		for (int i = 0; i < classes.length; i++) {
			classes[i] = args[i].getClass();
		}
		
		List<T> list = new ArrayList<>();
		
		for (Class<? extends T> cls : loadClasses(searchedClass)) {
			try {
				list.add(cls.getConstructor(classes).newInstance(args));
			} catch (ReflectiveOperationException | IllegalArgumentException e) {
				// ignore all exceptions which may be thrown by using reflection or by the constructor
				// search for other classes which may have a constructor that will fit
			}
		}
		
		return list;
	}
	
	/**
	 * Creates an {@link URI} to the given path inside the jar.
	 * @param resourcePath path of the searched resource
	 * @return URI to the resource
	 * @throws URISyntaxException if the path has the wrong syntax
	 */
	public URI getURI(String resourcePath) throws URISyntaxException {
		return new URI("jar:file:" + file.getPath().replace("\\", "/") + "!/" + resourcePath);
	}
	
	/**
	 * Returns an {@link InputStream} of the given resource.
	 * @param resourcePath path inside jar file to the resource
	 * @return input stream of the resource
	 * @throws IOException if an I/O error, a {@link SecurityException} or {@link IllegalStateException} has occurred
	 */
	public InputStream getStream(String resourcePath) throws IOException {
		ZipEntry entry = jar.getEntry(resourcePath);
		if (entry == null) {
			throw new IOException("couldn't find " + resourcePath);
		}
		try {
			return jar.getInputStream(entry);
		} catch (SecurityException | IllegalStateException e) {
			throw new IOException(e);
		}
	}
	
	/**
	 * Closes the jar file and all input streams to any jar entries.
	 */
	@Override
	public void close() throws IOException {
		jar.close();
	}
	
}
