package edu.kit.informatik.student.knowledgehunt.server.plugin;

final class Validator {
	
	private Validator() {
	}
	
	
	public static <T> T requireNonNull(T obj) {
		if (obj == null) {
			throw new IllegalPluginBehaviourException("object is null");
		}
		return obj;
	}
	
	
	public static <T extends Comparable<T>> T requireInRange(T t, T start, T end) {
		if (t.compareTo(start) < 0 || t.compareTo(end) >= 0) {
			throw new IllegalPluginBehaviourException("not in range");
		}
		return t;
	}
	
	public static <T extends Comparable<T>> T requireInInclusiveRange(T t, T start, T end) {
		if (t.compareTo(start) < 0 || t.compareTo(end) > 0) {
			throw new IllegalPluginBehaviourException("not in range");
		}
		return t;
	}
	
	
	public static int requireInRange(int i, int start, int end) {
		if (i < start || i >= end) {
			throw new IllegalPluginBehaviourException("number is not in range [" + start + ", " + end + ")");
		}
		return i;
	}
	
	public static int requireInInclusiveRange(int i, int start, int end) {
		return requireInRange(i, start, end + 1);
	}
	
	
	public static int requirePositive(int i) {
		if (i <= 0) {
			throw new IllegalPluginBehaviourException("number is not positive");
		}
		return i;
	}
	
	public static int requireNotNegative(int i) {
		if (i < 0) {
			throw new IllegalPluginBehaviourException("number is negative");
		}
		return i;
	}
	
}
