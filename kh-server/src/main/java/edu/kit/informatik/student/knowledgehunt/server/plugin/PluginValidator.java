package edu.kit.informatik.student.knowledgehunt.server.plugin;

import java.time.Duration;
import java.util.List;
import java.util.Set;

import edu.kit.informatik.student.knowledgehunt.model.GameMode;
import edu.kit.informatik.student.knowledgehunt.model.Location;
import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.model.Token;
import edu.kit.informatik.student.knowledgehunt.model.TokenType;
import edu.kit.informatik.student.knowledgehunt.protocol.ProtocolConstants;
import edu.kit.informatik.student.knowledgehunt.util.plugin.GameInitializationException;
import edu.kit.informatik.student.knowledgehunt.util.plugin.GamePlugin;

/**
 * A wrapper class for {@link GamePlugin GamePlugins} which performs some
 * basic checks on the values returned by the wrapped plugin.
 */
public final class PluginValidator implements ValidGamePlugin {
	
	private final GamePlugin plugin;
	private final GameMode gameMode;
	private final int minNumPlayers;
	private final int maxNumPlayers;
	
	
	public PluginValidator(GamePlugin plugin) {
		this.plugin = plugin;
		this.gameMode = Validator.requireNonNull(plugin.getGameMode());
		this.maxNumPlayers = Validator.requirePositive(plugin.getMaxNumPlayers());
		this.minNumPlayers = Validator.requireInInclusiveRange(plugin.getMinNumPlayers(), 0, maxNumPlayers);
	}
	
	
	@Override
	public ValidGamePlugin copy(Object...args) throws GameInitializationException {
		return new PluginValidator(PluginLoader.reinstantiatePlugin(plugin, args));
	}
	
	
	@Override
	public GameMode getGameMode() {
		return gameMode;
	}
	
	@Override
	public int getMinNumPlayers() {
		return minNumPlayers;
	}
	
	@Override
	public int getMaxNumPlayers() {
		return maxNumPlayers;
	}
	
	@Override
	public boolean hideTokens() {
		return plugin.hideTokens();
	}
	
	@Override
	public void initNewGame(List<Player> players) throws GameInitializationException {
		plugin.initNewGame(players);
	}
	
	@Override
	public Set<TokenType> getTokenTypes() {
		return Validator.requireNonNull(plugin.getTokenTypes());
	}
	
	@Override
	public void removePlayer(Player player) {
		plugin.removePlayer(player);
	}
	
	@Override
	public void stop() {
		plugin.stop();
	}
	
	@Override
	public void startPreround() {
		plugin.startPreround();
	}
	
	@Override
	public Duration getPreroundDuration() {
		return Validator.requireInInclusiveRange(plugin.getPreroundDuration(),
				Duration.ZERO, ProtocolConstants.MAX_DURATION_PREROUND);
	}
	
	@Override
	public Set<Token> getTokens() {
		return Validator.requireNonNull(plugin.getTokens());
	}
	
	@Override
	public Set<Location> getLocations() {
		return Validator.requireNonNull(plugin.getLocations());
	}
	
	@Override
	public void startRound() {
		plugin.startRound();
	}
	
	@Override
	public Duration getRoundDuration() {
		return Validator.requireInInclusiveRange(plugin.getRoundDuration(),
				Duration.ZERO, ProtocolConstants.MAX_DURATION_ROUND);
	}
	
	@Override
	public boolean placeToken(Token token, Location location) {
		return plugin.placeToken(token, location);
	}
	
	@Override
	public Set<Location> testToken(Token token) throws UnsupportedOperationException {
		return Validator.requireNonNull(plugin.testToken(token));
	}
	
	@Override
	public boolean shouldEndRoundNow() {
		return plugin.shouldEndRoundNow();
	}
	
	@Override
	public void startPostround() {
		plugin.startPostround();
	}
	
	@Override
	public Duration getPostroundDuration() {
		return Validator.requireInInclusiveRange(plugin.getPostroundDuration(),
				Duration.ZERO, ProtocolConstants.MAX_DURATION_POSTROUND);
	}
	
	@Override
	public void updatePlayerPoints() {
		plugin.updatePlayerPoints();
	}
	
	@Override
	public Player getWinner() {
		return plugin.getWinner();  // may be null
	}
	
	@Override
	public String getWinningReason() {
		return plugin.getWinningReason();
	}
	
}
