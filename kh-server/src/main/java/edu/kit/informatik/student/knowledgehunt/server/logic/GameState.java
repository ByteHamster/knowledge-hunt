package edu.kit.informatik.student.knowledgehunt.server.logic;

import edu.kit.informatik.student.knowledgehunt.protocol.messages.StateTransitionMessage;

enum GameState {
	PREROUND, ROUND, POSTROUND, FINISHED;
	
	GameState next() {
		return values()[(ordinal() + 1) % 3];
	}

	static GameState startState() {
		return values()[0];
	}

	public StateTransitionMessage.State toTransition() {
		switch (this) {
			case PREROUND:
				return StateTransitionMessage.State.PREROUND;
			case ROUND:
				return StateTransitionMessage.State.ROUND;
			case POSTROUND:
				return StateTransitionMessage.State.POSTROUND;
			case FINISHED:
				return StateTransitionMessage.State.LOBBY;
			default:
				throw new IllegalArgumentException("illegal game state: " + this);
		}
	}
}