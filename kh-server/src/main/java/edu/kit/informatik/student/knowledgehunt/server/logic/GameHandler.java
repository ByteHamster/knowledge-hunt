package edu.kit.informatik.student.knowledgehunt.server.logic;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import edu.kit.informatik.student.knowledgehunt.model.Location;
import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.model.Token;
import edu.kit.informatik.student.knowledgehunt.protocol.ProtocolConstants;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ErrorMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ErrorMessage.ErrorCode;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.InitLocationsMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.InitTokenTypesMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ProbeTokenLocationsRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ProbeTokenLocationsResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ScoreboardMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.SetTokenRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.StateTransitionMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.TokenUpdateMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.TokensMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.server.ServerSettings;
import edu.kit.informatik.student.knowledgehunt.server.plugin.ValidGamePlugin;
import edu.kit.informatik.student.knowledgehunt.util.plugin.GamePlugin;

/**
 * A GameHandler handles all messages during the preround, round and postround state.
 * The behaviour of the current game mode is specified in the {@link GamePlugin} that
 * is used by the this GameHandler.
 * <p>
 * The GameHandler also starts a {@link Timer} to change the state automatically.
 */
final class GameHandler {
	
	private static final Logger LOG = Logger.getLogger(GameHandler.class.getName());
	
	private final GamePlugin plugin;
	private final Set<ClientHandler> clients;
	private final GameTimer nextStateTimer;
	private final Consumer<ClientHandler> returnToServerLobbyConsumer;
	
	private GameState currentState = GameState.POSTROUND;
	private Map<Integer, Token> tokens;
	private Map<Integer, Location> locations;
	private boolean closed = false;
	
	
	GameHandler(ValidGamePlugin plugin, Set<ClientHandler> clients,
			Consumer<ClientHandler> returnToServerLobbyConsumer) {
		this.plugin = plugin;
		// make sure it cannot be modified from elsewhere and to use synchronized set
		// but not create "double" synchronized set thus copy it
		this.clients = Collections.synchronizedSet(new HashSet<>(clients));
		this.returnToServerLobbyConsumer = returnToServerLobbyConsumer;
		this.nextStateTimer = new GameTimer(this::startState);
	}
	
	
	public void start() {
		startState(GameState.PREROUND);
	}
	
	
	private void startNextState() {
		startState(currentState.next());
	}
	
	private synchronized void startState(GameState state) {
		if (closed || state == currentState) {
			return;
		}
		
		nextStateTimer.stop();
		currentState = state;
		LOG.finest("start state " + state + " in game " + toString());
		Timer pluginTimeoutTimer = new Timer("pluginTimeoutTimer", true);
		pluginTimeoutTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				LOG.severe("plugin timeout");
				closeDueToFailure("an exception occurred while executing the game mode");
			}
		}, ServerSettings.getPluginTimeout());
		
		try {
			int duration = changeState(state);
			processState(state);
			
			if (!closed) {
				nextStateTimer.scheduleStateTransition(state.next(), duration);
			}
		} catch (RuntimeException e) {
			LOG.log(Level.SEVERE, e.getMessage(), e);
			closeDueToFailure("an exception occurred while executing the game mode");
		} finally {
			pluginTimeoutTimer.cancel();
		}
	}
	
	
	private int changeState(GameState state) {
		Duration duration;
		int durationMillis;
		
		switch (state) {
		case PREROUND:
			plugin.startPreround();
			duration = plugin.getPreroundDuration();
			break;
		case ROUND:
			for (ClientHandler clientHandler : clients) {
				clientHandler.setMessageListener(new GameMessageListener(clientHandler));
			}
			plugin.startRound();
			duration = plugin.getRoundDuration();
			break;
		case POSTROUND:
			for (ClientHandler clientHandler : clients) {
				clientHandler.setMessageListener(new MessageListener(clientHandler));
			}
			plugin.startPostround();
			duration = plugin.getPostroundDuration();
			break;
		case FINISHED:
		default:
			throw new IllegalStateException("unknown state");
		}
		
		durationMillis = (int) duration.toMillis();
		
		StateTransitionMessage message = new StateTransitionMessage(durationMillis, state.toTransition());
		clients.forEach(ch -> ch.sendMessage(message));
		
		return durationMillis;
	}


	private void processState(GameState state) {
		switch (state) {
		case PREROUND:
			Set<Token> newTokens = plugin.getTokens();
			Set<Location> newLocations = plugin.getLocations();
			
			tokens = newTokens.stream().collect(Collectors.toMap(Token::getId, Function.identity()));
			locations = newLocations.stream().collect(Collectors.toMap(Location::getId, Function.identity()));
			
			if (plugin.hideTokens()) {
				for (ClientHandler ch : clients) {
					ch.sendMessage(new TokensMessage(new ArrayList<>(newTokens), ch.getPlayer()));
				}
			} else {
				sendToAllClients(new TokensMessage(new ArrayList<>(newTokens)));
			}
			sendToAllClients(new InitTokenTypesMessage(new ArrayList<>(plugin.getTokenTypes())));
			sendToAllClients(new InitLocationsMessage(new ArrayList<>(newLocations)));
			break;
		case ROUND:
			break;
		case POSTROUND:
			plugin.updatePlayerPoints();
			Player winner = plugin.getWinner();
			int winnerId = winner == null ? ProtocolConstants.INVALID_PLAYER_ID : winner.getId();
			String winningCondition = plugin.getWinningReason();
			List<Player> players = clients.stream().map(ClientHandler::getPlayer).collect(Collectors.toList());
			
			sendToAllClients(new TokensMessage(new ArrayList<>(tokens.values())));
			sendToAllClients(new ScoreboardMessage(winnerId, winningCondition, players));
			
			if (winner != null) {
				close(plugin.getPostroundDuration().toMillis());
			}
			break;
		case FINISHED:
		default:
			break;
		}
	}
	
	
	private void closeDueToFailure(String errorMessage) {
		LOG.info("closing GameHandler " + toString());
		closed = true;
		nextStateTimer.stop();
		plugin.stop();
		
		ErrorMessage message = new ErrorMessage(ErrorCode.SERVER_CLOSING, "error in plugin");
		message.setErrorMessage(errorMessage);
		sendToAllClients(message);
		
		clients.forEach(ClientHandler::disconnect);
	}
	
	private void close(long waitUntilReturn) {
		LOG.info("game closed");
		closed = true;
		nextStateTimer.stop();
		plugin.stop();
		
		Timer timer = new Timer(true);
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				sendToAllClients(new StateTransitionMessage(ProtocolConstants.INVALID_TIMER,
						StateTransitionMessage.State.LOBBY));
				clients.forEach(returnToServerLobbyConsumer);
			}
		}, waitUntilReturn);
	}
	
	
	private void sendToAllClients(Message message) {
		for (ClientHandler clientHandler : clients) {
			clientHandler.sendMessage(message);
		}
	}
	
	
	private final class GameMessageListener extends MessageListener {
		
		private GameMessageListener(ClientHandler clientHandler) {
			super(clientHandler);
		}
		
		
		@Override
		public void receiveErrorMessage(ErrorMessage message) {
			super.receiveErrorMessage(message);
			clients.remove(clientHandler);
			plugin.removePlayer(clientHandler.getPlayer());
		}
	
	
		@Override
		public void receiveProbeTokenLocationsRequestMessage(ProbeTokenLocationsRequestMessage message) {
			LOG.finest("received ProbeTokenLocationsRequestMessage from " + clientHandler);
			try {
				Token token = tokens.get(message.getTokenId());
				
				if (token == null) {
					throw new IllegalArgumentException();
				}
				
				List<Integer> validLocations = plugin.testToken(token)
						.stream().map(Location::getId).collect(Collectors.toList());
				clientHandler.sendMessage(
						new ProbeTokenLocationsResponseMessage(message.getRequestId(), validLocations));
			} catch (UnsupportedOperationException e) {
				clientHandler.sendMessage(new ErrorMessage(message.getRequestId(), ErrorCode.INVALID_MESSAGE,
						"probe token is not supported by this plugin"));
			} catch (IllegalArgumentException e) {
				clientHandler.sendMessage(new ErrorMessage(message.getRequestId(), ErrorCode.INVALID_ID,
						"invalid token id"));
			}
		}
	
	
		@Override
		public void receiveSetTokenRequestMessage(SetTokenRequestMessage message) {
			LOG.finest("received SetTokenRequestMessage from " + clientHandler);
			int playerId = clientHandler.getPlayer().getId();
			Token token = tokens.get(message.getTokenId());
			Location location = locations.get(message.getLocationId());
			
			if (token == null) {
				clientHandler.sendMessage(new ErrorMessage(message.getRequestId(), ErrorCode.INVALID_ID,
						"invalid token id"));
				return;
			} else if (location == null) {
				clientHandler.sendMessage(new ErrorMessage(message.getRequestId(), ErrorCode.INVALID_ID,
						"invalid location id"));
				return;
			} else if (token.getPlayerId() != playerId) {
				clientHandler.sendMessage(new ErrorMessage(message.getRequestId(), ErrorCode.NOT_ALLOWED,
						"token does not belong to player"));
			}
			
			boolean successfull = plugin.placeToken(token, location);
			
			if (successfull) {
				sendToAllClients(new TokenUpdateMessage(token.getId(), location.getId()));
				
				if (plugin.shouldEndRoundNow()) {
					startNextState();
				}
			} else {
				clientHandler.sendMessage(new ErrorMessage(message.getRequestId(), ErrorCode.CURRENTLY_NOT_ALLOWED,
						"token could not be placed"));
			}
		}
	}
	
}
