package edu.kit.informatik.student.knowledgehunt.server.logic;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * A NameManager stores all names that are currently in use.
 * When registring new names, it will also check whether the new
 * name is valid.
 */
class NameManager {
	
	private final Set<String> playerNames = Collections.synchronizedSet(new HashSet<>());
	private final Set<String> lobbyNames = Collections.synchronizedSet(new HashSet<>());
	
	
	boolean registerPlayerName(String name) {
		if (!isValidPlayerName(name)) {
			return false;
		}
		return playerNames.add(name);
	}
	
	private boolean isValidPlayerName(String name) {
		return isValidName(name);
	}
	
	void deregisterPlayerName(String name) {
		playerNames.remove(name);
	}
	
	
	boolean registerLobbyName(String name) {
		if (!isValidLobbyName(name)) {
			return false;
		}
		return lobbyNames.add(name);
	}
	
	private boolean isValidLobbyName(String name) {
		return isValidName(name);
	}
	
	void deregisterLobbyName(String name) {
		lobbyNames.remove(name);
	}
	
	
	private boolean isValidName(String name) {
		if (name.length() == 0 || name.length() > 64) {
			return false;
		}
		return name.matches("\\S([ \\S]*\\S)?");
	}
}
