package edu.kit.informatik.student.knowledgehunt.server;

import java.io.File;


public final class ServerSettings extends Settings {
	
	private static final String OPTIONS_PATH = "settings.properties";
	private static final File LOGGING_DIR = new File(".");
	private static ServerSettings instance;
	
	/** directory which contains .jar files with our
	 * {@link edu.kit.informatik.student.knowledgehunt.util.plugin.GamePlugin plugins} */
	private File pluginDirectory = new File("plugins");
	/** port used by the server */
	private int port = 8765;
	/** number of seconds until closing an empty lobby */
	private int emptyLobbyTimeout = 30;
	/** number of seconds until closing a plugin to prevent endless loops */
	private int pluginTimeout = 30;
	/** number of seconds between two discovery messages (see RFC section 5.3.1.1) */
	private int discoveryIntervall = 5;
	
	
	static {	// is executed exactly once (per classloader)
		instance = new ServerSettings();
		instance.load();
	}
	
	private ServerSettings() {
		super(OPTIONS_PATH);
	}
	
	
	public static void setPluginDirectory(File directory) {
		instance.pluginDirectory = directory;
	}
	
	public static File getPluginDirectory() {
		return instance.pluginDirectory;
	}
	
	public static File getLoggingDir() {
		return LOGGING_DIR;
	}
	
	public static int getPort() {
		return instance.port;
	}

	public static int getEmptyLobbyTimeout() {
		return instance.emptyLobbyTimeout * 1000;
	}
	
	public static int getPluginTimeout() {
		return instance.pluginTimeout * 1000;
	}


	public static int getDiscoveryIntervall() {
		return instance.discoveryIntervall * 1000;
	}
	
}
