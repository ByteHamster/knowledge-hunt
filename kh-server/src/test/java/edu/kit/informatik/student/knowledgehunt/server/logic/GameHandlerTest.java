package edu.kit.informatik.student.knowledgehunt.server.logic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import edu.kit.informatik.student.knowledgehunt.protocol.MessageSocket;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.GameLobbyPlayerListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.InitLocationsMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.InitTokenTypesMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ScoreboardMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.StateTransitionMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.StateTransitionMessage.State;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.TokensMessage;


public class GameHandlerTest extends AbstractServerTest {
	
	private MessageSocket client;
	private int lobbyId;
	
	@Override
	@Before
	public void setUp() throws IOException {
		super.setUp();
		client = newConnectionToServer(true);
		lobbyId = createGameLobby(client);
		joinGameLobby(client, lobbyId);
	}
	
	
	private MessageSocket newClientToLobby() throws IOException {
		MessageSocket client = newConnectionToServer(true);
		joinGameLobby(client, lobbyId);
		return client;
	}
	
	
	@Ignore  // takes some time
	@Test
	public void testStateDurations() throws IOException {
		markReady(client, true);
		MessageSocket client2 = newClientToLobby();
		markReady(client2, true);
		
		skipMessages(client, GameLobbyPlayerListMessage.class);
		skipMessages(client2, GameLobbyPlayerListMessage.class);
		
		StateTransitionMessage stateTransitionMessage = receiveOneMessage(client, StateTransitionMessage.class);
		long starttime = System.currentTimeMillis();
		assertEquals(1000, stateTransitionMessage.getMilliseconds());
		assertEquals(State.PREROUND, stateTransitionMessage.getNextState());
		receiveMultipleMessages(client, InitTokenTypesMessage.class, TokensMessage.class, InitLocationsMessage.class);
		
		starttime = testNextStateDuration(State.ROUND, starttime);
		starttime = testNextStateDuration(State.POSTROUND, starttime);
		receiveMultipleMessages(client, TokensMessage.class, ScoreboardMessage.class);
		starttime = testNextStateDuration(State.PREROUND, starttime);
	}
	
	private long testNextStateDuration(State state, long starttime) throws IOException {
		StateTransitionMessage stateTransitionMessage = receiveOneMessage(client, StateTransitionMessage.class);
		long endtime = System.currentTimeMillis();
		assertEquals(1000, stateTransitionMessage.getMilliseconds());
		assertEquals(state, stateTransitionMessage.getNextState());
		assertTrue((endtime - starttime) < 1500);
		assertTrue((endtime - starttime) > 500);
		return endtime;
	}
	
}
