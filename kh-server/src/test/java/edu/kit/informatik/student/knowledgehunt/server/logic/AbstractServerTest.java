package edu.kit.informatik.student.knowledgehunt.server.logic;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import edu.kit.informatik.student.knowledgehunt.model.GameMode;
import edu.kit.informatik.student.knowledgehunt.protocol.MessageSocket;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ConnectionRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ConnectionResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.CreateLobbyRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.CreateLobbyResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ErrorMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.GameLobbyPlayerListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.GameModeListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.JoinLobbyRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.JoinLobbyResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.LobbyListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.PlayerListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ReadyStatusMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageReceiver;
import edu.kit.informatik.student.knowledgehunt.server.Main;
import edu.kit.informatik.student.knowledgehunt.server.ServerSettings;
import edu.kit.informatik.student.knowledgehunt.util.LogConfiguration;

public abstract class AbstractServerTest {

	private Map<MessageSocket, Message> lastReceivedMessages = new HashMap<>();
	private Map<MessageSocket, Integer> playerIds = new HashMap<>();
	private List<MessageSocket> clients = new ArrayList<>();
	private int playerNameCounter = 0;
	private Map<String, String> modeNameToId;
	
	@BeforeClass
	public static void setUpBeforeClass() {
//		Runtime.getRuntime().addShutdownHook(new Thread(Main::stop));
		LogConfiguration.initialize(Level.WARNING, true);
		ServerSettings.setPluginDirectory(
				new File("src/test/resources/edu/kit/informatik/student/knowledgehunt/server/plugin"));
	}
	
	@Before
	public void setUp() throws IOException {
		Main.start();  // Start server
	}
	
	public final class MessageListener {
		private final MessageSocket client;
		
		public MessageListener(MessageSocket client) {
			this.client = client;
		}
		
		@MessageReceiver(type = Message.class)
		public void receiveMessage(Message message) {
			lastReceivedMessages.put(client, message);
		}
	}
	
	protected MessageSocket newConnectionToServer(boolean finishConnection) throws IOException {
		Socket socket = new Socket(InetAddress.getLocalHost(), ServerSettings.getPort());
		MessageSocket client = new MessageSocket(socket);
		client.setMessageListener(new MessageListener(client));
		client.setSocketErrorListener(e -> fail("ERROR IN SOCKET"));
		clients.add(client);
		
		if (finishConnection) {
			client.writeMessage(new ConnectionRequestMessage("TestPlayer" + playerNameCounter++));
			HashMap<Class<?>, Message> messages = receiveMultipleMessages(client, ConnectionResponseMessage.class,
					LobbyListMessage.class, PlayerListMessage.class, GameModeListMessage.class);
			
			playerIds.put(client,
					((ConnectionResponseMessage) messages.get(ConnectionResponseMessage.class)).getPlayerId());
			
			if (modeNameToId == null) {
				modeNameToId = ((GameModeListMessage) messages.get(GameModeListMessage.class)).getGameModes().stream()
						.collect(Collectors.toMap(GameMode::getName, GameMode::getId));
				System.out.println("default mode: " + modeNameToId);
			}
		}
		
		return client;
	}
	
	protected <T> T receiveOneMessage(MessageSocket client, Class<T> expected) throws IOException {
		if (lastReceivedMessages.get(client) == null) {
			client.receiveOneMessage();
		}
		Message message = lastReceivedMessages.remove(client);
		assertTrue("Received unexpected message: " + message.getClass(),
				expected.isInstance(message));
		//noinspection unchecked
		return (T) message;
	}

	private HashMap<Class<?>, Message> receiveMultipleMessages(MessageSocket client, int numberOfMessages)
			throws IOException {
		HashMap<Class<?>, Message> messages = new HashMap<>();
		for (int i = 0; i < numberOfMessages; i++) {
			Message message = receiveOneMessage(client, Message.class);
			if (message instanceof ErrorMessage) {
				fail("ErrorMessage received (AbstractServerTest#receiveMultipleMessages)");
			}
			messages.put(message.getClass(), message);
		}
		return messages;
	}
	
	/** will fail if an {@link ErrorMessage} was received */
	protected HashMap<Class<?>, Message> receiveMultipleMessages(MessageSocket client, Class<?>... types)
			throws IOException {
		HashMap<Class<?>, Message> messages = receiveMultipleMessages(client, types.length);
		for (Class<?> type : types) {
			assertTrue(type + " was not received, just " + messages.keySet(), messages.containsKey(type));
		}
		return messages;
	}
	
	protected void skipMessages(MessageSocket client, Class<?>...types) throws IOException {
		if (lastReceivedMessages.get(client) == null) {
			client.receiveOneMessage();
		}
		Message message = lastReceivedMessages.get(client);
		
		List<Class<?>> typesList = Arrays.asList(types);
		while (typesList.contains(message.getClass())) {
			System.out.println("skip message " + message.getClass() + " for client "
					+ client.getSocketId());
			lastReceivedMessages.remove(client);
			client.receiveOneMessage();
			message = lastReceivedMessages.get(client);
			// last message will be cached, because it should not be skipped
		}
	}
	
	
	@After
	public void cleanUp() {
		clients.forEach(MessageSocket::close);
		Main.stop();
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		LogConfiguration.close();
	}
	
	
	protected int createGameLobby(MessageSocket client, String modeName) throws IOException {
		CreateLobbyRequestMessage createLobbyRequestMessage = new CreateLobbyRequestMessage();
		createLobbyRequestMessage.setRequestId(123);
		createLobbyRequestMessage.setGameModeId(modeNameToId.get(modeName));
		createLobbyRequestMessage.setLobbyName("Test lobby");
		createLobbyRequestMessage.setMaxPlayers(50);
		client.writeMessage(createLobbyRequestMessage);
		
		HashMap<Class<?>, Message> messages = receiveMultipleMessages(client,
				CreateLobbyResponseMessage.class, LobbyListMessage.class);
		return ((CreateLobbyResponseMessage) messages.get(CreateLobbyResponseMessage.class)).getLobbyId();
	}
	
	/** only useable if {@link #newConnectionToServer(true)} was used */
	protected int createGameLobby(MessageSocket client) throws IOException {
		return createGameLobby(client, "Test Mode");
	}
	
	
	protected GameLobbyPlayerListMessage joinGameLobby(MessageSocket client, int lobbyId) throws IOException {
		client.writeMessage(new JoinLobbyRequestMessage(124, lobbyId));
		HashMap<Class<?>, Message> messages =
				receiveMultipleMessages(client, JoinLobbyResponseMessage.class, GameLobbyPlayerListMessage.class);
		return (GameLobbyPlayerListMessage) messages.get(GameLobbyPlayerListMessage.class);
	}
	
	
	protected void markReady(MessageSocket client, boolean ready) {
		ReadyStatusMessage readyMessage = new ReadyStatusMessage();
		readyMessage.setReady(ready);
		client.writeMessage(readyMessage);
	}
	
	
	protected int getId(MessageSocket client) {
		return playerIds.get(client);
	}
	
}
