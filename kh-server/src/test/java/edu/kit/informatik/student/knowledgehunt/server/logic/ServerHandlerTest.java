package edu.kit.informatik.student.knowledgehunt.server.logic;

import org.junit.Test;


public class ServerHandlerTest extends AbstractServerTest {
	
	@Test
	public void testConnection() throws InterruptedException {
		// due to the setUp and cleanUp method in the super class,
		// a single connection to the server will be established and closed
		Thread.sleep(10);
	}
	
}
