package edu.kit.informatik.student.knowledgehunt.server;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import edu.kit.informatik.student.knowledgehunt.model.GameMode;
import edu.kit.informatik.student.knowledgehunt.model.Location;
import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.model.Token;
import edu.kit.informatik.student.knowledgehunt.model.TokenType;
import edu.kit.informatik.student.knowledgehunt.util.IdGenerator;
import edu.kit.informatik.student.knowledgehunt.util.plugin.GamePlugin;

/**
 * @author Hans-Peter
 */
public class TestQuiz implements GamePlugin {
	private static final int NUM_LOCATIONS = 4;
	private static final int ID_TRUE = 0;
	private static final int ID_FALSE = 1;
	private List<Player> players;
	private List<List<Token>> placedTokens = new ArrayList<>();
	private List<Integer> correctAnswers = new ArrayList<>();

	@Override
	public GameMode getGameMode() {
		GameMode mode = new GameMode();
		mode.setDescription("Vote for answers by placing tokens.\n"
				+ "The first player with the correct answer earns 10 points, then 5, 3, 1.\n"
				+ "Winner is who first earns 30 points total.");
		mode.setName("Quiz");
		return mode;
	}

	@Override
	public int getMinNumPlayers() {
		return 1;
	}

	@Override
	public int getMaxNumPlayers() {
		return 20;
	}

	@Override
	public boolean hideTokens() {
		return true;
	}

	@Override
	public void initNewGame(List<Player> players) {
		this.players = players;
		placedTokens = Stream.generate((Supplier<ArrayList<Token>>) ArrayList::new)
				.limit(NUM_LOCATIONS).collect(Collectors.toCollection(ArrayList::new));
	}

	@Override
	public Set<TokenType> getTokenTypes() {
		Set<TokenType> set = new HashSet<>();
		set.add(new TokenType(ID_TRUE, "True", "This token should be placed if the claim is true"));
		set.add(new TokenType(ID_FALSE, "False", "This token should be placed if the claim is false"));
		return set;
	}

	@Override
	public void removePlayer(Player player) {
		players.remove(player);
	}

	@Override
	public void stop() {

	}

	@Override
	public void startPreround() {

	}

	@Override
	public Duration getPreroundDuration() {
		return Duration.ZERO;
	}

	@Override
	public Set<Token> getTokens() {
		IdGenerator id = new IdGenerator(players.size() * 2 * NUM_LOCATIONS);

		Set<Token> set = new HashSet<>();
		for (Player p : players) {
			for (int type = 0; type < 2; type++) {
				for (int number = 0; number < NUM_LOCATIONS; number++) {
					set.add(new Token(id.get(), type, (type == ID_TRUE) ? "True" : "False", 0, p.getId()));
				}
			}
		}
		return set;
	}

	@Override
	public Set<Location> getLocations() {
		Set<Location> set = new HashSet<>();

		Random rand = new Random();
		for (int i = 0; i < NUM_LOCATIONS; i++) {
			int n1 = rand.nextInt(70) + 10;
			int n2 = rand.nextInt(70) + 10;
			int result = n1 + n2;

			if (rand.nextInt(2) == 1) {
				result += rand.nextInt(30) - 15;
				correctAnswers.add(ID_FALSE);
			} else {
				correctAnswers.add(ID_TRUE);
			}
			set.add(new Location(0, n1 + "+" + n2 + "=" + result));
		}
		return set;
	}

	@Override
	public void startRound() {

	}

	@Override
	public Duration getRoundDuration() {
		return Duration.ZERO;
	}

	@Override
	public boolean placeToken(Token token, Location location) {
		long placed = placedTokens.get(location.getId()).stream()
				.filter(token1 -> token.getPlayerId() == token1.getPlayerId()).count();
		if (placed == 0) {
			placedTokens.get(location.getId()).add(token);
			return true;
		}
		return false;
	}

	@Override
	public Set<Location> testToken(Token token) throws UnsupportedOperationException {
		Set<Location> locations = new HashSet<>();
		for (int i = 0; i < NUM_LOCATIONS; i++) {
			long placed = placedTokens.get(i).stream()
					.filter(token1 -> token.getPlayerId() == token1.getPlayerId()).count();
			if (placed == 0) {
				locations.add(new Location(i, null));
			}
		}
		return locations;
	}

	@Override
	public void startPostround() {

	}

	@Override
	public Duration getPostroundDuration() {
		return Duration.ofSeconds(5);
	}

	@Override
	public void updatePlayerPoints() {
		for (List<Token> location: placedTokens) {
			int points = 10;
			for (int i = 0; i < location.size(); i++) {
				Token token = location.get(i);
				if (correctAnswers.get(i) == token.getId()) {
					findPlayer(token.getPlayerId()).incrementPoints(points);
					points /= 2;
				}
			}
		}
	}

	private Player findPlayer(int playerId) {
		return players.stream().filter(p -> p.getId() == playerId).findFirst().orElse(new Player());
	}

	@Override
	public Player getWinner() {
		Player maxPlayer = players.stream().max(Comparator.comparing(Player::getPoints)).orElse(new Player());
		if (maxPlayer.getPoints() >= 30) {
			return maxPlayer;
		}
		return null;
	}

	@Override
	public String getWinningReason() {
		return "Player was first to reach 30 points";
	}
}
