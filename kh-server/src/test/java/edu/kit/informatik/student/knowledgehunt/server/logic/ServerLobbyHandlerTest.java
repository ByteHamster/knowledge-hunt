package edu.kit.informatik.student.knowledgehunt.server.logic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;

import edu.kit.informatik.student.knowledgehunt.model.GameLobby;
import edu.kit.informatik.student.knowledgehunt.model.GameMode;
import edu.kit.informatik.student.knowledgehunt.protocol.MessageSocket;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ConnectionRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ConnectionResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.CreateLobbyRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.CreateLobbyResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ErrorMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ErrorMessage.ErrorCode;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.GameLobbyPlayerListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.GameModeListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.JoinLobbyRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.JoinLobbyResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.LobbyListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.PlayerListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;

public class ServerLobbyHandlerTest extends AbstractServerTest {
	
	private MessageSocket client;
	
	
	@Override
	public void setUp() throws IOException {
		super.setUp();
		client = newConnectionToServer(false);
	}
	
	
	@Test
	public void testConnect() throws IOException {
		ConnectionRequestMessage connReq = new ConnectionRequestMessage("TestPlayer");
		connReq.setRequestId(42);
		client.writeMessage(connReq);

		ConnectionResponseMessage connResp = receiveOneMessage(client, ConnectionResponseMessage.class);
		assertEquals(42, connResp.getRequestId());

		receiveMultipleMessages(client, LobbyListMessage.class, PlayerListMessage.class, GameModeListMessage.class);
	}

	@Test
	public void testConnectTwice() throws IOException {
		client.writeMessage(new ConnectionRequestMessage("TestPlayer"));
		receiveOneMessage(client, ConnectionResponseMessage.class);
		receiveMultipleMessages(client, LobbyListMessage.class, PlayerListMessage.class, GameModeListMessage.class);

		client.writeMessage(new ConnectionRequestMessage("NewName"));
		ErrorMessage err = receiveOneMessage(client, ErrorMessage.class);

		assertEquals(ErrorCode.INVALID_STATE.getCode(), err.getErrorCode());
	}

	@Test
	public void testReconnect() throws IOException {
		client.writeMessage(new ConnectionRequestMessage("player"));
		receiveOneMessage(client, ConnectionResponseMessage.class);
		receiveMultipleMessages(client, LobbyListMessage.class, PlayerListMessage.class, GameModeListMessage.class);
		//disconnect
		client.close();
		// connect again
		client = newConnectionToServer(false);
		client.writeMessage(new ConnectionRequestMessage("player"));
		receiveOneMessage(client, ConnectionResponseMessage.class);
		receiveMultipleMessages(client, LobbyListMessage.class, PlayerListMessage.class, GameModeListMessage.class);
	}
	
	@Test
	public void testConnectDifferentClients() throws IOException {
		for (int i = 0; i < 5; i++) {
			client = newConnectionToServer(false);
			client.writeMessage(new ConnectionRequestMessage("player" + i));
			receiveOneMessage(client, ConnectionResponseMessage.class);
		}
		// clients disconnect after the method at execution of @After method
		for (int i = 0; i < 5; i++) {
			client = newConnectionToServer(false);
			client.writeMessage(new ConnectionRequestMessage("player" + i));
			receiveOneMessage(client, ErrorMessage.class);
		}
	}
	
	@Test
	public void testCreateInvalidLobby() throws IOException {
		client.writeMessage(new ConnectionRequestMessage("TestPlayer"));
		receiveMultipleMessages(client, ConnectionResponseMessage.class,
				LobbyListMessage.class, PlayerListMessage.class, GameModeListMessage.class);

		client.writeMessage(new JoinLobbyRequestMessage(23, 42));
		ErrorMessage err = receiveOneMessage(client, ErrorMessage.class);

		assertEquals(23, err.getRequestId());
		assertEquals(ErrorCode.INVALID_ID.getCode(), err.getErrorCode());
	}
	
	
	@Test
	public void testCreateAndJoinLobby() throws IOException {
		client.writeMessage(new ConnectionRequestMessage("TestPlayer"));
		HashMap<Class<?>, Message> messages = receiveMultipleMessages(client, ConnectionResponseMessage.class,
				LobbyListMessage.class, PlayerListMessage.class, GameModeListMessage.class);
		List<GameMode> gameModes = ((GameModeListMessage) messages.get(GameModeListMessage.class)).getGameModes();
		System.out.println(gameModes);
		System.out.println(gameModes.get(0).getId());
		
		CreateLobbyRequestMessage createLobbyRequestMessage = new CreateLobbyRequestMessage();
		createLobbyRequestMessage.setRequestId(123);
		createLobbyRequestMessage.setGameModeId("0");
		createLobbyRequestMessage.setLobbyName("Test lobby");
		createLobbyRequestMessage.setMaxPlayers(50);
		client.writeMessage(createLobbyRequestMessage);
		
		messages = receiveMultipleMessages(client, CreateLobbyResponseMessage.class, LobbyListMessage.class);
		CreateLobbyResponseMessage createLobbyResponseMessage =
				(CreateLobbyResponseMessage) messages.get(CreateLobbyResponseMessage.class);
		assertEquals(createLobbyRequestMessage.getRequestId(), createLobbyResponseMessage.getRequestId());
		
		LobbyListMessage lobbyListMessage = (LobbyListMessage) messages.get(LobbyListMessage.class);
		assertEquals(1, lobbyListMessage.getLobbies().size());
		GameLobby lobby = lobbyListMessage.getLobbies().get(0);
		assertEquals(createLobbyRequestMessage.getLobbyName(), lobby.getName());
		assertEquals(0, lobby.getNumPlayers());
		assertTrue(lobby.getMaxPlayers() <= createLobbyRequestMessage.getMaxPlayers());
		
		
		client.writeMessage(new JoinLobbyRequestMessage(124, createLobbyResponseMessage.getLobbyId()));
		receiveMultipleMessages(client, JoinLobbyResponseMessage.class, GameLobbyPlayerListMessage.class);
	}
	
	

	@Test
	public void testSinglePlayerLobby() throws IOException {
		client = newConnectionToServer(true);
		int lobbyId = createGameLobby(client, "SinglePlayerTest");
		joinGameLobby(client, lobbyId);
		
		client = newConnectionToServer(true);
		client.writeMessage(new JoinLobbyRequestMessage(130, lobbyId));
		receiveOneMessage(client, ErrorMessage.class);
	}
	
}
