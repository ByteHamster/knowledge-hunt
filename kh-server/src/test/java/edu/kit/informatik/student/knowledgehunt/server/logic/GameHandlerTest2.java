package edu.kit.informatik.student.knowledgehunt.server.logic;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.student.knowledgehunt.model.Token;
import edu.kit.informatik.student.knowledgehunt.protocol.MessageSocket;
import edu.kit.informatik.student.knowledgehunt.protocol.ProtocolConstants;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.GameLobbyPlayerListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.InitLocationsMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.InitTokenTypesMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ScoreboardMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.StateTransitionMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.TokensMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;

public class GameHandlerTest2 extends AbstractServerTest {
	
	private MessageSocket client1;
	private MessageSocket client2;
	private int lobbyId;
	
	@Override
	@Before
	public void setUp() throws IOException {
		super.setUp();
		client1 = newConnectionToServer(true);
		lobbyId = createGameLobby(client1, "Quiz");
		joinGameLobby(client1, lobbyId);
		
		client2 = newConnectionToServer(true);
		joinGameLobby(client2, lobbyId);
		
		markReady(client1, true);
		markReady(client2, true);
		skipMessages(client1, GameLobbyPlayerListMessage.class);
		skipMessages(client2, GameLobbyPlayerListMessage.class);
		receiveOneMessage(client1, StateTransitionMessage.class);
		receiveOneMessage(client2, StateTransitionMessage.class);
	}
	
	
	@Test
	public void testTokens() throws IOException {
		testPreroundTokens(client1);
		testPreroundTokens(client2);
		
		// go to round
		receiveOneMessage(client1, StateTransitionMessage.class);
		receiveOneMessage(client2, StateTransitionMessage.class);
		
		// go to postround
		receiveOneMessage(client1, StateTransitionMessage.class);
		receiveOneMessage(client2, StateTransitionMessage.class);
		
		testPostroundTokens(client1);
		testPostroundTokens(client2);
	}


	private void testPreroundTokens(MessageSocket client) throws IOException {
		HashMap<Class<?>, Message> messages;
		messages = receiveMultipleMessages(client, InitTokenTypesMessage.class, TokensMessage.class,
				InitLocationsMessage.class);
		TokensMessage tokensMessage = (TokensMessage) messages.get(TokensMessage.class);
		
		for (Token token : tokensMessage.getTokens()) {
			boolean thisPlayer = token.getPlayerId() == getId(client);
//			System.out.println(toTokenString(token));
			// must be not equal if it refers to the same player, unequal otherwise
			assertTrue(toTokenString(token), token.getValue() == ProtocolConstants.HIDDEN_VALUE ^ thisPlayer);
			assertTrue(toTokenString(token), token.getType() == ProtocolConstants.HIDDEN_TYPE ^ thisPlayer);
			assertTrue(toTokenString(token),
					token.getDisplayName().equals(ProtocolConstants.HIDDEN_DISPLAY_NAME) ^ thisPlayer);
		}
	}
	
	
	private void testPostroundTokens(MessageSocket client) throws IOException {
		HashMap<Class<?>, Message> messages;
		messages = receiveMultipleMessages(client, TokensMessage.class, ScoreboardMessage.class);
		TokensMessage tokensMessage = (TokensMessage) messages.get(TokensMessage.class);
		
		for (Token token : tokensMessage.getTokens()) {
//			System.out.println(toTokenString(token));
			assertFalse(toTokenString(token), token.getValue() == ProtocolConstants.HIDDEN_VALUE);
			assertFalse(toTokenString(token), token.getType() == ProtocolConstants.HIDDEN_TYPE);
			assertFalse(toTokenString(token), token.getDisplayName().equals(ProtocolConstants.HIDDEN_DISPLAY_NAME));
		}
	}
	
	
	private String toTokenString(Token token) {
		return "token id: " + token.getId() + " player: " + token.getPlayerId() + " value: " + token.getValue()
				+ " type: " + token.getType() + " display name: " + token.getDisplayName();
	}
	
}
