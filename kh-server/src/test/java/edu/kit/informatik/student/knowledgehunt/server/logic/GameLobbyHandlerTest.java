package edu.kit.informatik.student.knowledgehunt.server.logic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.student.knowledgehunt.protocol.MessageSocket;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.GameLobbyPlayerListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.LeaveLobbyRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.LeaveLobbyResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.LobbyListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.PlayerListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ReadyStatusMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.StateTransitionMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.StateTransitionMessage.State;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;


public class GameLobbyHandlerTest extends AbstractServerTest {
	
	private MessageSocket client;
	private int lobbyId;
	private GameLobbyPlayerListMessage playerListMessage;
	
	@Override
	@Before
	public void setUp() throws IOException {
		super.setUp();
		client = newConnectionToServer(true);
		lobbyId = createGameLobby(client);
		playerListMessage = joinGameLobby(client, lobbyId);
	}
	
	@Test
	public void testMarkReady() throws IOException {
		assertEquals(1, playerListMessage.getPlayers().size());
		assertFalse(playerListMessage.getPlayers().get(0).isReady());
		
		ReadyStatusMessage message = new ReadyStatusMessage();
		message.setReady(true);
		client.writeMessage(message);
		
		GameLobbyPlayerListMessage playerListMessage = receiveOneMessage(client, GameLobbyPlayerListMessage.class);
		assertEquals(1, playerListMessage.getPlayers().size());
		assertTrue(playerListMessage.getPlayers().get(0).isReady());
	}
	
	
	@Test
	public void testUnmarkReady() throws IOException {
		assertEquals(1, playerListMessage.getPlayers().size());
		assertFalse(playerListMessage.getPlayers().get(0).isReady());
		
		client = newConnectionToServer(true);
		joinGameLobby(client, lobbyId);
		
		markReady(client, true);
		
		playerListMessage = receiveOneMessage(client, GameLobbyPlayerListMessage.class);
		assertEquals(2, playerListMessage.getPlayers().size());
		assertFalse(playerListMessage.getPlayers().get(0).isReady());
		assertTrue(playerListMessage.getPlayers().get(1).isReady());
		
		markReady(client, false);
		
		playerListMessage = receiveOneMessage(client, GameLobbyPlayerListMessage.class);
		assertEquals(2, playerListMessage.getPlayers().size());
		assertFalse(playerListMessage.getPlayers().get(0).isReady());
		assertFalse(playerListMessage.getPlayers().get(1).isReady());
		
		markReady(client, true);
		
		playerListMessage = receiveOneMessage(client, GameLobbyPlayerListMessage.class);
		assertEquals(2, playerListMessage.getPlayers().size());
		assertFalse(playerListMessage.getPlayers().get(0).isReady());
		assertTrue(playerListMessage.getPlayers().get(1).isReady());
	}
	
	
	@Test
	public void startGame() throws IOException {
		ReadyStatusMessage readyMessage = new ReadyStatusMessage();
		readyMessage.setReady(true);
		client.writeMessage(readyMessage);
		
		client = newConnectionToServer(true);
		joinGameLobby(client, lobbyId);
		client.writeMessage(readyMessage);
		
		receiveOneMessage(client, GameLobbyPlayerListMessage.class);
		StateTransitionMessage stateTransitionMessage = receiveOneMessage(client, StateTransitionMessage.class);
		assertEquals(1000, stateTransitionMessage.getMilliseconds());
		assertEquals(State.PREROUND, stateTransitionMessage.getNextState());
	}
	
	
	@Test
	public void testJoinAndLeave() throws IOException {
		HashMap<Class<?>, Message> messages;
		
		MessageSocket client2 = newConnectionToServer(true);
		joinGameLobby(client2, lobbyId);
		receiveOneMessage(client, GameLobbyPlayerListMessage.class);
		
		MessageSocket client3 = newConnectionToServer(true);
		
		LeaveLobbyRequestMessage leaveLobbyRequestMessage = new LeaveLobbyRequestMessage();
		leaveLobbyRequestMessage.setRequestId(258);
		client.writeMessage(leaveLobbyRequestMessage);
		receiveMultipleMessages(client,
				LeaveLobbyResponseMessage.class, LobbyListMessage.class, PlayerListMessage.class);
		receiveOneMessage(client2, GameLobbyPlayerListMessage.class);
		messages = receiveMultipleMessages(client3, PlayerListMessage.class, LobbyListMessage.class);
		assertEquals(1, ((LobbyListMessage) messages.get(LobbyListMessage.class)).getLobbies().size());
		assertEquals(3, ((PlayerListMessage) messages.get(PlayerListMessage.class)).getPlayers().size());
		
		((LobbyListMessage) messages.get(LobbyListMessage.class)).getLobbies().forEach(lobby ->
					System.out.println(lobby.getName() + " " + lobby.getId() + " " + lobby.getNumPlayers()));
		((PlayerListMessage) messages.get(PlayerListMessage.class)).getPlayers().forEach(player ->
					System.out.println(player.getName() + " " + player.getId() + " " + player.getLobbyId()));
	}
	
}
