package edu.kit.informatik.student.knowledgehunt.server;

import edu.kit.informatik.student.knowledgehunt.model.GameMode;

public class SinglePlayerPlugin extends TestPlugin {
	
	@Override
	public GameMode getGameMode() {
		GameMode mode = new GameMode();
		mode.setDescription("SinglePlayerTest Mode description");
		mode.setName("SinglePlayerTest");
		return mode;
	}
	
	@Override
	public int getMinNumPlayers() {
		return 1;
	}
	
	@Override
	public int getMaxNumPlayers() {
		return 1;
	}
	
}
