package edu.kit.informatik.student.knowledgehunt.server;

import edu.kit.informatik.student.knowledgehunt.model.GameMode;
import edu.kit.informatik.student.knowledgehunt.model.Location;
import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.model.Token;
import edu.kit.informatik.student.knowledgehunt.model.TokenType;
import edu.kit.informatik.student.knowledgehunt.util.plugin.GameInitializationException;
import edu.kit.informatik.student.knowledgehunt.util.plugin.GamePlugin;

import java.time.Duration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TestPlugin implements GamePlugin {
	@Override
	public GameMode getGameMode() {
		GameMode mode = new GameMode();
		mode.setDescription("Test Mode description");
		mode.setName("Test Mode");
		return mode;
	}

	@Override
	public int getMinNumPlayers() {
		return 2;  // do NOT change unless adjusted GameLobbyHandlerTest
	}

	@Override
	public int getMaxNumPlayers() {
		return 5;
	}

	@Override
	public boolean hideTokens() {
		return true;
	}

	@Override
	public void initNewGame(List<Player> players) throws GameInitializationException {
	}

	@Override
	public Set<TokenType> getTokenTypes() {
		return new HashSet<>();
	}

	@Override
	public void removePlayer(Player player) {
	}

	@Override
	public void stop() {
	}

	@Override
	public void startPreround() {
	}

	@Override
	public Duration getPreroundDuration() {
		return Duration.ofSeconds(1);
	}

	@Override
	public Set<Token> getTokens() {
		return new HashSet<>();
	}

	@Override
	public Set<Location> getLocations() {
		return new HashSet<>();
	}

	@Override
	public void startRound() {
	}

	@Override
	public Duration getRoundDuration() {
		return Duration.ofSeconds(1);
	}

	@Override
	public boolean placeToken(Token token, Location location) {
		return false;
	}

	@Override
	public Set<Location> testToken(Token token) throws UnsupportedOperationException {
		return new HashSet<>();
	}

	@Override
	public void startPostround() {
	}

	@Override
	public Duration getPostroundDuration() {
		return Duration.ofSeconds(1);
	}

	@Override
	public void updatePlayerPoints() {
	}

	@Override
	public Player getWinner() {
		return null;
	}

	@Override
	public String getWinningReason() {
		return null;
	}
}
