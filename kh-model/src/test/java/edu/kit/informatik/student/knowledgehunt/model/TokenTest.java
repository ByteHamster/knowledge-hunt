package edu.kit.informatik.student.knowledgehunt.model;

import org.junit.Before;
import org.junit.Test;

import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class TokenTest implements Observer {
	private CountDownLatch updatedLatch;

	@Before
	public void init() {
		updatedLatch = new CountDownLatch(1);
	}

	@Override
	public void update(Observable observable, Object o) {
		updatedLatch.countDown();
	}

	@Test
	public void testObservableId() throws InterruptedException {
		Token token = new Token();
		token.addObserver(this);
		token.setId(42);
		token.notifyObservers();
		assertTrue(updatedLatch.await(5, TimeUnit.SECONDS));
	}

	@Test
	public void testObservableDisplayName() throws InterruptedException {
		Token token = new Token();
		token.addObserver(this);
		token.setDisplayName("Test");
		token.notifyObservers();
		assertTrue(updatedLatch.await(5, TimeUnit.SECONDS));
	}
}
