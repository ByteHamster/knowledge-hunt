package edu.kit.informatik.student.knowledgehunt.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

/**
 * A class that configures the root logger of this projects.
 * 
 * @author Jonas
 */
public final class LogConfiguration {
	
	private static final Logger LOGGER = Logger.getLogger("edu.kit.informatik.student.knowledgehunt");
	private static Formatter formatter;
	
	
	private LogConfiguration() {
	}
	
	/**
	 * Initializes the root logger of this project.
	 * 
	 * @param level the log level
	 * @param advanced whether to use an advanced {@link Formatter} or not
	 */
	public static void initialize(Level level, boolean advanced) {
		LogManager.getLogManager().reset();
		LOGGER.setLevel(level);
		
		if (advanced) {
			formatter = new AdvancedTimeFormatter();
		} else {
			formatter = new SimpleTimeFormatter();
		}
		
		StreamHandler consoleHandler = new StreamHandler(System.out, formatter) {
			@Override
			public synchronized void publish(LogRecord record) {
				super.publish(record);
				flush();
			}
		};
		LOGGER.addHandler(consoleHandler);
	}
	
	
	public static void addFile(File file, Level level) {
		try {
			FileHandler fileHandler = new FileHandler(file.getPath());
			fileHandler.setFormatter(formatter);
			fileHandler.setLevel(level);
			LOGGER.addHandler(fileHandler);
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	
	/**
	 * Closes all logging output destinations. The loggers will no longer print anything.
	 */
	public static void close() {
		LogManager.getLogManager().reset();
	}
	
	
	private static class SimpleTimeFormatter extends Formatter {
		@Override
		public String format(LogRecord record) {
			StringBuilder builder = new StringBuilder(128);
			
			builder.append(DateFormat.getTimeInstance().format(new Date(record.getMillis())));
			builder.append(' ');
			builder.append(record.getLevel().getLocalizedName());
			builder.append(": ");
			builder.append(record.getMessage());
			builder.append(System.lineSeparator());
			
			return builder.toString();
		}
	}
	
	
	private static class AdvancedTimeFormatter extends Formatter {
		@Override
		public String format(LogRecord record) {
			StringBuilder builder = new StringBuilder(128);
			String sourceClassName = record.getSourceClassName();
			Throwable throwable = record.getThrown();
			
			builder.append(DateFormat.getTimeInstance().format(new Date(record.getMillis())));
			builder.append(' ');
			builder.append(record.getLevel().getLocalizedName());
			builder.append(" [");
			builder.append(sourceClassName.substring(sourceClassName.lastIndexOf('.') + 1));
			builder.append('#');
			builder.append(record.getSourceMethodName());
			builder.append("]: ");
			
			if (throwable == null) {
				builder.append(record.getMessage());
				builder.append(System.lineSeparator());
			} else {
				StringWriter sw = new StringWriter(1024);
				throwable.printStackTrace(new PrintWriter(sw));
				builder.append(sw.toString());
			}
			
			return builder.toString();
		}
		
	}
	
}
