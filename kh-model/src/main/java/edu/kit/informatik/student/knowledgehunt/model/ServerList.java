package edu.kit.informatik.student.knowledgehunt.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class ServerList extends Observable {
	private List<Server> servers = new ArrayList<>();

	public List<Server> getServers() {
		return servers;
	}

	public void addServer(Server server) {
		if (servers.contains(server)) {
			Server oldServer = servers.get(servers.indexOf(server));
			oldServer.setNumGameLobbies(server.getNumGameLobbies());
			oldServer.setNumPlayers(server.getNumPlayers());
			oldServer.notifyObservers();
		} else {
			servers.add(server);
			setChanged();
			notifyObservers();
		}
	}
}
