package edu.kit.informatik.student.knowledgehunt.model;

import java.util.Objects;
import java.util.Observable;

public class Player extends Observable {
	private int id;
	private String name;
	private boolean ready;
	private int points;
	private int lobbyId;

	
	public Player(int id, String name) {
		this.id = id;
		this.name = name;
		this.ready = false;
		this.points = 0;
	}
	
	public Player() {
	}
	
	@Override
	public String toString() {
		return "Player: name=" + (name != null ? name : "NoName") + ", ready=" + ready + ", points=" + points;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
		setChanged();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		setChanged();
	}

	public boolean isReady() {
		return ready;
	}

	public void setReady(boolean ready) {
		this.ready = ready;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public void incrementPoints(int points) {
		this.points += points;
	}

	public int getLobbyId() {
		return lobbyId;
	}

	public void setLobbyId(int lobbyId) {
		this.lobbyId = lobbyId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Player other = (Player) obj;
		return id == other.id;
	}
}
