package edu.kit.informatik.student.knowledgehunt.util.plugin;

/**
 * Signals that an exception occurred while initializing a new game.
 */
public class GameInitializationException extends Exception {
	
	private static final long serialVersionUID = 3881034077675637639L;
	
	public GameInitializationException() {
	}
	
	public GameInitializationException(String message) {
		super(message);
	}
	
	public GameInitializationException(Throwable cause) {
		super(cause);
	}
	
	public GameInitializationException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public GameInitializationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
	
}
