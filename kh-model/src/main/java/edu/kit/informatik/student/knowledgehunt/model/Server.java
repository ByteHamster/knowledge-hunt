package edu.kit.informatik.student.knowledgehunt.model;

import java.util.Observable;

public class Server extends Observable {
	private String name;
	private int port;
	private String ip;
	private int numGameLobbies;
	private int numPlayers;

	public Server() {
	}

	public Server(String ip, int port) {
		this.ip = ip;
		this.port = port;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
		setChanged();
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
		setChanged();
	}

	public int getNumGameLobbies() {
		return numGameLobbies;
	}

	public void setNumGameLobbies(int numGameLobbies) {
		if (this.numGameLobbies != numGameLobbies) {
			setChanged();
		}
		this.numGameLobbies = numGameLobbies;
	}

	public int getNumPlayers() {
		return numPlayers;
	}

	public void setNumPlayers(int numPlayers) {
		if (this.numPlayers != numPlayers) {
			setChanged();
		}
		this.numPlayers = numPlayers;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Server)) {
			return false;
		}
		Server other = (Server) o;
		// Comparing ip only because updated player count is still the same server
		return port == other.port && ip.equals(other.ip);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
