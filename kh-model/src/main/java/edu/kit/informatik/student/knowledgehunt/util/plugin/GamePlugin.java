package edu.kit.informatik.student.knowledgehunt.util.plugin;

import java.time.Duration;
import java.util.List;
import java.util.Set;

import edu.kit.informatik.student.knowledgehunt.model.GameMode;
import edu.kit.informatik.student.knowledgehunt.model.Location;
import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.model.Token;
import edu.kit.informatik.student.knowledgehunt.model.TokenType;

/**
 * A GameHandler is the facade of a plugin that provides a game mode.
 * @implSpec
 * 		All subclasses must provide a non-arg constructor which will be called.
 * 		<p>
 * 		For each game, a new instance will be created.
 * 		<p>
 * 		All methods can safely assume that all passed arguments are valid.
 * 		No paramter will be {@code null}, all {@linkplain Token tokens} and
 * 		{@linkplain Location locations} are valid and used by this plugin.
 * @author Jonas
 */
public interface GamePlugin {
	
	// GENERAL AND PRE-GAME MESSAGE
	
	/**
	 * Returns a {@link GameMode} object.
	 * @return game mode of this plugin
	 * @implSpec this method must always return the same or an equal {@link GameMode} object
	 */
	GameMode getGameMode();
	
	default int getMinNumPlayers() {
		return 2;
	}
	
	int getMaxNumPlayers();
	
	/**
	 * Returns {@code true} if tokens shall be hidden when deployed during round, {@code false} otherwise.
	 * @return whether tokens shall be hidden or not
	 */
	boolean hideTokens();
	
	/**
	 * Initializes a new game.
	 * @param players all players in this game
	 * @throws GameInitializationException if this method fails for any reason
	 * @implSpec
	 * 		The {@code players} list will not be modified by the caller.
	 * 		All modification is done explicitly by calling {@link #removePlayer(Player)}.
	 */
	void initNewGame(List<Player> players) throws GameInitializationException;
	
	/**
	 * Returns all {@linkplain TokenType token types} which occur in this game.
	 * @implSpec this method will only be called once after initializing a new game
	 * @return all token types
	 */
	Set<TokenType> getTokenTypes();
	
	/**
	 * Removes a player from this game.
	 * @param player to be removed
	 */
	void removePlayer(Player player);
	
	/**
	 * Close this game.
	 * @apiNote This method will always be called when the game stops.
	 */
	default void stop() { }
	
	
	// PREROUND MESSAGES
	
	default void startPreround() { }
	
	Duration getPreroundDuration();
	
	Set<Token> getTokens();
	
	Set<Location> getLocations();
	
	
	// ROUND MESSAGES
	
	default void startRound() { }
	
	Duration getRoundDuration();
	
	/**
	 * Places a token on a specific location.
	 * If the token cannot be placed, this method returns {@code false}
	 * and the board has not changed.
	 * @param token token to be placed
	 * @param location location where the token should be placed
	 * @return {@code true} if the token was successfully be placed, {@code false} otherwise
	 */
	boolean placeToken(Token token, Location location);
	
	/**
	 * Test if a token can be placed on a location.
	 * This method may not be supported and throw an {@link UnsupportedOperationException}.
	 * @param token token to test
	 * @return all {@link Location locations} where this token can be placed
	 * @throws UnsupportedOperationException if this method is not supported
	 */
	default Set<Location> testToken(Token token) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}
	
	default boolean shouldEndRoundNow() {
		return false;
	}
	
	// POSTROUND MESSAGES
	
	default void startPostround() { }
	
	Duration getPostroundDuration();
	
	/**
	 * Updates the points of all players.
	 */
	void updatePlayerPoints();
	
	/**
	 * Returns the winner of this game.
	 * If no one has won yet, this method returns {@code null}.
	 * @return winner of this game or {@code null}
	 */
	Player getWinner();
	
	String getWinningReason();
	
}
