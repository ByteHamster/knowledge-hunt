package edu.kit.informatik.student.knowledgehunt.model;

import java.util.Observable;

public class GameMode extends Observable {
	private String id;
	private String name;
	private String description;

	public GameMode(String id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	public GameMode() {
	}
	
	
	@Override
	public String toString() {
		return "GameMode:_id=" + id + ", name=" + name + ", description=" + description;
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
		setChanged();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		setChanged();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
		setChanged();
	}
}
