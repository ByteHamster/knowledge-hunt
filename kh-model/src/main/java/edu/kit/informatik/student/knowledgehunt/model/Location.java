package edu.kit.informatik.student.knowledgehunt.model;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Observable;

public class Location extends Observable {
	private int id;
	private String displayName;
	private String description;
	private int points;
	private ArrayList<Token> tokens = new ArrayList<>();
	
	public Location(int id, String displayName) {
		this.id = id;
		this.displayName = displayName;
	}

	public Location(int id, String displayName, int points) {
		this(id, displayName);
		this.points = points;
	}
	
	public Location() {
	}
	
	
	@Override
	public String toString() {
		return "Location: id=" + id + ", displayName=" + displayName + ", tokens=" + tokens;
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
		setChanged();
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
		setChanged();
	}

	public ArrayList<Token> getTokens() {
		return tokens;
	}

	public void setTokens(ArrayList<Token> tokens) {
		this.tokens = tokens;
		setChanged();
	}
	
	public void addToken(Token token) {
		this.tokens.add(token);
		setChanged();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Location other = (Location) obj;
		return id == other.id;
	}
	
}
