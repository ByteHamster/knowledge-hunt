package edu.kit.informatik.student.knowledgehunt.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.stream.Collectors;

public class Game extends Observable {
	private GameMode mode;
	private ArrayList<Player> players = new ArrayList<>();
	private Board board = new Board();

	public Game(GameMode mode, ArrayList<Player> players) {
		this.mode = mode;
		this.players = players;
		this.board = new Board();
	}
	
	public Game() {
	}

	public GameMode getMode() {
		return mode;
	}

	public void setMode(GameMode mode) {
		this.mode = mode;
		setChanged();
	}

	public ArrayList<Player> getPlayers() {
		return players;
	}

	public void setPlayers(ArrayList<Player> players) {
		this.players = players;
		setChanged();
	}

	public Board getBoard() {
		return board;
	}
	
	public void removePlayer(Player player) {
		players.remove(player);
		setChanged();
	}
	
	/**
	 * Only updates the points, ignores the other attributes of the given players
	 * @param playerListWithUpdatedPoints
	 */
	public void updatePlayerPoints(List<Player> playerListWithUpdatedPoints) {
		for (Player p :players) {
			for (Player p2 : playerListWithUpdatedPoints) {
				if (p.equals(p2)) { // Same ID
					p.setPoints(p2.getPoints());
				}
			}
		}
	}
	
	public String getPlayerNameById(int playerId) {
		String name = null;
		List<Player> r = players.stream().filter(p -> p.getId() == playerId).collect(Collectors.toList());
		if (r.size() == 1) {
			name = r.get(0).getName();
		}
		return name;
	}
	
}
