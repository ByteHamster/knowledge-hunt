package edu.kit.informatik.student.knowledgehunt.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.stream.Collectors;

public class Board extends Observable {
	private ArrayList<Location> locations = new ArrayList<>();
	private ArrayList<Token> tokens = new ArrayList<>();

	public ArrayList<Location> getLocations() {
		return locations;
	}

	public void setLocations(ArrayList<Location> locations) {
		this.locations = locations;
		setChanged();
	}

	public ArrayList<Token> getTokens() {
		return tokens;
	}

	public void setTokens(ArrayList<Token> tokens) {
		this.tokens = tokens;
		setChanged();
	}
	
	public void removeTokenById(int tokenId) {
		for (Iterator<Token> i = tokens.iterator(); i.hasNext();) {
			if (i.next().getId() == tokenId) {
				i.remove();
				setChanged();
			}
		}
	}
	
	public void addTokenToLocation(int locationId, int tokenId) {
		List<Location> location = locations.stream().filter(l -> l.getId() == locationId).collect(Collectors.toList());
		List<Token> token = tokens.stream().filter(t -> t.getId() == tokenId).collect(Collectors.toList());
		if (location.size() == 1 && token.size() == 1) {
			location.get(0).addToken(token.get(0));
			setChanged();
		} else {
			System.err.println(String.format("Could not add token %d to location %d", tokenId, locationId));
		}
	}
	
	public List<Token> getTokensByPlayerId(int playerId) {
		return tokens.stream().filter(t -> t.getPlayerId() == playerId).collect(Collectors.toList());
	}
	
	public void unhideTokens() {
		for (Location l : locations) {
			for (Token t : l.getTokens()) {
				for (Token t2 : tokens) {
					if (t.equals(t2)) {
						t.setDisplayName(t2.getDisplayName());
						t.setType(t2.getType());
						t.setValue(t2.getValue());
					}
				}
			}
		}
	}
}
