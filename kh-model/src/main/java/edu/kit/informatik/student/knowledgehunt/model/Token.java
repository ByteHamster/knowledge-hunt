package edu.kit.informatik.student.knowledgehunt.model;

import java.util.Objects;
import java.util.Observable;

public class Token extends Observable {
	private int type;
	private String displayName;
	private int value;
	private int id;
	private int playerId;

	public Token(int id, int type, String displayName, int value, int playerId) {
		this.id = id;
		this.type = type;
		this.displayName = displayName;
		this.value = value;
		this.playerId = playerId;
	}
	
	public Token() {
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
		setChanged();
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
		setChanged();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
		setChanged();
	}

	public int getPlayerId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Token other = (Token) obj;
		return id == other.id;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + getDisplayName() + ", id=" + getId() + ", val=" + getValue()
				+ "type=" + type + ")";
	}
}
