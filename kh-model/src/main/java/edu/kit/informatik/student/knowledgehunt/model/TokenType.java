package edu.kit.informatik.student.knowledgehunt.model;

import java.util.Objects;
import java.util.Observable;

public class TokenType extends Observable {
	private int id;
	private String displayName;
	private String description;

	public TokenType(int id, String displayName, String description) {
		this.id = id;
		this.displayName = displayName;
		this.description = description;
	}
	
	public TokenType() {
	}
	
	
	@Override
	public String toString() {
		return "TokenType: id=" + id + ", displayName=" + displayName + ", description=" + description;
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
		setChanged();
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
		setChanged();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
		setChanged();
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TokenType other = (TokenType) obj;
		return id == other.id;
	}
}
