package edu.kit.informatik.student.knowledgehunt.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;

public class GameLobby extends Observable {
	private int id;
	private String modeId;
	private String name;
	private HashSet<Integer> readyPlayers = new HashSet<>();
	private List<Player> players;
	private int numPlayers;
	private int maxPlayers;
	
	
	public GameLobby(int id, GameMode mode, int maxPlayers) {
		this.id = id;
		this.modeId = mode.getId();
		this.name = mode.getName();
		this.players = new ArrayList<>();
		this.maxPlayers = maxPlayers;
	}
	
	public GameLobby() {
	}
	
	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
		setChanged();
	}

	public void markReady(int playerId) {
		readyPlayers.add(playerId);
		setChanged();
	}
	
	public void unmarkReady(int playerId) {
		readyPlayers.remove(playerId);
		setChanged();
	}

	public boolean allReady() {
		for (Player player : players) {
			if (!readyPlayers.contains(player.getId())) {
				return false;
			}
		}
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getModeId() {
		return modeId;
	}

	public void setModeId(String modeId) {
		this.modeId = modeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumPlayers() {
		if (players != null) {
			return players.size();
		}
		// No players set. This is a client that currently only knows the number, not the actual players
		return numPlayers;
	}

	public void setNumPlayers(int numPlayers) {
		this.numPlayers = numPlayers;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}
	
	public void addPlayer(Player player) {
		if (players.size() + 1 > getMaxPlayers()) {
			throw new IllegalStateException("adding player exceeds player limit");
		}
		players.add(player);
		setChanged();
	}
	
	public void removePlayer(Player player) {
		players.remove(player);
		setChanged();
	}
	
	public boolean isEmpty() {
		if (players != null) {
			return players.isEmpty();
		}
		return numPlayers != 0;
	}
	
}
