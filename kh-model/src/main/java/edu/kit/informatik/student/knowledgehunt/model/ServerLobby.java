package edu.kit.informatik.student.knowledgehunt.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.stream.Collectors;

public class ServerLobby extends Observable {
	private static final String DEFAULT_MODENAME = "unknown mode name";
	private List<Player> players = new ArrayList<>();
	private List<GameMode> gameModes = new ArrayList<>();
	private List<GameLobby> games = new ArrayList<>();

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
		setChanged();
	}

	public List<GameMode> getGameModes() {
		return gameModes;
	}

	public void setGameModes(List<GameMode> gameModes) {
		this.gameModes = gameModes;
		setChanged();
	}

	public List<GameLobby> getGames() {
		return games;
	}

	public void setGames(List<GameLobby> games) {
		this.games = games;
		setChanged();
	}

	public String getGameModeNameFromId(String modeId) {
		String modeName = DEFAULT_MODENAME;
		List<GameMode> r = gameModes.stream().
				filter(g -> g.getId().equals(modeId)).collect(Collectors.toList());
		if (r.size() == 1) {
			modeName = r.get(0).getName();
		}
		return modeName;
	}
}
