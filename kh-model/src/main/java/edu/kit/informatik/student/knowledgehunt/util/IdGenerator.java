package edu.kit.informatik.student.knowledgehunt.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class IdGenerator {
	private final List<Integer> range;
	private int current = 0;

	public IdGenerator(int numberOfIds) {
		range = IntStream.range(0, numberOfIds).boxed()
				.collect(Collectors.toCollection(ArrayList::new));
		Collections.shuffle(range);
	}

	public int get() {
		return range.get(current++);
	}
}
