# Knowledge Hunt

This game is designed to illustrate students' everyday struggle to find places to learn.
At KIT, there are various locations where students can learn. The locations are of
different value for the gain of knowledge. For example, learning can be more efficient
in the library than in the student council. By occupying a location, knowledge can be obtained
in the form of knowledge points.

## Introduction
Each of the players has several learning tokens.
There are several locations that can be used for learning on the game board.
The learning tokens can be placed at locations. Each location has a value.
If a location is occupied successfully, a player receives the points of this location
in addition to the points of his placed tokens.

## Gameplay
The game is organized in rounds. While a round is in progress, all players can
place their tokens face down on any location. Moving or withdrawing the tokens is not allowed.
At the end of a round, there is an evaluation phase in which the tokens are revealed
and the points distribution is announced. The player whose accumulated token value is the highest
on the respective location wins the location. The player who won the location gets his tokens back.
He gets points amounting the sum of his tokens and the points of the location.
The tokens of other players that are placed in the location are removed from the game.
If there is a tie, the player who first set a token to that location wins.