package edu.kit.informatik.student.knowledgehunt.client;

import edu.kit.informatik.student.knowledgehunt.client.ui.GameLobbyStage;
import edu.kit.informatik.student.knowledgehunt.client.ui.GameStage;
import edu.kit.informatik.student.knowledgehunt.client.ui.ServerLobbyStage;
import edu.kit.informatik.student.knowledgehunt.client.ui.ServerSelectStage;

class UiListeners {
	ServerSelectStage.ServerSelectedListener serverSelectedListener;
	
	ServerLobbyStage.GameSelectedListener gameSelectedListener;
	ServerLobbyStage.LeaveServerListener leaveServerListener;
	ServerLobbyStage.OpenRulesListener openRulesListener;
	ServerLobbyStage.CreateGameLobbyListener createGameLobbyListener;
	
	GameLobbyStage.ReadyListener readyListener;
	GameLobbyStage.LeaveLobbyListener leaveLobbyListener;
	
	GameStage.ProbeTokenLocationsListener probeTokenLocationsListener;
	GameStage.SetTokenListener setTokenListener;
	GameStage.LeaveGameListener leaveGameListener;
}
