package edu.kit.informatik.student.knowledgehunt.client.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.stream.Collectors;

import edu.kit.informatik.student.knowledgehunt.client.ui.ServerLobbyStage.OpenRulesListener;
import edu.kit.informatik.student.knowledgehunt.client.util.ClientUtil;
import edu.kit.informatik.student.knowledgehunt.client.util.ColorGenerator;
import edu.kit.informatik.student.knowledgehunt.client.util.UiConstants;
import edu.kit.informatik.student.knowledgehunt.model.Game;
import edu.kit.informatik.student.knowledgehunt.model.Location;
import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.model.Token;
import edu.kit.informatik.student.knowledgehunt.model.TokenType;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tooltip;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;

public class GameStage implements Observer {

	private static final String LEAVE_BUTTON_TEXT = "Leave";
	private static final String RULES_BUTTON_TEXT = "Show Rules and Modes";
	private static final String TOKENLIST_LABEL_TEXT = "Your Tokens";
	private static final String LOCATIONLIST_LABEL_TEXT = "Locations";
	private static final String VICTORY_SUFFIX = " has won!";
	private static final String DRAGGED_TOKEN = "draggedToken";
	private static final String POSTROUND_LABEL_TEXT = "Points";
	private static final String ROUND_LABEL_TEXT = "Place Tokens!";
	private static final String PREROUND_LABEL_TEXT = "The Hunt begins soon...";
	private static final String SETTOKEN_ALERTTEXT = "Set token...";
	private static final int WINDOW_HEIGHT = 1000;
	private static final int WINDOW_WIDTH = 1600;
	private static final double TOKEN_RADIUS = 30.0;
	private static final int LISTVIEW_PREFHEIGHT = 400;
	private static final int LISTVIEW_PREFWIDTH = 1000;
	
	private Stage stage;
	private Game game;
	
	/**
	 * ID of the current player of this client
	 */
	private int playerId;
	
	private ProbeTokenLocationsListener probeTokenLocationListener;
	private SetTokenListener setTokenListener;
	private OpenRulesListener openRulesListener;
	private LeaveGameListener leaveGameListener;
	
	private ListView<Location> locationListView;
	private ListProperty<Location> locationListProperty = new SimpleListProperty<>();
	
	private ListView<TokenType> tokenListView;
	private ListProperty<TokenType> tokenListProperty = new SimpleListProperty<>();
	
	private DataFormat tokenDataFormat = new DataFormat(DRAGGED_TOKEN);
	private Token draggedToken;
	
	private Label stateLabel = new Label();
	private Label countdownLabel = new Label();
	private IntegerProperty countdown = new SimpleIntegerProperty();
	
	private boolean hideTokensInTokenListView = false;

	/**
	 * locationId -> cell id in the locationListView
	 * Needed for highlight probe token
	 */
	private HashMap<Integer, Integer> locationCellIndices = new HashMap<>(); //
	
	/**
	 * playerID -> Color
	 */
	private HashMap<Integer, Color> playerColorHashMap = new HashMap<>();
	private Button leaveButton;
	private Label winnerLabel = new Label();
	private Label winningReasonLabel = new Label();
	private HBox gameFieldPane;
	private VBox scoreBoardNode;
	
	private List<TokenType> tokenTypeList = new ArrayList<>();
	
	private ListProperty<Player> playerListProperty = new SimpleListProperty<>();
	private ListProperty<Player> scoreboardPlayerListProperty = new SimpleListProperty<>();
	private Label titleLabel = new Label();
	private Label gameNameLabel = new Label();
	private Label gameModeLabel = new Label();


	private ListView<Player> playerListView;
	private Alert setTokenAlert;

	public GameStage(Game game) {
		stage = new Stage();
		this.game = game;
		game.addObserver(this);
		game.getBoard().addObserver(this);
		stage.setTitle(UiConstants.GAME_NAME);
		stage.setScene(buildScene());
		
		initAlerts();
	}
	
	private void initAlerts() {
		setTokenAlert = new Alert(Alert.AlertType.INFORMATION);
		setTokenAlert.setHeaderText(SETTOKEN_ALERTTEXT);
	}

	public void show() {
		Platform.runLater(stage::show);
	}

	public void hide() {
		Platform.runLater(stage::hide);
	}
	
	public void showSetTokenAlert() {
		Platform.runLater(() -> setTokenAlert.show());
	}
	
	public void hideSetTokenAlert() {
		Platform.runLater(() -> setTokenAlert.hide());
	}
	
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
	
	public void setOpenRulesListener(OpenRulesListener listener) {
		openRulesListener = listener;
	}
	
	public void setProbeTokenLocationsListener(ProbeTokenLocationsListener listener) {
		probeTokenLocationListener = listener;
	}

	public void setSetTokenListener(SetTokenListener setTokenListener) {
		this.setTokenListener = setTokenListener;
	}
	
	public void setCurrentPlayerName(String currentPlayerName) {
		Platform.runLater(() -> {
			titleLabel.setText(currentPlayerName);
			titleLabel.setStyle(getColorStringFromPlayer(playerId));
		});
	}
	
	public void setGameNameLabel(String gameName) {
		Platform.runLater(() -> gameNameLabel.setText(gameName));
	}
	
	public void setGameModeLabel(String gameModeName) {
		Platform.runLater(() -> gameModeLabel.setText(gameModeName));
	}
	
	public void receiveProbeTokenLocations(List<Integer> locations) {
		Platform.runLater(() -> {
			locationListView.getSelectionModel().clearSelection();
			for (int i : locations) {
				if (locationCellIndices.containsKey(i)) {
					locationListView.getSelectionModel().select(locationCellIndices.get(i));
				}
			}
		});
	}
	
	public void setTokentypeList(List<TokenType> tokenTypes) {
		this.tokenTypeList = tokenTypes;
	}
	
	public void setLeaveGameListener(LeaveGameListener listener) {
		leaveGameListener = listener;
	}
	
	/**
	 * 
	 * @param countdownMillis Duration of the preround in millis
	 * 
	 */
	public void startPreround(int countdownMillis) {
		Platform.runLater(() -> {
			hideTokensInTokenListView = false;
			setLabelCountdown(PREROUND_LABEL_TEXT, countdownMillis);
			setMouseTransparent(true);
		});
	}

	/**
	 * 
	 * @param countdownMillis Duration of the round in millis
	 * 
	 */
	public void startRound(int countdownMillis) {
		Platform.runLater(() -> {
			setLabelCountdown(ROUND_LABEL_TEXT, countdownMillis);
			setMouseTransparent(false);
		});
	}
	
	/**
	 * 
	 * @param countdownMillis Duration of the round in millis
	 * 
	 */
	public void startPostround(int countdownMillis) {
		Platform.runLater(() -> {
			hideTokensInTokenListView = true;
			setLabelCountdown(POSTROUND_LABEL_TEXT, countdownMillis);
			setMouseTransparent(false);
		});
	}
	
	public void calculatePlayerColors() {
		Color[] colors = new ColorGenerator().getDistinctColors(game.getPlayers().size());
		for (int i = 0; i < colors.length; i++) {
			playerColorHashMap.put(game.getPlayers().get(i).getId(), colors[i]);
		}
	}
	
	public void showWinnerScoreBoard(int winnerId, String winningReason, List<Player> players) {
		Platform.runLater(() -> {
			String winnerName = game.getPlayerNameById(winnerId);
			winnerLabel.setText(winnerName + VICTORY_SUFFIX);
			winningReasonLabel.setText(winningReason);
			scoreboardPlayerListProperty.clear();
			scoreboardPlayerListProperty.set(FXCollections.observableArrayList(players));
			leaveButton.setVisible(true);
			scoreBoardNode.setVisible(true);
			gameFieldPane.setVisible(false);
			countdownLabel.setVisible(false);
			stateLabel.setVisible(false);
		});
	}
	
	public void resetUi() {
		hideTokensInTokenListView = false;
		locationCellIndices.clear();
		playerColorHashMap.clear();
		leaveButton.setVisible(false);
		scoreBoardNode.setVisible(false);
		gameFieldPane.setVisible(true);
		countdownLabel.setVisible(true);
		stateLabel.setVisible(true);
	}
	
	
	private void setLabelCountdown(String labelText, int countdownMillis) {
		stateLabel.setText(labelText);
		countdown.set(countdownMillis / 1000);
		Timeline timeline = new Timeline();
		timeline.getKeyFrames()
				.add(new KeyFrame(Duration.seconds(countdownMillis / 1000 + 1), new KeyValue(countdown, 0)));
		timeline.playFromStart();
	}

	private void setMouseTransparent(boolean transparent) {
		locationListView.setOpacity(transparent ? 0.5 : 1);
		locationListView.setMouseTransparent(transparent);
		locationListView.setFocusTraversable(!transparent);
		tokenListView.setOpacity(transparent ? 0.5 : 1);
		tokenListView.setMouseTransparent(transparent);
		tokenListView.setFocusTraversable(!transparent);
	}
	
	
	private String getColorStringFromPlayer(int playerId) {
		return UiConstants.PLAYER_NAME_COLOR + ": "
				+ ClientUtil.convertColorToHexString(playerColorHashMap.get(playerId)) + ";";
	}
	
	private Scene buildScene() {
		VBox root = new VBox();
		root.setSpacing(20);
		root.setPadding(new Insets(29));
		root.setAlignment(Pos.CENTER);
		root.getChildren().addAll(buildTopPane(), 
				buildGameFieldPane(), buildPlayerListPane(), buildBottomPane());
				
		Scene s = new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT);
		s.getStylesheets().add((getClass().getResource(UiConstants.STYLE_SHEET_FILE)).toExternalForm());
		return s;
		
	}
	
	private Node buildTopPane() {
		StackPane outerBox = new StackPane();
		outerBox.setAlignment(Pos.CENTER);
		VBox v = new VBox(10);
		titleLabel.getStyleClass().add(UiConstants.NAME_LABEL);
		HBox h = new HBox(10);
		gameNameLabel.getStyleClass().add(UiConstants.GAMENAME_LABEL);
		gameModeLabel.getStyleClass().add(UiConstants.GAMEMODE_LABEL);
		h.getChildren().addAll(gameNameLabel, gameModeLabel);
		v.getChildren().addAll(titleLabel, h);
		v.setAlignment(Pos.CENTER_LEFT);
		outerBox.getChildren().addAll(v, buildCountDownPane());
		return outerBox;
	}

	private Node buildCountDownPane() {
		VBox v = new VBox();
		v.setSpacing(20);
		v.setAlignment(Pos.CENTER);
		stateLabel.getStyleClass().add(UiConstants.TITLE_LABEL);
		countdownLabel.getStyleClass().add(UiConstants.COUNTDOWN_LABEL);
		countdownLabel.textProperty().bind(countdown.asString());
		v.getChildren().addAll(stateLabel, countdownLabel);
		return v;
	}

	private Node buildGameFieldPane() {
		StackPane stack = new StackPane();
		gameFieldPane = new HBox();
		gameFieldPane.setSpacing(20);
		gameFieldPane.setPadding(new Insets(20));
		gameFieldPane.getChildren().addAll(buildLocationList(), buildTokenList());
		stack.getChildren().addAll(gameFieldPane, buildScoreboard());
		return stack;
	}

	private Node buildScoreboard() {
		scoreBoardNode = new VBox(10);
		scoreBoardNode.setAlignment(Pos.CENTER);
		ListView<Player> scoreboardPlayerListView = new ListView<>();
		scoreboardPlayerListView.setMaxSize(400, 400);
		scoreboardPlayerListView.setPrefSize(400, 400);
		scoreboardPlayerListView.setMouseTransparent(true); // make list elements unselectable via mouse
		scoreboardPlayerListView.setFocusTraversable(false); // make list elements unselectable via keyboard
		scoreboardPlayerListView.itemsProperty().bind(scoreboardPlayerListProperty);
		scoreboardPlayerListView.setCellFactory(elem -> new ListCell<Player>() {
			@Override
			protected void updateItem(Player player, boolean empty) {
				super.updateItem(player, empty);

				if (empty || player == null) {
					setText(null);
				} else {
					HBox h = new HBox(5);
					Label playerNameLabel = new Label(player.getName());
					playerNameLabel.setPrefWidth(150);
					playerNameLabel.getStyleClass().clear();
					playerNameLabel.setTextFill(playerColorHashMap.get(player.getId()));
					h.getChildren().addAll(playerNameLabel, new Label("" + player.getPoints()));
					setGraphic(h);
				}
			}
		});
		winnerLabel.getStyleClass().add(UiConstants.TITLE_LABEL);
		winningReasonLabel.getStyleClass().add(UiConstants.GAMEMODE_LABEL);
		scoreBoardNode.getChildren().addAll(winnerLabel, winningReasonLabel, scoreboardPlayerListView);
		scoreBoardNode.setVisible(false);
		return scoreBoardNode;
	}

	private Node buildLocationList() {
		VBox v = new VBox();
		v.setSpacing(10);
		Label locationLabel = new Label(LOCATIONLIST_LABEL_TEXT);
		locationLabel.getStyleClass().add(UiConstants.DESCRIPTION_LABEL);
		v.getChildren().add(locationLabel);
		locationListView = new ListView<>();
		locationListView.setPrefSize(LISTVIEW_PREFWIDTH, LISTVIEW_PREFHEIGHT);
		locationListView.setMouseTransparent(true); 
		locationListView.setFocusTraversable(false); 
		locationListView.itemsProperty().bind(locationListProperty);
		locationListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		locationListView.getStyleClass().add(UiConstants.LOCATION_LIST);
		locationListProperty.set(FXCollections.observableArrayList(
				getSortedLocationList(game.getBoard().getLocations())));
		locationListView.setCellFactory(elem -> {
			ListCell<Location> locCell = new ListCell<Location>() {
				@Override
				protected void updateItem(Location location, boolean empty) {
					super.updateItem(location, empty);
					
					setPrefHeight(TOKEN_RADIUS * 2 + 2);
					if (empty) {
						setGraphic(null);
					} else {
						locationCellIndices.put(location.getId(), getIndex()); //Needed for probe token highlight
						HBox hBox = new HBox(5);
						hBox.setAlignment(Pos.CENTER_LEFT);

						String points = "";
						if (location.getPoints() != Integer.MIN_VALUE) {
							points = " (" + location.getPoints() + ")";
						}
						Label locationLabel = new Label(location.getDisplayName() + points);
						locationLabel.getStyleClass().clear();
						locationLabel.setPrefWidth(150);
						locationLabel.setTooltip(new Tooltip(location.getDescription()));
						hBox.getChildren().addAll(locationLabel);

						for (Token t : location.getTokens()) {
							hBox.getChildren().add(buildToken(t));
						}
						setGraphic(hBox);
					}
				}
			};
			locCell.setOnDragOver(e -> {
				Dragboard db = e.getDragboard();
				if (db.hasContent(tokenDataFormat) && draggedToken != null) {
					e.acceptTransferModes(TransferMode.MOVE);
				}
			});
			locCell.setOnDragDropped(e -> {
				Dragboard db = e.getDragboard();
				if (db.hasContent(tokenDataFormat) && draggedToken != null) {
					setTokenListener.onSetLocation(draggedToken, locCell.getItem());
				}
				//Reset Highlight
				locationListView.getSelectionModel().clearSelection();

			});
			return locCell;
		});
		v.getChildren().add(locationListView);
		return v;
	}

	private Node buildTokenList() {
		VBox v = new VBox();
		v.setSpacing(10);
		Label tokensLabel = new Label(TOKENLIST_LABEL_TEXT);
		tokensLabel.getStyleClass().add(UiConstants.DESCRIPTION_LABEL);
		v.getChildren().add(tokensLabel);
		tokenListView = new ListView<>();
		tokenListView.setPrefSize(LISTVIEW_PREFWIDTH, LISTVIEW_PREFHEIGHT);
		tokenListView.setMouseTransparent(true); 
		tokenListView.setFocusTraversable(false); 
		tokenListView.itemsProperty().bind(tokenListProperty);
		tokenListView.getStyleClass().add(UiConstants.TOKEN_LIST);
		game.getBoard().getTokensByPlayerId(playerId);
		createTokenListViewContent();
		v.getChildren().add(tokenListView);
		return v;
	}

	private void createTokenListViewContent() {
		tokenListProperty.set(FXCollections.observableArrayList(tokenTypeList));
		tokenListView.getSelectionModel().clearSelection();
		tokenListView.setCellFactory(elem -> {
			ListCell<TokenType> cell = new ListCell<TokenType>() {
				@Override
				protected void updateItem(TokenType tokenType, boolean empty) {
					super.updateItem(tokenType, empty);
					if (empty || tokenType == null || hideTokensInTokenListView) {
						setText(null);
					} else {
						HBox hBox = new HBox(5);
						hBox.setAlignment(Pos.CENTER_LEFT);
						setPrefHeight(TOKEN_RADIUS * 2 + 2);
						Label tokentypeLabel = new Label(tokenType.getDisplayName());
						tokentypeLabel.getStyleClass().clear();
						tokentypeLabel.setPrefWidth(120);
						tokentypeLabel.setWrapText(true);
						hBox.getChildren().add(tokentypeLabel);
						List<Token> sortedTokenList = game.getBoard().getTokensByPlayerId(playerId);
						sortedTokenList.sort(ClientUtil::compareToken);
						for (Token t : sortedTokenList) {
							if (t.getType() == tokenType.getId()) {
								setTooltip(new Tooltip(getTokenDescription(t.getType())));
								Node n = buildToken(t);
								n.setOnDragDetected(e -> {
									draggedToken = t;
									probeTokenLocationListener.onProbeTokenLocations(draggedToken);
									Dragboard db = n.startDragAndDrop(TransferMode.MOVE);
									ClipboardContent content = new ClipboardContent();
									content.put(tokenDataFormat, DRAGGED_TOKEN);
									db.setContent(content);
								});
								n.setOnDragDone(e -> {
									locationListView.getSelectionModel().clearSelection();
									tokenListView.getSelectionModel().clearSelection();
								});
								hBox.getChildren().add(n);
							}
						}
						setGraphic(hBox);
					}
				}
			};
			return cell;
		});
	}

	private Node buildPlayerListPane() {
		playerListView = new ListView<>();
		playerListView.setMaxSize(800, 400);
		playerListView.setPrefSize(800, 400);
		playerListView.setMouseTransparent(true); // make list elements unselectable via mouse
		playerListView.setFocusTraversable(false); // make list elements unselectable via keyboard
		playerListView.itemsProperty().bind(playerListProperty);
		createPlayerListViewContent();
		return playerListView;
	}

	private void createPlayerListViewContent() {
		playerListProperty.set(FXCollections.observableArrayList(game.getPlayers()));
		playerListView.setCellFactory(elem -> new ListCell<Player>() {
			@Override
			protected void updateItem(Player player, boolean empty) {
				super.updateItem(player, empty);

				if (empty || player == null) {
					setText(null);
				} else {
					HBox h = new HBox(5);
					Label playerNameLabel = new Label(player.getName());
					playerNameLabel.setPrefWidth(150);
					playerNameLabel.getStyleClass().clear();
					playerNameLabel.setTextFill(playerColorHashMap.get(player.getId()));
					h.getChildren().addAll(playerNameLabel, new Label("" + player.getPoints()));
					setGraphic(h);
				}
			}
		});
	}

	private Node buildBottomPane() {
		HBox h = new HBox(10);
		h.setAlignment(Pos.CENTER);
		Button showRulesButton = new Button(RULES_BUTTON_TEXT);
		showRulesButton.setOnMouseClicked(event -> openRulesListener.onOpenRules());
		leaveButton = new Button(LEAVE_BUTTON_TEXT);
		leaveButton.setOnMouseClicked(event -> leaveGameListener.leaveGame());
		leaveButton.setVisible(false);
		h.getChildren().addAll(leaveButton, showRulesButton);

		return h;
	}
	
	private Node buildToken(Token token) {
		Circle circle = new Circle();
		circle.setRadius(TOKEN_RADIUS);
		
		VBox v = new VBox();
		v.setAlignment(Pos.CENTER);
		Label tokenTypeLabel = new Label();
		tokenTypeLabel.getStyleClass().add(UiConstants.TOKEN_LABEL);
		tokenTypeLabel.setMaxWidth(TOKEN_RADIUS * 2);
		tokenTypeLabel.setTextAlignment(TextAlignment.CENTER);
		tokenTypeLabel.setAlignment(Pos.CENTER);
		tokenTypeLabel.setWrapText(true);
		v.getChildren().add(tokenTypeLabel);
		tokenTypeLabel.setTooltip(new Tooltip(getTokenDescription(token.getType())));
		if (token.getType() != Integer.MIN_VALUE) {
			circle.setFill(playerColorHashMap.get(token.getPlayerId()));				
			tokenTypeLabel.setText(token.getDisplayName());
		} else {
			circle.setStroke(Color.BLACK);
			circle.setFill(Color.WHITE);
			tokenTypeLabel.setText("?");
		}
		
		StackPane s = new StackPane();
		s.getChildren().addAll(circle, v);
		return s;
	}
	
	private String getTokenDescription(int tokenTypeId) {
		String desc = "";
		
		List<TokenType> r = tokenTypeList.stream().filter(
				t -> t.getId() == tokenTypeId).collect(Collectors.toList());
		if (r.size() == 1) {
			desc = r.get(0).getDescription();
		}
		return desc;
	}

	@Override
	public void update(Observable o, Object arg) {
		Platform.runLater(() -> {
			locationListProperty.clear();
			locationListProperty.addAll(getSortedLocationList(game.getBoard().getLocations()));
			createTokenListViewContent();
			createPlayerListViewContent();
		});
	}
	
	private List<Location> getSortedLocationList(List<Location> unsortedList) {
		List<Location> l = new ArrayList<>(unsortedList);
		l.sort(ClientUtil::compareLocation);
		return l;
	}
	
	@FunctionalInterface
	public interface ProbeTokenLocationsListener {
		void onProbeTokenLocations(Token token);
	}
	
	@FunctionalInterface
	public interface SetTokenListener {
		void onSetLocation(Token token, Location location);
	}
	
	@FunctionalInterface
	public interface LeaveGameListener {
		void leaveGame();
	}
}
