package edu.kit.informatik.student.knowledgehunt.client.ui;

import java.util.Observable;
import java.util.Observer;

import edu.kit.informatik.student.knowledgehunt.client.ui.ServerLobbyStage.OpenRulesListener;
import edu.kit.informatik.student.knowledgehunt.client.util.UiConstants;
import edu.kit.informatik.student.knowledgehunt.model.GameLobby;
import edu.kit.informatik.student.knowledgehunt.model.Player;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class GameLobbyStage implements Observer {
	private static final String LEAVE_BUTTON_TEXT = "Leave";
	private static final String RULES_BUTTON_TEXT = "Show Rules and Modes";
	private static final String READY_BUTTON_TEXT = "Ready";
	private static final String READY_ALERTTEXT = "Updating Ready status...";
	private static final String LEAVELOBBY_ALERTTEXT = "Leaving Lobby...";
	private static final int WINDOW_HEIGHT = 700;
	private static final int WINDOW_WIDTH = 600;
	
	private Stage stage;
	private ReadyListener readyListener;
	private OpenRulesListener openRulesListener;
	private LeaveLobbyListener leaveLobbyListener;
	
	private GameLobby gameLobby;
	private boolean isReady = true; //That means the next ready button click will send 'true' to the server
	
	private Label titleLabel = new Label();
	private Label gameModeLabel = new Label();
	private Label gameNameLabel = new Label();
	private ToggleButton readyButton  = new ToggleButton();
	private Alert readyAlert;
	private Alert leaveLobbyAlert;
	private ListProperty<Player> playerListListproperty = new SimpleListProperty<>();
	
	

	
	public GameLobbyStage(GameLobby gameLobby) {
		stage = new Stage();
		
		this.gameLobby = gameLobby;
		gameLobby.addObserver(this);
		
		stage.setTitle(UiConstants.GAME_NAME);
		stage.setScene(buildScene());
		initAlerts();
	}

	private void initAlerts() {
		readyAlert = new Alert(Alert.AlertType.INFORMATION);
		readyAlert.setHeaderText(READY_ALERTTEXT);
		
		leaveLobbyAlert = new Alert(Alert.AlertType.INFORMATION);
		leaveLobbyAlert.setHeaderText(LEAVELOBBY_ALERTTEXT);
	}

	public void show() {
		Platform.runLater(stage::show);

	}

	public void hide() {
		Platform.runLater(stage::hide);
	}
	
	public void showReadyAlert() {
		Platform.runLater(() -> readyAlert.show());
	}
	
	public void hideReadyAlert() {
		Platform.runLater(() -> readyAlert.hide());
	}
	
	public void showLeaveLobbyAlert() {
		Platform.runLater(() -> leaveLobbyAlert.show());
	}
	
	public void hideLeaveLobbyAlert() {
		Platform.runLater(() -> leaveLobbyAlert.hide());
	}

	public void setCurrentPlayerName(String currentPlayerName) {
		Platform.runLater(() -> titleLabel.setText("Prepare for the hunt, " + currentPlayerName + "!"));
	}
	
	public void setGameNameLabel(String gameName) {
		Platform.runLater(() -> gameNameLabel.setText(gameName));
	}
	
	public void setGameModeLabel(String gameModeName) {
		Platform.runLater(() -> gameModeLabel.setText("- " + gameModeName + " -"));
	}

	public void setReadyListener(ReadyListener readyListener) {
		this.readyListener = readyListener;
	}
	
	public void setRulesOpenedListener(OpenRulesListener listener) {
		openRulesListener = listener;
	}
	
	public void setLeaveLobbyListener(LeaveLobbyListener listener) {
		leaveLobbyListener = listener;
	}
	
	public void resetUI() {
		isReady = true;
		readyButton.setSelected(false);
	}
	
	private Scene buildScene() {
		VBox root = new VBox();
		root.setAlignment(Pos.TOP_CENTER);
		root.setPadding(new Insets(20));
		root.setSpacing(20);
		
		root.getChildren().add(buildTopPane());
		root.getChildren().add(buildPlayerListPane());
		root.getChildren().add(buildBottomPane());
		
		Scene scene = new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT);
		scene.getStylesheets().add((getClass().getResource(UiConstants.STYLE_SHEET_FILE)).toExternalForm());
		return scene;
	}

	private Node buildTopPane() {
		VBox v = new VBox();
		v.setAlignment(Pos.CENTER);
		v.setSpacing(10);
		titleLabel.getStyleClass().add(UiConstants.TITLE_LABEL);
		gameModeLabel.getStyleClass().add(UiConstants.GAMEMODE_LABEL);
		gameNameLabel.getStyleClass().add(UiConstants.GAMENAME_LABEL);
		v.getChildren().addAll(titleLabel, gameNameLabel, gameModeLabel);
		
		return v;
	}

	private Node buildPlayerListPane() {
		TableView<Player> playerListTableView = new TableView<>();
		playerListTableView.setMaxSize(400, 800);
		playerListTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY); //Hide empty columns
		playerListTableView.itemsProperty().bind(playerListListproperty);
		
		TableColumn<Player, String> playerNameColumn = new TableColumn<>("Player");
		playerNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
		playerNameColumn.prefWidthProperty().bind(playerListTableView.widthProperty().multiply(0.69));
		playerNameColumn.setResizable(false);
		TableColumn<Player, Boolean> readyColumn = new TableColumn<>(READY_BUTTON_TEXT);
		readyColumn.setCellValueFactory(new PropertyValueFactory<>("ready"));
		readyColumn.prefWidthProperty().bind(playerListTableView.widthProperty().multiply(0.3));
		readyColumn.setResizable(false);
		
		playerListTableView.getColumns().add(playerNameColumn);
		playerListTableView.getColumns().add(readyColumn);
		
		return playerListTableView;
	}

	private Node buildBottomPane() {
		HBox h = new HBox();
		h.setSpacing(20);
		h.setAlignment(Pos.CENTER);
		
		readyButton = new ToggleButton(READY_BUTTON_TEXT);
		readyButton.setOnMouseClicked(event -> {
			readyListener.playerIsReady(isReady);
			isReady = !isReady;
		});
		
		Button showRulesButton = new Button(RULES_BUTTON_TEXT);
		showRulesButton.setOnMouseClicked(event -> openRulesListener.onOpenRules());
		
		Button leaveLobbyButton = new Button(LEAVE_BUTTON_TEXT);
		leaveLobbyButton.setOnMouseClicked(event -> leaveLobbyListener.leaveLobby());

		h.getChildren().addAll(readyButton, showRulesButton, leaveLobbyButton);
		return h;
	}

	@FunctionalInterface
	public interface ReadyListener {
		void playerIsReady(boolean ready);
	}
	
	
	@FunctionalInterface
	public interface LeaveLobbyListener {
		void leaveLobby();
	}

	@Override
	public void update(Observable o, Object arg) {
		Platform.runLater(() -> {
			playerListListproperty.clear();
			playerListListproperty.set(FXCollections.observableArrayList(gameLobby.getPlayers()));
		});
		
	}
}
