package edu.kit.informatik.student.knowledgehunt.client.util;

public final class UiConstants {
	public static final String STYLE_SHEET_FILE = "khstyle.css";
	public static final String GAME_NAME = "Knowledge Hunt";

	
	//constants for custom css classes
	public static final String DESCRIPTION_LABEL = "description-label";
	public static final String PLAYER_NAME_COLOR = "player-name-color";
	public static final String NAME_LABEL = "name-label";
	public static final String GAMENAME_LABEL = "gamename-label";
	public static final String GAMEMODE_LABEL = "gamemode-label";
	public static final String TITLE_LABEL = "title-label";
	public static final String COUNTDOWN_LABEL = "countdown-label";
	public static final String TOKEN_LABEL = "token-label";
	public static final String INFO_LABEL = "info-label";
	public static final String TOKEN_LIST = "token-list";
	public static final String LOCATION_LIST = "location-list";
	public static final String RULES_LABEL = "rules-label";
	
	
	private UiConstants() {

	}
}
