package edu.kit.informatik.student.knowledgehunt.client.ui;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import edu.kit.informatik.student.knowledgehunt.client.util.UiConstants;
import edu.kit.informatik.student.knowledgehunt.model.Server;
import edu.kit.informatik.student.knowledgehunt.model.ServerList;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ServerSelectStage implements Observer {
	private static final String CONNECTING_ALERTTEXT = "Connecting...";
	private static final int WINDOW_HEIGHT = 600;
	private static final int WINDOW_WIDTH = 400;

	private Stage stage;

	/**
	 * listProperty is needed to update the serverListView when the ServerList is
	 * updated
	 */
	private ListProperty<Server> listProperty = new SimpleListProperty<>();
	private ServerSelectedListener serverSelectedListener;
	private List<Server> serverList;
	private TextField userNameTextField = new TextField();
	private Alert connectingAlert;

	/**
	 * Used to display a message in case of invalid user input (user name, port)
	 */
	private Label infoLabel = new Label();

	public ServerSelectStage(Stage stage, ServerList serverList) {
		this.stage = stage;
		serverList.addObserver(this);
		this.serverList = serverList.getServers();
		stage.setTitle(UiConstants.GAME_NAME);
		stage.setScene(buildScene());
		initConnectingAlert();
	}

	private void initConnectingAlert() {
		connectingAlert = new Alert(Alert.AlertType.INFORMATION);
		connectingAlert.setHeaderText(CONNECTING_ALERTTEXT);
	}

	public void show() {
		Platform.runLater(stage::show);
	}

	public void hide() {
		Platform.runLater(stage::hide);
	}

	public void showConnectingAlert() {
		Platform.runLater(() -> connectingAlert.show());
	}
	
	public void hideConnectingAlert() {
		Platform.runLater(() -> connectingAlert.hide());
	}
	public void setServerSelectedListener(ServerSelectedListener serverSelectedListener) {
		this.serverSelectedListener = serverSelectedListener;
	}

	private Scene buildScene() {
		VBox root = new VBox(10);
		root.setAlignment(Pos.CENTER);
		root.getChildren().add(buildConnectPane());
		root.getChildren().add(buildServerListPane());
		Scene scene = new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT);
		scene.getStylesheets().add((getClass().getResource(UiConstants.STYLE_SHEET_FILE)).toExternalForm());
		return scene;
	}

	private Pane buildConnectPane() {
		GridPane connectPane = new GridPane();
		TextField serverIPTextField = new TextField();
		TextField serverPortTextField = new TextField();

		connectPane.setAlignment(Pos.CENTER);
		connectPane.setHgap(10);
		connectPane.setVgap(10);
		Label userNameLabel = new Label("User Name:");
		userNameLabel.getStyleClass().add(UiConstants.DESCRIPTION_LABEL);
		connectPane.add(userNameLabel, 0, 1);
		connectPane.add(userNameTextField, 1, 1);
		Label ipLabel = new Label("Server IP:");
		ipLabel.getStyleClass().add(UiConstants.DESCRIPTION_LABEL);
		connectPane.add(ipLabel, 0, 2);
		connectPane.add(serverIPTextField, 1, 2);
		Label portLabel = new Label("Server Port:");
		portLabel.getStyleClass().add(UiConstants.DESCRIPTION_LABEL);
		connectPane.add(portLabel, 0, 3);
		connectPane.add(serverPortTextField, 1, 3);

		HBox h = new HBox();
		h.setAlignment(Pos.CENTER);
		infoLabel.getStyleClass().add(UiConstants.INFO_LABEL);
		h.getChildren().add(infoLabel);
		connectPane.add(h, 0, 5, 2, 1);
		

		Button connectBtn = new Button("Connect");
		connectBtn.setDefaultButton(true); //React on pressing ENTER
		connectBtn.setOnAction(event -> {
			try {
				int port = Integer.parseInt(serverPortTextField.getText());
				connectToServer(new Server(serverIPTextField.getText(), port));
			} catch (NumberFormatException n) {
				showInfoMessage("Invalid port");
			}
		});
		VBox v = new VBox();
		v.setAlignment(Pos.CENTER);
		v.getChildren().add(connectBtn);
		connectPane.add(v, 0, 4, 2, 1);
		return connectPane;
	}

	private Pane buildServerListPane() {
		VBox vbox = new VBox();
		vbox.setAlignment(Pos.CENTER);
		vbox.setSpacing(10);
		ListView<Server> serverListView = new ListView<>();
		serverListView.setMaxWidth(300);
		serverListView.setPrefSize(200, 300);
		serverListView.itemsProperty().bind(listProperty);
		listProperty.set(FXCollections.observableArrayList(serverList));
		
		serverListView.setCellFactory(elem -> new ListCell<Server>() {
			@Override
			protected void updateItem(Server server, boolean empty) {
				super.updateItem(server, empty);

				if (empty || server == null) {
					setText(null);
				} else {
					setText(server.getName() + " (" + server.getIp() +  ":" + server.getPort() + ")");
				}
			}
		});
		serverListView.setOnMouseClicked(event -> {
			if (event.getClickCount() == 2) {
				Server selectedServer = serverListView.getSelectionModel().getSelectedItem();
				if (selectedServer != null) {
					connectToServer(selectedServer);
				}
			}
		});
		Label serversLabel = new Label("Discovered servers");
		serversLabel.getStyleClass().add(UiConstants.DESCRIPTION_LABEL);
		vbox.getChildren().add(serversLabel);
		vbox.getChildren().add(serverListView);
		return vbox;
	}

	private void connectToServer(Server server) {
		String userName = userNameTextField.getText();
		if (checkUserName(userName)) {

			serverSelectedListener.onServerSelected(server, userName);
		} else {
			showInfoMessage("Please enter a user name");

		}
	}

	private void showInfoMessage(String message) {
		infoLabel.setText(message);
		FadeTransition ft = new FadeTransition(Duration.millis(1337), infoLabel);
		ft.setFromValue(1.0);
		ft.setToValue(0.0);
		ft.play();
	}

	@Override
	public void update(Observable o, Object arg) {
		Platform.runLater(() -> {
			listProperty.clear();
			listProperty.addAll(serverList);
		});
	}

	private boolean checkUserName(String name) {
		return !name.isEmpty();
	}

	@FunctionalInterface
	public interface ServerSelectedListener {
		void onServerSelected(Server server, String username);
	}
}
