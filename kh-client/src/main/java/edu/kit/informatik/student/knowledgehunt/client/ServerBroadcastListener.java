package edu.kit.informatik.student.knowledgehunt.client;

import edu.kit.informatik.student.knowledgehunt.model.Server;
import edu.kit.informatik.student.knowledgehunt.model.ServerList;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.DiscoveryMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.logging.Logger;

class ServerBroadcastListener {
	private static final Logger LOG = Logger.getLogger(ServerBroadcastListener.class.getName());
	private DatagramSocket serverSocket;
	private final ServerList serverList;

	private Thread listenerThread = new Thread() {
		@Override
		public void run() {
			try {
				byte[] receiveData = new byte[DiscoveryMessage.MAX_PACKET_LENGTH];
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
				while (!isInterrupted()) {
					serverSocket.receive(receivePacket);
					try {
						DiscoveryMessage message = (DiscoveryMessage) Message.parse(receiveData);
						Server server = new Server();
						server.setName(message.getName());
						server.setIp(message.getIpAdress());
						server.setPort(message.getPort());
						server.setNumPlayers(message.getNumPlayers());
						server.setNumGameLobbies(message.getNumGameLobbies());
						serverList.addServer(server);
					} catch (IllegalArgumentException | ClassCastException e) {
						LOG.info("Received invalid server announcement broadcast");
					}
				}
			} catch (IOException e) {
				if (!isInterrupted()) {
					LOG.severe(e.getMessage());
				}
			}
		}
	};

	ServerBroadcastListener(ServerList serverList) {
		this.serverList = serverList;
	}

	void start() throws SocketException {
			serverSocket = new DatagramSocket(DiscoveryMessage.PORT);
			listenerThread.start();
	}

	void stop() {
		try {
			listenerThread.interrupt();
			if (serverSocket != null) {
				serverSocket.close();
			}
			listenerThread.join();
		} catch (InterruptedException e) {
			LOG.severe("Unable to stop ServerBroadcastListener:" + e.getMessage());
		}
	}
}
