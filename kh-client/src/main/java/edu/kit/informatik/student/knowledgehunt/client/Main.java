package edu.kit.informatik.student.knowledgehunt.client;

import javafx.application.Application;

public final class Main {

	private Main() {

	}

	public static void main(String[] parameters) {
		Application.launch(Controller.class, parameters);
	}
}
