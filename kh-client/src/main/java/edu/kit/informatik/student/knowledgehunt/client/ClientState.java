package edu.kit.informatik.student.knowledgehunt.client;

public enum ClientState {
	DISCONNECTED,
	SERVER_LOBBY,
	GAME_LOBBY,
	PREROUND,
	ROUND,
	POSTROUND
}