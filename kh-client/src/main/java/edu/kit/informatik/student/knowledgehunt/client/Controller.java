package edu.kit.informatik.student.knowledgehunt.client;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;

import com.google.common.annotations.VisibleForTesting;

import edu.kit.informatik.student.knowledgehunt.client.ui.GameLobbyStage;
import edu.kit.informatik.student.knowledgehunt.client.ui.GameStage;
import edu.kit.informatik.student.knowledgehunt.client.ui.RuleDisplayStage;
import edu.kit.informatik.student.knowledgehunt.client.ui.ServerLobbyStage;
import edu.kit.informatik.student.knowledgehunt.client.ui.ServerSelectStage;
import edu.kit.informatik.student.knowledgehunt.model.Game;
import edu.kit.informatik.student.knowledgehunt.model.GameLobby;
import edu.kit.informatik.student.knowledgehunt.model.Location;
import edu.kit.informatik.student.knowledgehunt.model.Server;
import edu.kit.informatik.student.knowledgehunt.model.ServerList;
import edu.kit.informatik.student.knowledgehunt.model.ServerLobby;
import edu.kit.informatik.student.knowledgehunt.model.Token;
import edu.kit.informatik.student.knowledgehunt.protocol.MessageSocket;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ConnectionRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ConnectionResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.CreateLobbyRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.CreateLobbyResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ErrorMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.GameLobbyPlayerListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.GameModeListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.InitLocationsMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.InitTokenTypesMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.JoinLobbyRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.JoinLobbyResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.LeaveLobbyRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.LeaveLobbyResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.LobbyListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.PlayerListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ProbeTokenLocationsRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ProbeTokenLocationsResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.PushTextMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ReadyStatusMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ScoreboardMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.SetTokenRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.StateTransitionMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.TokenUpdateMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.TokensMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageReceiver;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

public class Controller extends Application implements MessageSocket.SocketErrorListener {
	private static final int NOT_IMPLEMENTED = 22;

	private static final Logger LOG = Logger.getLogger(Controller.class.getName());

	@VisibleForTesting
	ServerSelectStage serverSelectStage;
	@VisibleForTesting
	ServerLobbyStage serverLobbyStage;
	@VisibleForTesting
	GameLobbyStage gameLobbyStage;
	@VisibleForTesting
	GameStage gameStage;
	@VisibleForTesting
	RuleDisplayStage ruleDisplayStage;

	@VisibleForTesting
	UiListeners uiListeners = new UiListeners();

	private ClientState state = ClientState.DISCONNECTED;
	private MessageSocket messageSocket;
	private ServerLobby serverLobby = new ServerLobby();
	private GameLobby gameLobby = new GameLobby();
	private Game game = new Game();
	private ServerList serverList = new ServerList();
	private ServerBroadcastListener serverBroadcastListener = new ServerBroadcastListener(serverList);
	private String currentPlayerName;
	private int createLobbyRequestId;
	private int probeTokenRequestId;

	@Override
	public void init() {

	}

	@Override
	public void start(Stage stage) {
		try {
			serverBroadcastListener.start();
		} catch (SocketException e) {
			LOG.severe("Unable to start serverBroadcastListener: " + e.getMessage());
			LOG.severe("Ignoring server discovery broadcasts.");
		}

		createStages(stage);
		setupUiListeners();
		serverSelectStage.show();
	}

	void createStages(Stage stage) {
		serverSelectStage = new ServerSelectStage(stage, serverList);
		serverLobbyStage = new ServerLobbyStage(serverLobby);
		gameLobbyStage = new GameLobbyStage(gameLobby);
		gameStage = new GameStage(game);
		ruleDisplayStage = new RuleDisplayStage(serverLobby);
	}

	private void setupUiListeners() {
		serverSelectStage.setServerSelectedListener(this::connectServer);
		uiListeners.serverSelectedListener = this::connectServer;
		
		serverLobbyStage.setGameSelectedListener(this::joinLobby);
		uiListeners.gameSelectedListener = this::joinLobby;

		serverLobbyStage.setLeaveServerListener(this::leaveServer);
		uiListeners.leaveServerListener = this::leaveServer;
		
		serverLobbyStage.setCreateGameLobbyListener(this::createLobby);
		uiListeners.createGameLobbyListener = this::createLobby;

		serverLobbyStage.setOpenRulesListener(this::openRules);
		uiListeners.openRulesListener = this::openRules;
		
		gameLobbyStage.setRulesOpenedListener(this::openRules);
		
		gameLobbyStage.setReadyListener(this::markReady);
		uiListeners.readyListener = this::markReady;

		gameLobbyStage.setLeaveLobbyListener(this::leaveLobby);
		uiListeners.leaveLobbyListener = this::leaveLobby;
		
		gameStage.setProbeTokenLocationsListener(this::probeTokenLocation);
		uiListeners.probeTokenLocationsListener = this::probeTokenLocation;
		
		gameStage.setSetTokenListener(this::setToken);
		uiListeners.setTokenListener = this::setToken;
		
		gameStage.setLeaveGameListener(this::leaveGame);
		uiListeners.leaveGameListener = this::leaveGame;
		
		gameStage.setOpenRulesListener(this::openRules);
	}
	
	private void openRules() {
		ruleDisplayStage.show();
	}
	
	@MessageReceiver(type = StateTransitionMessage.class)
	public void stateTransitionReceived(StateTransitionMessage message) {
		int countdownMillis = message.getMilliseconds();
		StateTransitionMessage.State nextState = message.getNextState();
		if (state == ClientState.GAME_LOBBY && nextState == StateTransitionMessage.State.PREROUND) {
			gameStage.show();
			gameLobbyStage.hide();
			state = ClientState.PREROUND;
			gameStage.startPreround(countdownMillis);
		} else if (state == ClientState.PREROUND && nextState == StateTransitionMessage.State.ROUND) {
			state = ClientState.ROUND;
			gameStage.startRound(countdownMillis);
		} else if (state == ClientState.ROUND && nextState == StateTransitionMessage.State.POSTROUND) {
			state = ClientState.POSTROUND;
			gameStage.startPostround(countdownMillis);
		} else if (state == ClientState.POSTROUND && nextState == StateTransitionMessage.State.PREROUND) {
			state = ClientState.PREROUND;
			gameStage.startPreround(countdownMillis);
		} else if (state == ClientState.POSTROUND && nextState == StateTransitionMessage.State.LOBBY) {
			state = ClientState.SERVER_LOBBY;
		} else {
			writeInvalidStateErrorMessage();
		}
	}


	private void connectServer(Server server, String username) {
		serverSelectStage.showConnectingAlert();
		try {
			this.currentPlayerName = username;
			Socket socket = new Socket(server.getIp(), server.getPort());
			messageSocket = new MessageSocket(socket);
			messageSocket.setSocketErrorListener(this);
			messageSocket.setMessageListener(this);
			messageSocket.listen();
			messageSocket.writeMessage(new ConnectionRequestMessage(username));
		} catch (IOException e) {
			serverSelectStage.hideConnectingAlert();
			displayErrorMessage(e.getMessage());
			LOG.info(e.getMessage());
		}
	}
	
	@MessageReceiver(type = ConnectionResponseMessage.class)
	public void connectionSucceeded(ConnectionResponseMessage message) {
		if (state == ClientState.DISCONNECTED) {
			serverSelectStage.hideConnectingAlert();
			serverSelectStage.hide();
			serverLobbyStage.show();
			gameStage.setPlayerId(message.getPlayerId());
			serverLobbyStage.setCurrentPlayerName(this.currentPlayerName);
			state = ClientState.SERVER_LOBBY;
		} else {
			writeInvalidStateErrorMessage();
		}
	}
	
	@MessageReceiver(type = PlayerListMessage.class)
	public void playerListReceived(PlayerListMessage message) {
		if (state == ClientState.SERVER_LOBBY) {
			serverLobby.setPlayers(message.getPlayers());
			serverLobby.notifyObservers();
		} else {
			writeInvalidStateErrorMessage();
		}
	}
	
	@MessageReceiver(type = GameModeListMessage.class)
	public void gameModeListReceived(GameModeListMessage message) {
		if (state == ClientState.SERVER_LOBBY) {
			serverLobby.setGameModes(message.getGameModes());
			serverLobby.notifyObservers();
		} else {
			writeInvalidStateErrorMessage();
		}
	}

	@MessageReceiver(type = LobbyListMessage.class)
	public void lobbyListReceived(LobbyListMessage message) {
		if (state == ClientState.SERVER_LOBBY) {
			serverLobby.setGames(message.getLobbies());
			serverLobby.notifyObservers();
		} else {
			writeInvalidStateErrorMessage();
		}
	}
	
	private void createLobby(GameLobby gameLobby) {
		serverLobbyStage.showCreateLobbyAlert();
		CreateLobbyRequestMessage createLobbyMsg = new CreateLobbyRequestMessage();
		createLobbyRequestId = getRandomRequestId();
		createLobbyMsg.setRequestId(createLobbyRequestId);
		createLobbyMsg.setLobbyName(gameLobby.getName());
		createLobbyMsg.setGameModeId(gameLobby.getModeId());
		createLobbyMsg.setMaxPlayers(gameLobby.getMaxPlayers());
		this.gameLobby.setMaxPlayers(gameLobby.getMaxPlayers());
		this.gameLobby.setModeId(gameLobby.getModeId());
		this.gameLobby.setName(gameLobby.getName());
		this.gameLobby.setId(gameLobby.getId());
		this.gameLobby.setNumPlayers(0);
		messageSocket.writeMessage(createLobbyMsg);
	}
	
	@MessageReceiver(type = CreateLobbyResponseMessage.class)
	public void createLobbySucceeded(CreateLobbyResponseMessage message) {
		serverLobbyStage.hideCreateLobbyAlert();
		if (message.getRequestId() != createLobbyRequestId) {
			writeInvalidStateErrorMessage();
		}
	}
	
	private void joinLobby(GameLobby gameLobby) {
		serverLobbyStage.showConnectLobbyAlert();
		JoinLobbyRequestMessage message = new JoinLobbyRequestMessage();
		this.gameLobby.setMaxPlayers(gameLobby.getMaxPlayers());
		this.gameLobby.setModeId(gameLobby.getModeId());
		this.gameLobby.setName(gameLobby.getName());
		this.gameLobby.setId(gameLobby.getId());
		this.gameLobby.setNumPlayers(gameLobby.getNumPlayers());
		message.setLobbyId(gameLobby.getId());
		message.setRequestId(gameLobby.getId());
		messageSocket.writeMessage(message);
	}

	@MessageReceiver(type = JoinLobbyResponseMessage.class)
	public void joinLobbyResponseReceived(JoinLobbyResponseMessage message) {
		serverLobbyStage.hideConnectLobbyAlert();
		if (state == ClientState.SERVER_LOBBY) {
			if (message.getRequestId() == gameLobby.getId()) {
				gameStage.resetUi();
				gameLobbyStage.resetUI();
				gameLobbyStage.setCurrentPlayerName(currentPlayerName);
				gameLobbyStage.setGameNameLabel(gameLobby.getName());
				gameLobbyStage.setGameModeLabel(serverLobby.getGameModeNameFromId(gameLobby.getModeId()));
				gameLobbyStage.show();
				serverLobbyStage.hide();
				state = ClientState.GAME_LOBBY;
			}
		} else {
			writeInvalidStateErrorMessage();
		}
	}
	
	@MessageReceiver(type = GameLobbyPlayerListMessage.class)
	public void gameLobbyPlayerListReceived(GameLobbyPlayerListMessage message) {
		gameLobbyStage.hideReadyAlert();
		if (state == ClientState.SERVER_LOBBY || state == ClientState.GAME_LOBBY) { 
			gameLobby.setPlayers(message.getPlayers());
			game.setPlayers(new ArrayList<>(message.getPlayers()));
			gameLobby.notifyObservers();
			game.notifyObservers();
		} else {
			writeInvalidStateErrorMessage();
		}
	}
	
	private void markReady(boolean ready) {
		gameLobbyStage.showReadyAlert();
		ReadyStatusMessage message = new ReadyStatusMessage();
		message.setReady(ready);
		messageSocket.writeMessage(message);
	}
	
	@MessageReceiver(type = InitLocationsMessage.class)
	public void initLocationsReceived(InitLocationsMessage message) {
		if (state == ClientState.PREROUND) {
			game.getBoard().setLocations(message.getLocations()); 
			game.getBoard().notifyObservers();
		} else {
			writeInvalidStateErrorMessage();
		}
	}
	
	@MessageReceiver(type = InitTokenTypesMessage.class)
	public void initTokenTypesReceived(InitTokenTypesMessage message) {
		gameLobbyStage.hideReadyAlert();
		if (state == ClientState.PREROUND) {
			gameStage.setTokentypeList(message.getTypes());
			gameStage.calculatePlayerColors();
			gameStage.setCurrentPlayerName(currentPlayerName);
			gameStage.setGameNameLabel(gameLobby.getName()); 
			gameStage.setGameModeLabel(serverLobby.getGameModeNameFromId(gameLobby.getModeId()));
		} else {
			writeInvalidStateErrorMessage();
		}
	}
	
	@MessageReceiver(type = TokensMessage.class)
	public void tokensReceived(TokensMessage message) {
		if (state == ClientState.PREROUND) {
			game.getBoard().setTokens(new ArrayList<>(message.getTokens()));
			game.getBoard().notifyObservers();
		} else if (state == ClientState.POSTROUND) {
			game.getBoard().setTokens(new ArrayList<>(message.getTokens()));
			game.getBoard().unhideTokens();
			game.getBoard().notifyObservers();
		} else {
			writeInvalidStateErrorMessage();
		}
	}

	private void probeTokenLocation(Token token) {
		ProbeTokenLocationsRequestMessage probeMessage = new ProbeTokenLocationsRequestMessage();
		probeTokenRequestId = getRandomRequestId();
		probeMessage.setRequestId(probeTokenRequestId);
		probeMessage.setTokenId(token.getId());
		messageSocket.writeMessage(probeMessage);
	}
	
	@MessageReceiver(type = ProbeTokenLocationsResponseMessage.class)
	public void probeTokenLocationReceived(ProbeTokenLocationsResponseMessage message) {
		if (state == ClientState.ROUND && message.getRequestId() == probeTokenRequestId) {
			gameStage.receiveProbeTokenLocations(message.getAllowedLocations());
		} else {
			writeInvalidStateErrorMessage();
		}
	}

	private void setToken(Token token, Location location) {
		gameStage.showSetTokenAlert();
		SetTokenRequestMessage setTokenMessage = new SetTokenRequestMessage();
		setTokenMessage.setRequestId(token.getId());
		setTokenMessage.setLocationId(location.getId());
		setTokenMessage.setTokenId(token.getId());
		messageSocket.writeMessage(setTokenMessage);
	}

	@MessageReceiver(type = TokenUpdateMessage.class)
	public void tokenUpdateReceived(TokenUpdateMessage message) {
		gameStage.hideSetTokenAlert();
		if (state == ClientState.ROUND) {
			game.getBoard().addTokenToLocation(message.getLocationId(), message.getTokenId());
			game.getBoard().removeTokenById(message.getTokenId());
			game.getBoard().notifyObservers();
		} else {
			writeInvalidStateErrorMessage();
		}
	}
	
	@MessageReceiver(type = ScoreboardMessage.class)
	public void scoreBoardReceived(ScoreboardMessage message) {
		if (state == ClientState.POSTROUND) {
			game.updatePlayerPoints(message.getPlayers());
			game.notifyObservers();
			if (message.getWinner() != Integer.MIN_VALUE) { //We have a winner
				gameStage.showWinnerScoreBoard(message.getWinner(), message.getWinningReason(), game.getPlayers());
			}
			
		} else {
			writeInvalidStateErrorMessage();
		}
	}


	private void leaveGame() {
		if (this.state == ClientState.POSTROUND || this.state == ClientState.SERVER_LOBBY) {
			serverLobbyStage.show();
			gameStage.hide();
			this.state = ClientState.SERVER_LOBBY;
		}
	}
	
	private void leaveLobby() {
		gameLobbyStage.showLeaveLobbyAlert();
		LeaveLobbyRequestMessage message = new LeaveLobbyRequestMessage();
		message.setRequestId(gameLobby.getId());
		messageSocket.writeMessage(message);
	}

	@MessageReceiver(type = LeaveLobbyResponseMessage.class)
	public void leaveLobby(LeaveLobbyResponseMessage message) {
		gameLobbyStage.hideLeaveLobbyAlert();
		if (state == ClientState.GAME_LOBBY) {
			if (gameLobby.getId() == message.getRequestId()) {
				serverLobbyStage.show();
				gameLobbyStage.hide();
				state = ClientState.SERVER_LOBBY;
			}
		} else {
			writeInvalidStateErrorMessage();
		}
	}
	
	private void leaveServer() {
		messageSocket.close();
		state = ClientState.DISCONNECTED;

		serverLobby.setGames(new ArrayList<>());
		serverLobby.setGameModes(new ArrayList<>());
		serverLobby.setPlayers(new ArrayList<>());

		serverSelectStage.show();
		serverLobbyStage.hide();
	}
	
	@MessageReceiver(type = PushTextMessage.class)
	public void pushTextReceived(PushTextMessage message) {
		Platform.runLater(() -> new Alert(Alert.AlertType.INFORMATION, message.getText(), ButtonType.OK).show());
	}

	@MessageReceiver(type = ErrorMessage.class)
	public void errorReceived(ErrorMessage message) {
		if (message.getErrorCode() != NOT_IMPLEMENTED) {
			serverSelectStage.hideConnectingAlert();
			serverLobbyStage.hideConnectLobbyAlert();
			serverLobbyStage.hideCreateLobbyAlert();
			gameLobbyStage.hideLeaveLobbyAlert();
			gameLobbyStage.hideReadyAlert();
			gameStage.hideSetTokenAlert();
			displayErrorMessage(message.getErrorMessage());
		}
	}
	
	@VisibleForTesting
	void displayErrorMessage(String errorMessage) {
		Platform.runLater(() -> new Alert(Alert.AlertType.ERROR, errorMessage, ButtonType.OK).show());
	}

	private void writeInvalidStateErrorMessage() {
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setErrorCode(ErrorMessage.ErrorCode.INVALID_STATE);
		errorMessage.setErrorMessage("The message is not allowed in state " + state.name());
		messageSocket.writeMessage(errorMessage);
	}

	@Override
	public void stop() {
		if (messageSocket != null) {
			messageSocket.close();
			messageSocket = null;
		}
		serverBroadcastListener.stop();
		state = ClientState.DISCONNECTED;
	}

	@VisibleForTesting
	ClientState getState() {
		return state;
	}

	@Override
	public void socketError(Exception e) {
		Platform.runLater(() -> {
			new Alert(Alert.AlertType.ERROR, e.getMessage(), ButtonType.OK).show();
			serverSelectStage.hide();
			serverLobbyStage.hide();
			gameLobbyStage.hide();
			gameStage.hide();
			ruleDisplayStage.hide();
			leaveServer();
		});
	}
	
	private int getRandomRequestId() {
		return ThreadLocalRandom.current().nextInt(0, 1001);
	}
}