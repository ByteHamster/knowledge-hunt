package edu.kit.informatik.student.knowledgehunt.client.ui;

import java.util.Observable;
import java.util.Observer;

import edu.kit.informatik.student.knowledgehunt.client.util.ClientUtil;
import edu.kit.informatik.student.knowledgehunt.client.util.UiConstants;
import edu.kit.informatik.student.knowledgehunt.model.GameMode;
import edu.kit.informatik.student.knowledgehunt.model.ServerLobby;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class RuleDisplayStage implements Observer {
	private Stage stage;
	private ServerLobby serverLobby;
	private Label descriptionLabel = new Label();
	
	private static final int WINDOW_HEIGHT = 800;
	private static final int WINDOW_WIDTH = 1100;
	
	private ListProperty<GameMode> gameModeListProperty = new SimpleListProperty<>();

	public RuleDisplayStage(ServerLobby serverLobby) {
		stage = new Stage();
		this.serverLobby = serverLobby;
		serverLobby.addObserver(this);
		stage.setTitle(UiConstants.GAME_NAME);
		stage.setScene(buildScene());
	}

	public void show() {
		Platform.runLater(stage::show);
		Platform.runLater(stage::toFront);
	}

	public void hide() {
		Platform.runLater(stage::hide);
	}
	
	private Scene buildScene() {
		HBox root = new HBox();
		root.setSpacing(20);
		root.setPadding(new Insets(20));
		root.getChildren().addAll(buildModeListPane(), buildDescriptionPane());
		Scene scene = new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT);
		scene.getStylesheets().add((getClass().getResource(UiConstants.STYLE_SHEET_FILE)).toExternalForm());
		return scene;
	}

	private Node buildModeListPane() {
		ListView<GameMode> gameModesListView = new ListView<>();
		gameModesListView.setMaxSize(250, 800);
		gameModesListView.itemsProperty().bind(gameModeListProperty);
		gameModeListProperty.set(FXCollections.observableArrayList(serverLobby.getGameModes()));
		//Make ListView display the GameModes with their names
		gameModesListView.setCellFactory(elem -> new ListCell<GameMode>() {
			@Override
			protected void updateItem(GameMode mode, boolean empty) {
				super.updateItem(mode, empty);

				if (empty || mode == null) {
					setText(null);
				} else {
					setText(mode.getName());
				}
			}
		});
		gameModesListView.setOnMouseClicked(event -> {
			GameMode selected = gameModesListView.getSelectionModel().getSelectedItem();
			if (selected != null) {

				descriptionLabel.setText(ClientUtil.wrapCharactersPerRow(selected.getDescription(), 80));
			}
		});
		
		return gameModesListView;
	}

	private Node buildDescriptionPane() {
		descriptionLabel.getStyleClass().add(UiConstants.RULES_LABEL);
		return descriptionLabel;
	}

	@Override
	public void update(Observable o, Object arg) {
		Platform.runLater(() -> {
			gameModeListProperty.clear();
			gameModeListProperty.addAll(serverLobby.getGameModes());
		});
	}
}
