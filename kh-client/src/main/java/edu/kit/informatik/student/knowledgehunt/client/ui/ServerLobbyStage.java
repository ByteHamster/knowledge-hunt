package edu.kit.informatik.student.knowledgehunt.client.ui;

import java.util.Observable;
import java.util.Observer;

import edu.kit.informatik.student.knowledgehunt.client.util.UiConstants;
import edu.kit.informatik.student.knowledgehunt.model.GameLobby;
import edu.kit.informatik.student.knowledgehunt.model.GameMode;
import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.model.ServerLobby;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ServerLobbyStage implements Observer {

	private static final String PLAYERLIST_LABEL = "Current Players in this Lobby";
	private static final String GAMELOBBIES_LABEL = "Current Games in this Lobby";
	private static final String INVALID_MAXPLAYERS_INFOTEXT = "Invalid Number of Max Players";
	private static final String ENTER_GAMENAME_INFOTEXT = "Please enter a Game Name";
	private static final String SELECT_GAMEMODE_INFOTEXT = "Please select a Game Mode";
	private static final String CREATEGAME_BUTTON_TEXT = "Create New Game";
	private static final String GAMEMODE_COMBOBOX_PROMPT = "Select a Game Mode";
	private static final String MAXPLAYERS_TEXTFIELD_PROMPT = "Max. Number of Players";
	private static final String GAMENAME_TEXTFIELD_PROMPT = "Enter a Game Name";
	private static final String CREATEGAME_LABEL_TEXT = "Create a New Game";
	private static final String DISCONNECT_BUTTON_TEXT = "Disconnect";
	private static final String RULES_BUTTON_TEXT = "Show Rules and Modes";
	private static final String CREATELOBBY_ALERTTEXT = "Creating Lobby...";
	private static final String CONNECTLOBBY_ALERTTEXT = "Connecting to Lobby...";
	private static final int WINDOW_HEIGHT = 500;
	private static final int WINDOW_WIDTH = 1200;

	private Stage stage;
	private ServerLobby serverLobby;
	private GameSelectedListener gameSelectedListener;
	private OpenRulesListener openRulesListener;
	private CreateGameLobbyListener createGameLobbyListener;
	private LeaveServerListener leaveServerListener;
	
	private ListProperty<Player> playerListProperty = new SimpleListProperty<>();
	private ListProperty<GameLobby> gameLobbyListProperty = new SimpleListProperty<>();
	private ListProperty<GameMode> gameModeListProperty = new SimpleListProperty<>();

	private Label infoLabel = new Label();
	private Label titleLabel = new Label();
	private TextField gameNameTextField = new TextField();
	
	private Alert createLobbyAlert;
	private Alert connectLobbyAlert;

	public ServerLobbyStage(ServerLobby serverLobby) {
		this.serverLobby = serverLobby;
		serverLobby.addObserver(this);
		stage = new Stage();

		stage.setTitle(UiConstants.GAME_NAME);
		stage.setScene(buildScene());

		initAlerts();
	}

	private void initAlerts() {
		createLobbyAlert = new Alert(Alert.AlertType.INFORMATION);
		createLobbyAlert.setHeaderText(CREATELOBBY_ALERTTEXT);
		
		connectLobbyAlert = new Alert(Alert.AlertType.INFORMATION);
		connectLobbyAlert.setHeaderText(CONNECTLOBBY_ALERTTEXT);
	}

	public void show() {
		Platform.runLater(stage::show);
	}

	public void hide() {
		Platform.runLater(stage::hide);
	}

	public void showCreateLobbyAlert() {
		Platform.runLater(() -> createLobbyAlert.show());
	}
	
	public void hideCreateLobbyAlert() {
		Platform.runLater(() -> createLobbyAlert.hide());
	}
	
	public void showConnectLobbyAlert() {
		Platform.runLater(() -> connectLobbyAlert.show());
	}
	
	public void hideConnectLobbyAlert() {
		Platform.runLater(() -> connectLobbyAlert.hide());
	}
	
	public void setGameSelectedListener(GameSelectedListener listener) {
		gameSelectedListener = listener;
	}

	public void setLeaveServerListener(LeaveServerListener leaveServerListener) {
		this.leaveServerListener = leaveServerListener;
	}
	public void setOpenRulesListener(OpenRulesListener listener) {
		openRulesListener = listener;
	}

	public void setCreateGameLobbyListener(CreateGameLobbyListener listener) {
		createGameLobbyListener = listener;
	}
	
	public void setCurrentPlayerName(String currentPlayerName) {
		Platform.runLater(() -> {
			titleLabel.setText("Welcome, " + currentPlayerName + "!");
			gameNameTextField.setText(currentPlayerName + "'s lobby");
		});
	}

	private Scene buildScene() {
		BorderPane root = new BorderPane();

		Insets insets = new Insets(10);
		
		Node topNode = buildTitlePane();
		root.setTop(topNode);
		BorderPane.setMargin(topNode, insets);
		BorderPane.setAlignment(topNode, Pos.CENTER);
		
		Node leftNode = buildPlayerListPane();
		root.setLeft(leftNode);
		BorderPane.setMargin(leftNode, insets);

		Node centerNode = buildGamesLobbiesPane();
		root.setCenter(centerNode);
		BorderPane.setMargin(centerNode, insets);

		Node rightNode = buildCreateGamePane();
		root.setRight(rightNode);
		BorderPane.setMargin(rightNode, insets);
		BorderPane.setAlignment(rightNode, Pos.CENTER_LEFT);

		Node bottomNode = buildShowRulesPane();
		root.setBottom(bottomNode);
		BorderPane.setMargin(bottomNode, insets);
		Scene scene = new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT);
		scene.getStylesheets().add((getClass().getResource(UiConstants.STYLE_SHEET_FILE)).toExternalForm());
		return scene;
	}

	private Node buildTitlePane() {
		titleLabel.getStyleClass().add(UiConstants.TITLE_LABEL);		
		return titleLabel;
	}

	private Node buildShowRulesPane() {
		HBox h = new HBox();
		h.setAlignment(Pos.CENTER);
		h.setSpacing(40);

		Button showRulesButton = new Button(RULES_BUTTON_TEXT);
		showRulesButton.setOnMouseClicked(event -> openRulesListener.onOpenRules());

		Button disconnectButton = new Button(DISCONNECT_BUTTON_TEXT);
		disconnectButton.setOnMouseClicked(event -> leaveServerListener.onLeaveServer());

		h.getChildren().addAll(showRulesButton, disconnectButton);

		return h;
	}

	private Node buildCreateGamePane() {
		VBox v = new VBox();
		v.setSpacing(10);
		v.setAlignment(Pos.CENTER);
		v.setPrefWidth(250);
		Label createGameLabel = new Label(CREATEGAME_LABEL_TEXT);
		createGameLabel.getStyleClass().add(UiConstants.DESCRIPTION_LABEL);
		v.getChildren().add(createGameLabel);

		gameNameTextField.setPromptText(GAMENAME_TEXTFIELD_PROMPT);
		v.getChildren().add(gameNameTextField);

		TextField maxPlayersTextField = new TextField();
		maxPlayersTextField.setPrefWidth(100);
		maxPlayersTextField.setPromptText(MAXPLAYERS_TEXTFIELD_PROMPT);
		v.getChildren().add(maxPlayersTextField);

		ComboBox<GameMode> selectModeComboBox = new ComboBox<>();
		selectModeComboBox.setPrefWidth(250);
		selectModeComboBox.setPromptText(GAMEMODE_COMBOBOX_PROMPT);
		selectModeComboBox.itemsProperty().bind(gameModeListProperty);
		gameModeListProperty.set(FXCollections.observableArrayList(serverLobby.getGameModes()));
		// Make the combo box to display the game mode name as entries
		selectModeComboBox.setCellFactory(param -> new ListCell<GameMode>() {
			@Override
			protected void updateItem(GameMode mode, boolean empty) {
				super.updateItem(mode, empty);

				if (empty || mode == null) {
					setText(null);
				} else {
					setText(mode.getName());
				}
			}
		});
		// Make the combo box to display the game mode name when it is chosen by the
		// player
		selectModeComboBox.setButtonCell(new ListCell<GameMode>() {
			@Override
			protected void updateItem(GameMode mode, boolean empty) {
				super.updateItem(mode, empty);

				if (empty || mode == null) {
					setText(null);
				} else {
					setText(mode.getName());
				}
			}
		});

		v.getChildren().add(selectModeComboBox);

		infoLabel.getStyleClass().add(UiConstants.INFO_LABEL);

		Button createGameButton = buildCreateButton(gameNameTextField, maxPlayersTextField, selectModeComboBox);

		v.getChildren().add(createGameButton);
		v.getChildren().add(infoLabel);

		return v;
	}

	private Button buildCreateButton(TextField gameNameTextField, TextField maxPlayersTextField,
			ComboBox<GameMode> selectModeComboBox) {
		Button createGameButton = new Button(CREATEGAME_BUTTON_TEXT);
		createGameButton.setOnMouseClicked(event -> {
			try {
				int maxPlayers = Integer.parseInt(maxPlayersTextField.getText());
				if (!gameNameTextField.getText().isEmpty()) {
					if (!(selectModeComboBox.getValue() == null)) {
						GameLobby g = new GameLobby(0, selectModeComboBox.getValue(), maxPlayers);
						g.setName(gameNameTextField.getText());
						createGameLobbyListener.onCreateGameLobby(g);
					} else {
						showInfoMessage(SELECT_GAMEMODE_INFOTEXT);
					}
				} else {
					showInfoMessage(ENTER_GAMENAME_INFOTEXT);
				}

			} catch (NumberFormatException n) {
				showInfoMessage(INVALID_MAXPLAYERS_INFOTEXT);
			}

		});
		return createGameButton;
	}

	private Node buildGamesLobbiesPane() {
		VBox v = new VBox();
		v.setSpacing(10);
		v.setAlignment(Pos.CENTER);
		v.setMaxSize(800, 1000);
		Label gameListLabel = new Label(GAMELOBBIES_LABEL);
		gameListLabel.getStyleClass().add(UiConstants.DESCRIPTION_LABEL);
		v.getChildren().add(gameListLabel);
		TableView<GameLobby> gameLobbyTableView = new TableView<>();
		gameLobbyTableView.setMaxWidth(700);
		gameLobbyTableView.setPrefSize(700, 300);
		gameLobbyTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY); // Hide empty columns
		gameLobbyTableView.itemsProperty().bind(gameLobbyListProperty);
		gameLobbyTableView.setOnMouseClicked(event -> {
			if (event.getClickCount() == 2) {
				GameLobby selectedGameLobby = gameLobbyTableView.getSelectionModel().getSelectedItem();
				if (selectedGameLobby != null) {
					gameSelectedListener.onGameSelected(selectedGameLobby);
				}
			}
		});
		TableColumn<GameLobby, String> gameNameColumn = new TableColumn<>("Game Name");
		gameNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
		gameNameColumn.prefWidthProperty().bind(gameLobbyTableView.widthProperty().multiply(0.35));
		gameNameColumn.setResizable(false);
		TableColumn<GameLobby, String> gameModeColumn = new TableColumn<>("Game Mode");
		gameModeColumn.setCellValueFactory(new PropertyValueFactory<>("modeId"));
		gameModeColumn.setCellFactory(param -> new TableCell<GameLobby, String>() {
			@Override
			protected void updateItem(String mode, boolean empty) {
				super.updateItem(mode, empty);

				if (empty || mode == null) {
					setText(null);
				} else {
					setText(serverLobby.getGameModeNameFromId(mode));
				}
			}
		});
		gameModeColumn.prefWidthProperty().bind(gameLobbyTableView.widthProperty().multiply(0.34));
		gameModeColumn.setResizable(false);
		TableColumn<GameLobby, Integer> numPlayersColumn = new TableColumn<>("Players");
		numPlayersColumn.setCellValueFactory(new PropertyValueFactory<>("numPlayers"));
		numPlayersColumn.prefWidthProperty().bind(gameLobbyTableView.widthProperty().multiply(0.15));
		numPlayersColumn.setResizable(false);
		
		TableColumn<GameLobby, Integer> maxPlayersColumn = new TableColumn<>("Max. Players");
		maxPlayersColumn.setCellValueFactory(new PropertyValueFactory<>("maxPlayers"));
		maxPlayersColumn.prefWidthProperty().bind(gameLobbyTableView.widthProperty().multiply(0.15));
		maxPlayersColumn.setResizable(false);
		gameLobbyTableView.getColumns().addAll(gameNameColumn, gameModeColumn, numPlayersColumn, maxPlayersColumn);
		gameLobbyListProperty.set(FXCollections.observableArrayList(serverLobby.getGames()));
		v.getChildren().add(gameLobbyTableView);
		return v;
	}

	private Node buildPlayerListPane() {
		VBox v = new VBox();
		v.setSpacing(10);
		v.setAlignment(Pos.CENTER);
		Label playerListLabel = new Label(PLAYERLIST_LABEL);
		playerListLabel.getStyleClass().add(UiConstants.DESCRIPTION_LABEL);
		v.getChildren().add(playerListLabel);
		ListView<Player> playerListView = new ListView<>();
		playerListView.setPrefSize(200, 300);
		playerListView.setMouseTransparent(true); // make list elements unselectable via mouse
		playerListView.setFocusTraversable(false); // make list elements unselectable via keyboard
		playerListView.itemsProperty().bind(playerListProperty);
		playerListProperty.set(FXCollections.observableArrayList(serverLobby.getPlayers()));
		playerListView.setCellFactory(elem -> new ListCell<Player>() {
			@Override
			protected void updateItem(Player player, boolean empty) {
				super.updateItem(player, empty);

				if (empty || player == null) {
					setText(null);
				} else {
					setAlignment(Pos.CENTER);
					setText(player.getName());
				}
			}
		});
		v.getChildren().add(playerListView);
		return v;
	}

	private void showInfoMessage(String message) {
		infoLabel.setText(message);
		FadeTransition ft = new FadeTransition(Duration.millis(1337), infoLabel);
		ft.setFromValue(1.0);
		ft.setToValue(0.0);
		ft.play();
	}
	


	@Override
	public void update(Observable o, Object arg) {
		Platform.runLater(() -> {
			playerListProperty.clear();
			playerListProperty.addAll(serverLobby.getPlayers());
			gameLobbyListProperty.clear();
			gameLobbyListProperty.addAll(serverLobby.getGames());
			if (!gameModeListProperty.getValue().equals(serverLobby.getGameModes())) {
				gameModeListProperty.clear();
				gameModeListProperty.addAll(serverLobby.getGameModes());
			}
		});
	}

	@FunctionalInterface
	public interface GameSelectedListener {
		void onGameSelected(GameLobby game);
	}

	@FunctionalInterface
	public interface OpenRulesListener {
		void onOpenRules();
	}

	@FunctionalInterface
	public interface LeaveServerListener {
		void onLeaveServer();
	}
	
	@FunctionalInterface
	public interface CreateGameLobbyListener {
		void onCreateGameLobby(GameLobby gameLobby);
	}
}
