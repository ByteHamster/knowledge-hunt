package edu.kit.informatik.student.knowledgehunt.client.util;

import javafx.scene.paint.Color;

public class ColorGenerator {
	
	private double brightness;
	private double saturation;
	
	/**
	 * 
	 * @param saturation must be in [0,1]
	 * @param brightness must be in [0,1]
	 */
	public ColorGenerator(double saturation, double brightness) {
		if (saturation < 0 || saturation > 1 || brightness < 0 || brightness > 1) {
			throw new IllegalArgumentException("Invalid arguments");
		}
		this.saturation = saturation;
		this.brightness = brightness;
	}
	
	public ColorGenerator() {
		this(0.6, 0.8);
	}

	public Color[] getDistinctColors(int number) {
		if (number < 1) {
			throw new IllegalArgumentException();
		} else {
			Color[] resultArray = new Color[number];
			for (int i = 0; i < number; i++) {
				resultArray[i] = Color.hsb(((double) (i * 360 / number)), saturation, brightness);
			}
			return resultArray;
		}
	}
}
