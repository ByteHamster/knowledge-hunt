package edu.kit.informatik.student.knowledgehunt.client.util;

import org.apache.commons.text.WordUtils;

import edu.kit.informatik.student.knowledgehunt.model.Location;
import edu.kit.informatik.student.knowledgehunt.model.Token;
import javafx.scene.paint.Color;

public final class ClientUtil {

	private ClientUtil() {
		
	}

	public static String convertColorToHexString(Color c) {
		return String.format("#%02X%02X%02X", (int) (c.getRed() * 255), (int) (c.getGreen() * 255),
				(int) (c.getBlue() * 255));
	}
	
	public static String wrapCharactersPerRow(String text, int limit) {
		return WordUtils.wrap(text, limit);
	}
	
	public static int compareToken(Token a, Token b) {
			int pointOrder = Integer.compare(a.getValue(), b.getValue());
			int nameOrder = a.getDisplayName().compareTo(b.getDisplayName());
			
			if (pointOrder  == 0) {
				return nameOrder;
			} else {
				return pointOrder;
			}
			
	}
	
	public static int compareLocation(Location a, Location b) {
		int pointOrder = new Integer(a.getPoints()).compareTo(new Integer(b.getPoints()));
		int nameOrder = a.getDisplayName().compareTo(b.getDisplayName());
		
		if (pointOrder  == 0) {
			return nameOrder;
		} else {
			return pointOrder;
		}
		
}
}
