package edu.kit.informatik.student.knowledgehunt.client;

import static org.mockito.Mockito.mock;

import edu.kit.informatik.student.knowledgehunt.client.ui.GameLobbyStage;
import edu.kit.informatik.student.knowledgehunt.client.ui.GameStage;
import edu.kit.informatik.student.knowledgehunt.client.ui.RuleDisplayStage;
import edu.kit.informatik.student.knowledgehunt.client.ui.ServerLobbyStage;
import edu.kit.informatik.student.knowledgehunt.client.ui.ServerSelectStage;
import javafx.stage.Stage;

public class MockController extends Controller {

	@Override
	void createStages(Stage stage) {
		serverSelectStage = mock(ServerSelectStage.class);
		serverLobbyStage = mock(ServerLobbyStage.class);
		gameLobbyStage = mock(GameLobbyStage.class);
		gameStage = mock(GameStage.class);
		ruleDisplayStage = mock(RuleDisplayStage.class);
	}

	void displayErrorMessage(String errorMessage) {
		// Do nothing
	}
}