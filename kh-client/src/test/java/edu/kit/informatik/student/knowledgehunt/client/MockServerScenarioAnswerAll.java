package edu.kit.informatik.student.knowledgehunt.client;

import edu.kit.informatik.student.knowledgehunt.protocol.messages.ConnectionRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.ConnectionResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.GameModeListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.JoinLobbyRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.JoinLobbyResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.LeaveLobbyRequestMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.LeaveLobbyResponseMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.LobbyListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.PlayerListMessage;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.MessageReceiver;

public class MockServerScenarioAnswerAll extends MockServerScenario {
	public static final int GAME_LOBBY_ID = 32;
	public static final int PLAYER_ID = 42;

	@MessageReceiver(type = ConnectionRequestMessage.class)
	public void connectionRequest(ConnectionRequestMessage message) {
		writeMessage(new ConnectionResponseMessage(message.getRequestId(), PLAYER_ID));
		writeMessage(new PlayerListMessage());
		writeMessage(new LobbyListMessage());
		writeMessage(new GameModeListMessage());
	}

	@MessageReceiver(type = JoinLobbyRequestMessage.class)
	public void joinLobby(JoinLobbyRequestMessage message) {
		writeMessage(new JoinLobbyResponseMessage(message.getRequestId()));
	}

	@MessageReceiver(type = LeaveLobbyRequestMessage.class)
	public void leaveLobby(LeaveLobbyRequestMessage message) {
		writeMessage(new LeaveLobbyResponseMessage(message.getRequestId()));
	}
}
