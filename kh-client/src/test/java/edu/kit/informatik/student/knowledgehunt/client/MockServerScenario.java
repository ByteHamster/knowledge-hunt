package edu.kit.informatik.student.knowledgehunt.client;

import edu.kit.informatik.student.knowledgehunt.protocol.MessageSocket;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.meta.Message;

abstract class MockServerScenario {
	private MessageSocket socket;

	void setSocket(MessageSocket socket) {
		this.socket = socket;
	}

	void writeMessage(Message message) {
		socket.writeMessage(message);
	}
}
