package edu.kit.informatik.student.knowledgehunt.client;

import edu.kit.informatik.student.knowledgehunt.model.Server;
import edu.kit.informatik.student.knowledgehunt.protocol.MessageSocket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

class MockServer {
	private static final int PORT = 4242;
	private ServerSocket serverSocket;
	private MessageSocket messageSocket;
	private MockServerScenario scenario;

	Server getServerInfo() {
		return new Server(serverSocket.getInetAddress().getHostName(), PORT);
	}

	void start() {
		try {
			serverSocket = new ServerSocket();
			serverSocket.bind(new InetSocketAddress(PORT));
			new Thread(this::waitForClient).start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void waitForClient() {
		try {
			Socket socket = serverSocket.accept();
			messageSocket = new MessageSocket(socket);
			scenario.setSocket(messageSocket);
			messageSocket.listen();
			messageSocket.setMessageListener(scenario);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void stop() {
		try {
			messageSocket.close();
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void setScenario(MockServerScenario scenario) {
		this.scenario = scenario;
	}
}
