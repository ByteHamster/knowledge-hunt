package edu.kit.informatik.student.knowledgehunt.client;

import edu.kit.informatik.student.knowledgehunt.model.Server;
import edu.kit.informatik.student.knowledgehunt.model.ServerList;
import edu.kit.informatik.student.knowledgehunt.protocol.messages.DiscoveryMessage;
import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ServerBroadcastListenerTest {
	private ServerList serverList;
	private ServerBroadcastListener serverBroadcastListener;

	@Before
	public void setUp() throws SocketException {
		serverList = new ServerList();
		serverBroadcastListener = new ServerBroadcastListener(serverList);
		serverBroadcastListener.start();
	}

	@Test
	public void testServerDiscovery() {
		assertTrue(serverList.getServers().isEmpty());

		DiscoveryMessage message = new DiscoveryMessage();
		message.setIpAdress("ip-foo");
		message.setPort(42);
		sendPacket(message.getBytes());

		Awaitility.await().until(() -> !serverList.getServers().isEmpty());
		Server server = serverList.getServers().get(0);

		assertEquals("ip-foo", server.getIp());
		assertEquals(42, server.getPort());
	}

	@Test
	public void testInvalidPackage() {
		assertTrue(serverList.getServers().isEmpty());

		sendPacket(new byte[] {0}); // Server should ignore invalid UDP packets
		sendPacket(new DiscoveryMessage().getBytes());

		Awaitility.await().until(() -> !serverList.getServers().isEmpty());
	}

	private void sendPacket(byte[] sendData) {
		try (DatagramSocket serverSocket = new DatagramSocket()) {
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length,
					InetAddress.getLocalHost(), DiscoveryMessage.PORT);
			serverSocket.send(sendPacket);
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	@After
	public void cleanUp() {
		serverBroadcastListener.stop();
	}
}
