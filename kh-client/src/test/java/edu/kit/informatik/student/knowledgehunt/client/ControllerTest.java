package edu.kit.informatik.student.knowledgehunt.client;

import edu.kit.informatik.student.knowledgehunt.model.GameLobby;
import edu.kit.informatik.student.knowledgehunt.model.GameMode;
import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static edu.kit.informatik.student.knowledgehunt.client.MockServerScenarioAnswerAll.GAME_LOBBY_ID;

public class ControllerTest {
	private MockController controller;
	private MockServer server;

	@Before
	public void setUp() {
		controller = new MockController();
		controller.start(null);
		server = new MockServer();
		server.start();
	}

	@Test
	public void testControllerConnect() {
		server.setScenario(new MockServerScenarioAnswerAll());

		controller.uiListeners.serverSelectedListener.onServerSelected(server.getServerInfo(), "username");
		Awaitility.await().until(() -> controller.getState() == ClientState.SERVER_LOBBY);

		controller.uiListeners.leaveServerListener.onLeaveServer();
		Awaitility.await().until(() -> controller.getState() == ClientState.DISCONNECTED);
	}

	@Test
	public void testControllerJoinGameLobby() {
		server.setScenario(new MockServerScenarioAnswerAll());

		controller.uiListeners.serverSelectedListener.onServerSelected(server.getServerInfo(), "username");
		Awaitility.await().until(() -> controller.getState() == ClientState.SERVER_LOBBY);

		controller.uiListeners.gameSelectedListener.onGameSelected(
				new GameLobby(GAME_LOBBY_ID, new GameMode(), 2));
		Awaitility.await().until(() -> controller.getState() == ClientState.GAME_LOBBY);

		controller.uiListeners.leaveLobbyListener.leaveLobby();
		Awaitility.await().until(() -> controller.getState() == ClientState.SERVER_LOBBY);
	}

	@After
	public void cleanUp() {
		controller.stop();
		server.stop();
	}
}