package edu.kit.informatik.student.knowledgehunt.gamemode;

import edu.kit.informatik.student.knowledgehunt.model.GameMode;
import edu.kit.informatik.student.knowledgehunt.model.Location;
import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.model.Token;
import edu.kit.informatik.student.knowledgehunt.model.TokenType;
import edu.kit.informatik.student.knowledgehunt.util.IdGenerator;
import edu.kit.informatik.student.knowledgehunt.util.plugin.GamePlugin;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Quiz implements GamePlugin {
	private static final int NUM_LOCATIONS = 4;
	private static final int WIN_POINTS = 100;
	private static final int ID_TRUE = 0;
	private static final int ID_FALSE = 1;
	private List<Player> players;
	private List<List<Token>> placedTokens = new ArrayList<>();
	private List<Integer> correctAnswers = new ArrayList<>();
	private Set<Token> tokens = new HashSet<>();
	private Set<Location> locations = new HashSet<>();

	@Override
	public GameMode getGameMode() {
		GameMode mode = new GameMode();
		mode.setDescription("Vote for answers by placing tokens.\n"
				+ "The first player with the correct answer earns 10 points, then 5, 3, 1.\n"
				+ "Winner is who first earns " + WIN_POINTS + " points total.");
		mode.setName("Quiz");
		return mode;
	}

	@Override
	public int getMinNumPlayers() {
		return 1;
	}

	@Override
	public int getMaxNumPlayers() {
		return 8;
	}

	@Override
	public boolean hideTokens() {
		return true;
	}

	@Override
	public void initNewGame(List<Player> players) {
		this.players = players;
	}

	@Override
	public Set<TokenType> getTokenTypes() {
		Set<TokenType> set = new HashSet<>();
		set.add(new TokenType(ID_TRUE, "True", "This token should be placed if the claim is true"));
		set.add(new TokenType(ID_FALSE, "False", "This token should be placed if the claim is false"));
		return set;
	}

	@Override
	public void removePlayer(Player player) {
		players.remove(player);
	}

	@Override
	public void stop() {

	}

	@Override
	public void startPreround() {
		placedTokens = Stream.generate((Supplier<ArrayList<Token>>) ArrayList::new)
				.limit(NUM_LOCATIONS).collect(Collectors.toCollection(ArrayList::new));

		IdGenerator id = new IdGenerator(players.size() * 2 * NUM_LOCATIONS);
		tokens = new HashSet<>();
		for (Player p : players) {
			for (int type = 0; type < 2; type++) {
				for (int number = 0; number < NUM_LOCATIONS; number++) {
					tokens.add(new Token(id.get(), type, (type == ID_TRUE) ? "True" : "False",
							Integer.MIN_VALUE, p.getId()));
				}
			}
		}

		locations.clear();
		correctAnswers.clear();
		Random rand = new Random();
		for (int i = 0; i < NUM_LOCATIONS; i++) {
			int n1 = rand.nextInt(70) + 10;
			int n2 = rand.nextInt(70) + 10;
			int result = n1 + n2;

			if (rand.nextInt(2) == 1) {
				result += rand.nextInt(30) - 15;
				correctAnswers.add(ID_FALSE);
			} else {
				correctAnswers.add(ID_TRUE);
			}
			locations.add(new Location(i, n1 + "+" + n2 + "=" + result, Integer.MIN_VALUE));
		}
	}

	@Override
	public Duration getPreroundDuration() {
		return Duration.ZERO;
	}

	@Override
	public Set<Token> getTokens() {
		return tokens;
	}

	@Override
	public Set<Location> getLocations() {
		return locations;
	}

	@Override
	public void startRound() {

	}

	@Override
	public Duration getRoundDuration() {
		return Duration.ofSeconds(30);
	}

	@Override
	public boolean placeToken(Token token, Location location) {
		long placedOnLocation = placedTokens.get(location.getId()).stream()
				.filter(token1 -> token.getPlayerId() == token1.getPlayerId()).count();
		long placedToken = placedTokens.stream().flatMap(Collection::stream)
				.filter(token1 -> token.getId() == token1.getId()).count();
		if (placedOnLocation == 0 && placedToken == 0) {
			placedTokens.get(location.getId()).add(token);
			tokens.removeIf(t -> token.getId() == t.getId());
			return true;
		}
		return false;
	}

	@Override
	public Set<Location> testToken(Token token) throws UnsupportedOperationException {
		Set<Location> locations = new HashSet<>();
		for (int i = 0; i < NUM_LOCATIONS; i++) {
			long placed = placedTokens.get(i).stream()
					.filter(token1 -> token.getPlayerId() == token1.getPlayerId()).count();
			if (placed == 0) {
				locations.add(new Location(i, null));
			}
		}
		return locations;
	}

	@Override
	public boolean shouldEndRoundNow() {
		long placed = placedTokens.stream().mapToLong(Collection::size).sum();
		return placed == players.size() * NUM_LOCATIONS;
	}

	@Override
	public void startPostround() {

	}

	@Override
	public Duration getPostroundDuration() {
		return Duration.ofSeconds(10);
	}

	@Override
	public void updatePlayerPoints() {
		for (int loc = 0; loc < placedTokens.size(); loc++) {
			List<Token> location = placedTokens.get(loc);
			int points = 10;
			for (Token token : location) {
				if (correctAnswers.get(loc) == token.getType()) {
					GameModeUtils.findPlayer(players, token.getPlayerId()).incrementPoints(points);
					points /= 2;
				}
			}
		}
	}

	@Override
	public Player getWinner() {
		Player maxPlayer = players.stream().max(Comparator.comparing(Player::getPoints)).orElse(new Player());
		if (maxPlayer.getPoints() >= WIN_POINTS) {
			return maxPlayer;
		}
		return null;
	}

	@Override
	public String getWinningReason() {
		return "Player was first to reach " + WIN_POINTS + " points";
	}
}
