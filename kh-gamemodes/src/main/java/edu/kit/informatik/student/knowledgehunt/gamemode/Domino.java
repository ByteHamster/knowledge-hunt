package edu.kit.informatik.student.knowledgehunt.gamemode;

import edu.kit.informatik.student.knowledgehunt.model.GameMode;
import edu.kit.informatik.student.knowledgehunt.model.Location;
import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.model.Token;
import edu.kit.informatik.student.knowledgehunt.model.TokenType;
import edu.kit.informatik.student.knowledgehunt.util.IdGenerator;
import edu.kit.informatik.student.knowledgehunt.util.plugin.GamePlugin;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class Domino implements GamePlugin {
	private static final int NUM_TOKENS = 8;
	private static final int NUM_JOKERS = 1;
	private static final int MAX_NUMBER = 4;
	private static final int ID_STONE = 0;
	private static final int ID_JOKER = 1;
	private List<Player> players;
	private List<Token> placedTokens = new ArrayList<>();
	private Set<Token> tokens = new HashSet<>();
	private HashSet<Location> locations = new HashSet<>();

	@Override
	public GameMode getGameMode() {
		GameMode mode = new GameMode();
		mode.setDescription("Each token is named after two numbers: \"A / B\"."
				+ "You can only place stones if they fit to the previous token."
				+ "For example, (1 / 3) fits to (3 / 5). The joker fits everywhere.\n\n"
				+ "The player who first places all tokens wins.");
		mode.setName("Domino");
		return mode;
	}

	@Override
	public int getMinNumPlayers() {
		return 1;
	}

	@Override
	public int getMaxNumPlayers() {
		return 8;
	}

	@Override
	public boolean hideTokens() {
		return false;
	}

	@Override
	public void initNewGame(List<Player> players) {
		this.players = players;
	}

	@Override
	public Set<TokenType> getTokenTypes() {
		Set<TokenType> set = new HashSet<>();
		set.add(new TokenType(ID_STONE, "Domino stone", "Can be placed if it fits previous tokens"));
		set.add(new TokenType(ID_JOKER, "Joker", "Can always be placed"));
		return set;
	}

	@Override
	public void removePlayer(Player player) {
		players.remove(player);
	}

	@Override
	public void stop() {

	}

	@Override
	public void startPreround() {
		placedTokens = new ArrayList<>();

		IdGenerator id = new IdGenerator(players.size() * (NUM_TOKENS + NUM_JOKERS));
		tokens = new HashSet<>();
		Random rand = new Random();
		for (Player p : players) {
			for (int number = 0; number < NUM_TOKENS; number++) {
				tokens.add(new Token(id.get(), ID_STONE,
						(rand.nextInt(MAX_NUMBER) + 1) + " / " + (rand.nextInt(MAX_NUMBER) + 1),
						Integer.MIN_VALUE, p.getId()));
			}
			for (int number = 0; number < NUM_JOKERS; number++) {
				tokens.add(new Token(id.get(), ID_JOKER, "* / *",
						Integer.MIN_VALUE, p.getId()));
			}
		}

		locations = new HashSet<>();
		locations.add(new Location(0, "Table", Integer.MIN_VALUE));
	}

	@Override
	public Duration getPreroundDuration() {
		return Duration.ofSeconds(10);
	}

	@Override
	public Set<Token> getTokens() {
		return tokens;
	}

	@Override
	public Set<Location> getLocations() {
		return locations;
	}

	@Override
	public void startRound() {

	}

	@Override
	public Duration getRoundDuration() {
		return Duration.ofSeconds(90);
	}

	@Override
	public boolean placeToken(Token t, Location location) {
		Token token = GameModeUtils.findToken(tokens, t.getId());
		if (token != null) {
			if (isPlacementAllowed(token)) {
				placedTokens.add(token);
				tokens.removeIf(t0 -> token.getId() == t0.getId());
				return true;
			}
		}
		return false;
	}

	private boolean isPlacementAllowed(Token token) {
		boolean allowed = false;
		if (token.getType() == ID_JOKER) {
			allowed = true;
		} else if (placedTokens.size() == 0) {
			allowed = true;
		} else {
			Token previousToken = placedTokens.get(placedTokens.size() - 1);

			if (previousToken.getType() == ID_JOKER) {
				allowed = true;
			} else {
				String[] previousTokenName = previousToken.getDisplayName().split(" / ");
				String[] currentTokenName = token.getDisplayName().split(" / ");
				if (previousTokenName[1].equals(currentTokenName[0])) {
					allowed = true;
				}
			}
		}
		return allowed;
	}

	@Override
	public Set<Location> testToken(Token token) throws UnsupportedOperationException {
		return isPlacementAllowed(token) ? locations : new HashSet<>();
	}

	@Override
	public boolean shouldEndRoundNow() {
		if (getPlayerWhoPlacedAll() != null) {
			return true;
		}
		return tokens.stream().noneMatch(this::isPlacementAllowed);
	}

	@Override
	public void startPostround() {

	}

	@Override
	public Duration getPostroundDuration() {
		return Duration.ofSeconds(5);
	}

	@Override
	public void updatePlayerPoints() {

	}

	@Override
	public Player getWinner() {
		return getPlayerWhoPlacedAll();
	}

	private Player getPlayerWhoPlacedAll() {
		Map<Integer, Integer> placedByPlayer = new HashMap<>();
		for (Token token : placedTokens) {
			int placed = 0;
			if (placedByPlayer.containsKey(token.getPlayerId())) {
				placed = placedByPlayer.get(token.getPlayerId());
			}
			placed++;
			placedByPlayer.put(token.getPlayerId(), placed);

			if (placed == NUM_TOKENS + NUM_JOKERS) {
				return GameModeUtils.findPlayer(players, token.getPlayerId());
			}
		}
		return null;
	}

	@Override
	public String getWinningReason() {
		return "Player was first to place all stones";
	}
}
