package edu.kit.informatik.student.knowledgehunt.gamemode;

import edu.kit.informatik.student.knowledgehunt.model.GameMode;
import edu.kit.informatik.student.knowledgehunt.model.Location;
import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.model.Token;
import edu.kit.informatik.student.knowledgehunt.model.TokenType;
import edu.kit.informatik.student.knowledgehunt.util.IdGenerator;
import edu.kit.informatik.student.knowledgehunt.util.plugin.GamePlugin;

import java.time.Duration;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Default implements GamePlugin {
	private List<Player> players;
	private static final int ID_NORMAL = 0;
	private static final int ID_DISTURB = 1;
	private static final int WINNER_POINTS = 200;
	private List<List<Token>> placedTokens = new ArrayList<>();
	private List<Location> locations = new ArrayList<>();
	private Set<Token> tokens = new HashSet<>();

	@Override
	public GameMode getGameMode() {
		GameMode mode = new GameMode();
		mode.setDescription("Default KnowledgeHunt game");
		mode.setName("Knowledge Hunt");
		return mode;
	}

	@Override
	public int getMinNumPlayers() {
		return 1;
	}

	@Override
	public int getMaxNumPlayers() {
		return 20;
	}

	@Override
	public boolean hideTokens() {
		return true;
	}

	@Override
	public void initNewGame(List<Player> players) {
		this.players = players;
		reset();
	}

	private void reset() {
		locations = new ArrayList<>();
		IdGenerator id = new IdGenerator(5);
		locations.add(new Location(id.get(), "Bib", 50));
		locations.add(new Location(id.get(), "Tutorium", 50));
		locations.add(new Location(id.get(), "Forum", 40));
		locations.add(new Location(id.get(), "Mensa", 10));
		locations.add(new Location(id.get(), "Fachschaft", -10));
		placedTokens = IntStream.range(0, 5).mapToObj(t -> new ArrayList<Token>()).collect(Collectors.toList());

		id = new IdGenerator(players.size() * 6);
		tokens.clear();
		for (Player player : players) {
			for (int i = 0; i < 5; i++) {
				int value = (i + 1) * 10;
				tokens.add(new Token(id.get(), ID_NORMAL, "Token (" + value + ")", value, player.getId()));
			}
			tokens.add(new Token(id.get(), ID_DISTURB, "Disturb-Token", 0, player.getId()));
		}
	}

	@Override
	public Set<TokenType> getTokenTypes() {
		Set<TokenType> set = new HashSet<>();
		set.add(new TokenType(ID_NORMAL, "Tokens", "This token can be placed to occupy locations"));
		set.add(new TokenType(ID_DISTURB, "Disturb-Tokens",
				"If this token is placed, nobody gets points on that location"));
		return set;
	}

	@Override
	public void removePlayer(Player player) {
		players.remove(player);
	}

	@Override
	public void stop() {

	}

	@Override
	public void startPreround() {
		placedTokens = IntStream.range(0, 5).mapToObj(t -> new ArrayList<Token>()).collect(Collectors.toList());
	}

	@Override
	public Duration getPreroundDuration() {
		return Duration.ofSeconds(10);
	}

	@Override
	public Set<Token> getTokens() {
		return tokens;
	}

	@Override
	public Set<Location> getLocations() {
		return new HashSet<>(locations);
	}

	@Override
	public void startRound() {

	}

	@Override
	public Duration getRoundDuration() {
		return Duration.ofSeconds(20);
	}

	@Override
	public boolean placeToken(Token token, Location location) {
		long placed = placedTokens.stream().flatMap(Collection::stream)
				.filter(t -> t.getId() == token.getId()).count();
		long available = tokens.stream()
				.filter(t -> t.getId() == token.getId()).count();
		if (placed == 0 && available == 1) {
			placedTokens.get(location.getId()).add(token);
			return true;
		}
		return false;
	}

	@Override
	public Set<Location> testToken(Token token) throws UnsupportedOperationException {
		long available = tokens.stream()
				.filter(t -> t.getId() == token.getId()).count();
		if (available == 1) {
			return new HashSet<>(locations);
		}
		return new HashSet<>();
	}

	@Override
	public boolean shouldEndRoundNow() {
		long placed = placedTokens.stream().mapToLong(Collection::size).sum();
		long available = tokens.size();
		return placed == available;
	}

	@Override
	public void startPostround() {

	}

	@Override
	public Duration getPostroundDuration() {
		return Duration.ofSeconds(10);
	}

	@Override
	public void updatePlayerPoints() {
		for (int loc = 0; loc < placedTokens.size(); loc++) {
			List<Token> locationTokens = placedTokens.get(loc);
			Location location = GameModeUtils.findLocation(locations, loc);
			Map<Integer, Integer> playerPoints = new HashMap<>();
			boolean disturbed = false;
			for (Token token : locationTokens) {
				int oldPoints = 0;
				if (playerPoints.containsKey(token.getPlayerId())) {
					oldPoints = playerPoints.get(token.getPlayerId());
				}
				playerPoints.put(token.getPlayerId(), oldPoints + token.getValue());
				if (token.getType() == ID_DISTURB) {
					disturbed = true;
					break;
				}
			}

			int winnerId = Integer.MIN_VALUE;
			if (!disturbed) {
				winnerId = playerPoints.entrySet().stream()
						.max(Comparator.comparing(Map.Entry::getValue))
						.orElse(new AbstractMap.SimpleEntry<>(Integer.MIN_VALUE, 0))
						.getKey();
				if (winnerId != Integer.MIN_VALUE && GameModeUtils.findPlayer(players, winnerId) != null) {
					GameModeUtils.findPlayer(players, winnerId).incrementPoints(location.getPoints());
				}
			}

			for (Token token : locationTokens) {
				if (token.getPlayerId() != winnerId) {
					tokens.removeIf(t -> t.getId() == token.getId());
				}
			}
		}
	}

	@Override
	public Player getWinner() {
		Player maxPlayer = players.stream().max(Comparator.comparing(Player::getPoints)).orElse(new Player());
		if (maxPlayer.getPoints() >= WINNER_POINTS) {
			return maxPlayer;
		}
		return null;
	}

	@Override
	public String getWinningReason() {
		return "Player was first to reach " + WINNER_POINTS + " points";
	}
}
