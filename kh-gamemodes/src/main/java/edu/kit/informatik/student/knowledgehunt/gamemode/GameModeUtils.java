package edu.kit.informatik.student.knowledgehunt.gamemode;

import edu.kit.informatik.student.knowledgehunt.model.Location;
import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.model.Token;

import java.util.Collection;

final class GameModeUtils {

	private GameModeUtils() {

	}

	static Player findPlayer(Collection<Player> players, int playerId) {
		return players.stream().filter(p -> p.getId() == playerId).findFirst().orElse(null);
	}

	static Token findToken(Collection<Token> tokens, int tokenId) {
		return tokens.stream().filter(t -> t.getId() == tokenId).findFirst().orElse(null);
	}

	static Location findLocation(Collection<Location> locations, int locationId) {
		return locations.stream().filter(t -> t.getId() == locationId).findFirst().orElse(null);
	}
}
