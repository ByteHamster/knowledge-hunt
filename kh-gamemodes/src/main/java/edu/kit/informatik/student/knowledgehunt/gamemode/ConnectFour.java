package edu.kit.informatik.student.knowledgehunt.gamemode;

import edu.kit.informatik.student.knowledgehunt.model.GameMode;
import edu.kit.informatik.student.knowledgehunt.model.Location;
import edu.kit.informatik.student.knowledgehunt.model.Player;
import edu.kit.informatik.student.knowledgehunt.model.Token;
import edu.kit.informatik.student.knowledgehunt.model.TokenType;
import edu.kit.informatik.student.knowledgehunt.util.IdGenerator;
import edu.kit.informatik.student.knowledgehunt.util.plugin.GamePlugin;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ConnectFour implements GamePlugin {
	private static final int ID_STONE = 0;
	private static final int NUM_ROWS = 6;
	private static final int MAX_COLS = 7;
	private static final int NUM_TOKENS = NUM_ROWS * MAX_COLS;
	private List<Player> players;
	private List<List<Token>> placedTokens = new ArrayList<>();
	private Set<Token> tokens = new HashSet<>();
	private HashSet<Location> locations = new HashSet<>();
	private int nextPlayer = 0;

	@Override
	public GameMode getGameMode() {
		GameMode mode = new GameMode();
		mode.setDescription("Classic Connect Four game.");
		mode.setName("Connect Four");
		return mode;
	}

	@Override
	public int getMinNumPlayers() {
		return 2;
	}

	@Override
	public int getMaxNumPlayers() {
		return 2;
	}

	@Override
	public boolean hideTokens() {
		return false;
	}

	@Override
	public void initNewGame(List<Player> players) {
		this.players = players;
	}

	@Override
	public Set<TokenType> getTokenTypes() {
		Set<TokenType> set = new HashSet<>();
		set.add(new TokenType(ID_STONE, "Stone", "Can be placed on the board"));
		return set;
	}

	@Override
	public void removePlayer(Player player) {
		players.remove(player);
	}

	@Override
	public void stop() {

	}

	@Override
	public void startPreround() {
		placedTokens = IntStream.range(0, NUM_ROWS).mapToObj(t -> new ArrayList<Token>()).collect(Collectors.toList());

		IdGenerator id = new IdGenerator(NUM_TOKENS);
		tokens = new HashSet<>();
		for (int i = 0; i < players.size(); i++) {
			Player p = players.get(i);
			for (int number = 0; number < NUM_TOKENS / 2; number++) {
				tokens.add(new Token(id.get(), ID_STONE, i == 0 ? "A" : "B", 0, p.getId()));
			}
		}

		locations = new HashSet<>();
		for (int i = 0; i < NUM_ROWS; i++) {
			locations.add(new Location(i, "Column " + (i + 1)));
		}
	}

	@Override
	public Duration getPreroundDuration() {
		return Duration.ofSeconds(0);
	}

	@Override
	public Set<Token> getTokens() {
		return tokens;
	}

	@Override
	public Set<Location> getLocations() {
		return locations;
	}

	@Override
	public void startRound() {

	}

	@Override
	public Duration getRoundDuration() {
		return Duration.ofMinutes(10);
	}

	@Override
	public boolean placeToken(Token t, Location location) {
		Token token = GameModeUtils.findToken(tokens, t.getId());
		if (token != null) {
			if (isPlacementAllowed(token, location.getId())) {
				placedTokens.get(location.getId()).add(token);
				tokens.removeIf(t0 -> token.getId() == t0.getId());
				nextPlayer = (nextPlayer + 1) % 2;
				return true;
			}
		}
		return false;
	}

	private boolean isPlacementAllowed(Token token, int location) {
		if (token.getPlayerId() != players.get(nextPlayer).getId()) {
			return false;
		}
		return placedTokens.get(location).size() < MAX_COLS;
	}

	@Override
	public Set<Location> testToken(Token t) throws UnsupportedOperationException {
		Token token = GameModeUtils.findToken(tokens, t.getId());
		return locations.stream().filter(location ->
				isPlacementAllowed(token, location.getId())).collect(Collectors.toSet());
	}

	@Override
	public boolean shouldEndRoundNow() {
		return getWinner() != null || tokens.isEmpty();
	}

	@Override
	public void startPostround() {

	}

	@Override
	public Duration getPostroundDuration() {
		return Duration.ofSeconds(10);
	}

	@Override
	public void updatePlayerPoints() {

	}

	@Override
	public Player getWinner() {
		// Horizontal
		for (int row = 0; row < NUM_ROWS; row++) {
			int currentPlayer = -1;
			int currentPlayerNum = 0;
			for (Token t : placedTokens.get(row)) {
				if (t.getPlayerId() == currentPlayer) {
					currentPlayerNum++;
				} else {
					currentPlayer = t.getPlayerId();
					currentPlayerNum = 1;
				}
				if (currentPlayerNum == 4 && currentPlayer != -1) {
					return GameModeUtils.findPlayer(players, currentPlayer);
				}
			}
		}
		// Vertical
		for (int col = 0; col < MAX_COLS; col++) {
			int currentPlayer = -1;
			int currentPlayerNum = 0;
			for (int row = 0; row < NUM_ROWS; row++) {
				if (placedTokens.get(row).size() <= col) {
					currentPlayer = -1;
					currentPlayerNum = 0;
				} else if (placedTokens.get(row).get(col).getPlayerId() == currentPlayer) {
					currentPlayerNum++;
				} else {
					currentPlayer = placedTokens.get(row).get(col).getPlayerId();
					currentPlayerNum = 1;
				}
				if (currentPlayerNum == 4 && currentPlayer != -1) {
					return GameModeUtils.findPlayer(players, currentPlayer);
				}
			}
		}
		// Diagonal
		for (int col = 0; col < MAX_COLS; col++) {
			for (int row = 0; row < NUM_ROWS; row++) {
				if (checkDiagonal1From(col, row) != null) {
					return checkDiagonal1From(col, row);
				} else if (checkDiagonal2From(col, row) != null) {
					return checkDiagonal2From(col, row);
				}
			}
		}
		return null;
	}

	private Player checkDiagonal1From(int col, int row) {
		int currentPlayer = -1;
		int currentPlayerNum = 0;

		for (int d = 0; d < Math.max(NUM_ROWS, MAX_COLS); d++) {
			if (row + d >= NUM_ROWS || col + d >= MAX_COLS || placedTokens.get(row + d).size() <= col + d) {
				currentPlayer = -1;
				currentPlayerNum = 0;
			} else if (placedTokens.get(row + d).get(col + d).getPlayerId() == currentPlayer) {
				currentPlayerNum++;
			} else {
				currentPlayer = placedTokens.get(row + d).get(col + d).getPlayerId();
				currentPlayerNum = 1;
			}
			if (currentPlayerNum == 4 && currentPlayer != -1) {
				return GameModeUtils.findPlayer(players, currentPlayer);
			}
		}
		return null;
	}

	private Player checkDiagonal2From(int col, int row) {
		int currentPlayer = -1;
		int currentPlayerNum = 0;

		for (int d = 0; d < Math.max(NUM_ROWS, MAX_COLS); d++) {
			if (row - d < 0 || col + d >= MAX_COLS || placedTokens.get(row - d).size() <= col + d) {
				currentPlayer = -1;
				currentPlayerNum = 0;
			} else if (placedTokens.get(row - d).get(col + d).getPlayerId() == currentPlayer) {
				currentPlayerNum++;
			} else {
				currentPlayer = placedTokens.get(row - d).get(col + d).getPlayerId();
				currentPlayerNum = 1;
			}
			if (currentPlayerNum == 4 && currentPlayer != -1) {
				return GameModeUtils.findPlayer(players, currentPlayer);
			}
		}
		return null;
	}

	@Override
	public String getWinningReason() {
		return "Player got four in a row";
	}
}
