package edu.kit.informatik.student.knowledgehunt.protocol.annotations;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MessageDefAnnotationProcessor extends AbstractProcessor {
	private static final String PACKAGE = "edu.kit.informatik.student.knowledgehunt.protocol";
	private Filer filer;

	@Override
	public Set<String> getSupportedAnnotationTypes() {
		Set<String> set = new HashSet<>();
		set.add(MessageDef.class.getName());
		return set;
	}

	@Override
	public SourceVersion getSupportedSourceVersion() {
		return SourceVersion.RELEASE_8;
	}

	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		super.init(processingEnv);
		filer = processingEnv.getFiler();
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		Map<Integer, String> messages = new HashMap<>();

		if (roundEnv.errorRaised() || roundEnv.processingOver()) {
			return true;
		}

		Set<Element> toProcess = new HashSet<>(roundEnv.getElementsAnnotatedWith(MessageDef.class));

		for (Element e : roundEnv.getElementsAnnotatedWith(MessageDef.class)) {
			PackageElement pkg = findPkg(e);
			for (Element elem : pkg.getEnclosedElements()) {
				if (elem.getAnnotation(MessageDef.class) != null) {
					toProcess.add(elem);
				}
			}
		}

		for (Element element : toProcess) {
			if (!element.getKind().isClass()) {
				processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
						"@MessageDef must be applied to a class", element);
				return true;
			}
			TypeElement typeElement = (TypeElement) element;

			if (!typeElement.getModifiers().contains(Modifier.PUBLIC)) {
				processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
						"@MessageDef classes must be public", element);
				return true;
			}

			if (!typeElement.getSuperclass().toString().equals(PACKAGE + ".messages.meta.Message")) {
				processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
						"@MessageDef classes must derive from Message", element);
				return true;
			}

			long emptyConstructorsCount = typeElement.getEnclosedElements().stream()
					.filter(o -> o.getKind() == ElementKind.CONSTRUCTOR)
					.map(o -> (ExecutableElement) o)
					.filter(o -> o.getParameters().size() == 0)
					.filter(o -> o.getModifiers().contains(Modifier.PUBLIC))
					.count();

			if (emptyConstructorsCount == 0) {
				processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
						"@MessageDef classes must provide an empty constructor", element);
				return true;
			}

			MessageDef messageDef = element.getAnnotation(MessageDef.class);
			messages.put(messageDef.id(), typeElement.getQualifiedName().toString());
			System.out.println("Found message: " + typeElement.getQualifiedName().toString());
		}

		try {
			generateCode(messages);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return true;
	}

	private PackageElement findPkg(Element el) {
		Element e = el;
		while (true) {
			if (e instanceof PackageElement) {
				return (PackageElement) e;
			}
			e = e.getEnclosingElement();
		}
	}

	private void generateCode(Map<Integer, String> messages) throws IOException {
		JavaFileObject jfo = filer.createSourceFile(PACKAGE + ".annotations.MessageFactory");
		Writer writer = jfo.openWriter();
		writer.write("// Generated automatically. Do not modify.\n");
		writer.write("package " + PACKAGE + ".annotations;\n");
		writer.write("public class MessageFactory {\n");
		writer.write("  public static " + PACKAGE + ".messages.meta.Message getMessage(int id) {\n");
		writer.write("    switch (id) {\n");
		for (Map.Entry<Integer, String> message : messages.entrySet()) {
			writer.write("      case " + message.getKey() + ":\n");
			writer.write("        return new " + message.getValue() + "();\n");
		}
		writer.write("      default:\n        return null;\n");
		writer.write("    }\n  }\n}\n");
		writer.close();
	}
}
